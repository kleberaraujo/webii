<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="filme" class="Model.FilmeDAO" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CineClub | Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
    </head>

    <body>

        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>

            <div id="page-wrapper">

                <div class="row">
                    <div class="col-lg-8">
                        <h1>Filmes</h1>
                    </div>
                    <div class="col-lg-4">
                        <p class="pesquisa">
                            <form method="POST" action="filmes?action=pesq" >
                                <input name="pesq" class="form-control" placeholder="Buscar filmes...">
                                <!--<button type="submit" class="btn btn-primary">Pesquisar</button>-->
                            </form>
                        </p>
                    </div>
                </div>

                <c:forEach var="f" items="${filmes}" >
                    <div class="row">
                        <div class="bs-example">
                            <div class="jumbotron">
                                <div class="row">
                                    <div class="col-lg-4 text-center">
                                        <div class="panel panel-default">
                                            <div class="panel-body-img">
                                                <c:if test="${not empty f.video }">
                                                    <iframe class="panel-body-img" src="http://www.youtube.com/embed/${f.video}"></iframe>
                                                </c:if>
                                                <c:if test="${empty f.video }">
                                                    <img class="panel-body-img" src="./${f.imagem}"/>
                                                </c:if>
                                            </div>
                                        </div>            
                                    </div>
                                    <h2 id="h2">
                                        ${f.titulo}
                                    </h2>
                                    ${f.sinopse}
                                    <a id="bt_remover" onclick="return f_delete();" href="filmes?action=remover&id=${f.id}">Remover</a>
                                    <a id="bt_alterar" href="filmes?action=setAlterar&id=${f.id}">Alterar</a>
                                </div>     
                            </div>
                        </div>
                    </c:forEach>

                    <!--<div class="col-lg-12">
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div> -->
                </div><!-- /.row -->

            </div><!-- /#page-wrapper -->

        </div><!-- /#wrapper -->

        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>

        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
