package DAOs;

import Infra.ConnectionFactory;
import Beans.Conserto;
import Beans.Relatorio;
import Infra.Data;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConsertoDAO{  //implements Mediator {
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public ConsertoDAO() {}
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public void Salvar(Conserto pConserto) throws Exception{
        try {
            Date sqlDate  = new Date(pConserto.getDataEntrada().getTime());
            Date sqlDate1 = new Date(pConserto.getDataEntregaPrevisao().getTime());
            
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("INSERT INTO Conserto (idCliente,idTipo, fabricante, modelo, "
                    + "dataEntrada, dataEntregaPrevisao, valor, problema, descricao, status) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS );
            stmt.setInt(1, pConserto.getIdCliente());
            stmt.setInt(2, pConserto.getTipo());
            stmt.setString(3, pConserto.getFabricante());
            stmt.setString(4, pConserto.getModelo());
            stmt.setDate(5, sqlDate);
            stmt.setDate(6, sqlDate1);
            stmt.setDouble(7, pConserto.getValor());
            stmt.setString(8, pConserto.getProblema());
            stmt.setString(9, pConserto.getDescricao());
            stmt.setInt(10, pConserto.getStatus());
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                pConserto.setIdConserto(rs.getInt(1));
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void AlterarPedidoAberto(int idConserto) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("UPDATE Conserto SET status = ? ,dataEntregaReal = ? , dataInicial = ? ,"
                    + "dataFinal = ? WHERE idConserto = ? ");
            stmt.setInt(1, 1);
            stmt.setDate(2, null);
            stmt.setDate(3, null);
            stmt.setDate(4, null);
            stmt.setInt(5, idConserto);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void AlterarConsertoAndamento(int idConserto, Date dataInicial, int status) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("UPDATE Conserto SET dataInicial = ? ,status = ? "
                    + "WHERE idConserto = ?");
            stmt.setDate(1, dataInicial);
            stmt.setInt(2, status);
            stmt.setInt(3, idConserto);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void AlterarConsertoRealizado(int idConserto, Date dataFinal, String observacao, int status) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("UPDATE Conserto SET dataFinal = ? ,observacao = ? ,status = ? "
                    + "WHERE idConserto = ?");
            stmt.setDate(1, dataFinal);
            stmt.setString(2, observacao);
            stmt.setInt(3, status);
            stmt.setInt(4, idConserto);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void AlterarConsertoEntregue(int idConserto, Date dataFinal, int status) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("UPDATE Conserto SET dataEntregaReal = ? , status = ? "
                    + "WHERE idConserto = ?");
            stmt.setDate(1, dataFinal);
            stmt.setInt(2, status);
            stmt.setInt(3, idConserto);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void RemoverConseroTecnico(int idConserto) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("DELETE FROM TecnicoConserto WHERE idConserto = ?");
            stmt.setInt(1, idConserto);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void SalvarConsertoTecnico(int idConserto, String pUsuario) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("INSERT INTO TecnicoConserto (idConserto,login) VALUES (?,?)");
            stmt.setInt(1, idConserto);
            stmt.setString(2, pUsuario);
            stmt.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public Relatorio consultarConserto(int pIdConserto) throws Exception {
        try{
            Data dTemp = new Data();
            Relatorio tRelatorio = null;
            java.util.Date d = dTemp.DataAtualDate();
            Date sqlDate  = new Date(d.getTime());
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT co.idconserto, cli.nome, co.idtipo AS tipo, co.fabricante, co.modelo,"
                                      + "co.dataentrada, co.dataentregaprevisao, co.valor, co.problema, co.descricao, "
                                      + "co.status, tc.login AS tecnico, co.datainicial, co.datafinal, co.observacao, co.dataentregareal, "
                                      + "cli.idCliente, cli.cpf, cli.datanasc, cli.sexo, cli.cep, cid.nome AS cidade, "
                                      + "cli.endereco, cli.email, (co.dataEntregaPrevisao - ?) AS diasAtrasados "
                                      + "FROM conserto co "
                                      + "INNER JOIN cliente cli ON (co.idcliente = cli.idcliente) " 
                                      + "INNER JOIN status s ON (co.status = s.idstatus) "
                                      + "INNER JOIN tipoconserto tipo ON (co.idtipo = tipo.idtipo) "
                                      + "INNER JOIN cidade cid ON (cid.idcidade = cli.cidade) "
                                      + "LEFT JOIN tecnicoconserto tc ON (tc.idconserto = co.idconserto) "
                                      + "WHERE co.idconserto = ?");
            stmt.setDate(1, sqlDate);
            stmt.setInt(2, pIdConserto);
            rs = stmt.executeQuery();
            while(rs.next()){
                tRelatorio = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                            rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
            }
            return tRelatorio;
        }
        catch(Exception e){
            throw new Exception("Conserto não encontrado");
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<String> listarTipo() throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT idtipo, descricao FROM tipoconserto");
            List <String> lista = new ArrayList();
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("idtipo")+" - "+rs.getString("descricao"));
            }
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<String> listarStatus() throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT idstatus, descricao FROM status");
            List <String> lista = new ArrayList();
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("idstatus")+" - "+rs.getString("descricao"));
            }
            return lista;
        }
        catch(Exception e){
            throw new Exception("Erro listar status");
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    // </editor-fold>
    
}
