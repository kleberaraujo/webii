<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${empty login}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${acesso == 0}">
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html>
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nãotebuk - Concerto de equipamento</title>
            <link rel="stylesheet" type="text/css" href="./css/style.css">
            <link rel="stylesheet" type="text/css" href="./css/jquery-ui-1.10.4.custom.min.css">
            <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
            <script type="text/javascript" src="./javascript/jquery-1.10.2.min.js"></script>
            <script type="text/javascript" src="./javascript/jquery-ui-1.10.4.custom.js"></script>
            <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
            <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
            <script type="text/javascript" src="./javascript/jquery.maskMoney.js"></script>
            <script type="text/javascript" src="./javascript/funcoes.js"></script>
	</head>
	<body>
		<h1>Adicionar novo pedido de Concerto</h1>
		
		<div id="main_box">
                    <form  name="cadastroConserto" id="f_cpedido" action="Processar?action=inserirConserto" method="post" onsubmit="return valida_pedido(this);" >
                        <div class="f_row">
                            <label class="l_cadastro">Cliente:</label>
                            <c:choose>
                                <c:when test="${ListaClientes != null}">
                                    <select name="cliente" size="1" class="i_text">
                                        <c:forEach items="${ListaClientes}" var="Cliente">
                                            <option value="${Cliente}"/>${Cliente}
                                        </c:forEach>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <input type="text" name="emailCliente" class="i_text" readonly value="Nenhum Cliente Cadastrado"/>
                                </c:otherwise>
                            </c:choose>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Tipo:</label>
                            <select name="tipo" size="1" class="i_text">
                                <c:forEach items="${ListaTipo}" var="Tipo">
                                    <option value="${Tipo}">${Tipo}	
                                </c:forEach>
                            </select>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Fabricante:</label>
                            <input type="text" name="fabricante" class="i_text"/>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Modelo:</label>
                            <input type="text" name="modelo" class="i_text"/>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Data de Entrada:</label>
                            <input type="date" name="dataEntrada" class="i_text" readonly="readonly" id="dataEntrada" value="${Datas[0]}"/>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Data de Entrega:</label>
                            <input type="date" name="dataEntrega" class="i_text" id="dataEntrega" value="${Datas[1]}"/>
                        </div>

                        <div class="f_row">
                            <label for="i_valor" class="l_cadastro">Valor:</label>
                            <input type="text" id="i_valor" name="valor" class="i_text" value="100,00" required/>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Problema:</label>
                            <input type="text" id="i_sdesc" name="problema" class="i_text" required/>
                        </div>

                        <div class="f_row">
                            <label class="l_cadastro">Descrição completa:</label>
                            <textarea name="descricaoProblema" id="i_fdesc" class="i_text" cols=50 rows=7 ></textarea>
                        </div>

                        <div class="f_row">
                            <input type="button" id="b_salvar" onclick="cancelarCadastroTecnico();" name="b_cancelar" value="Cancelar"/>
                            <input type="submit" id="b_salvar" name="b_cadastro" value="Cadastrar"/>
                        </div>
                    </form>
		</div>
	</body>
</html>