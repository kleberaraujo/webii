
package Infra;

import java.util.Date;
import org.apache.commons.mail.*;

public class Email {   
    
    public static void enviarEmail(String endereco, String pedido, String cliente) throws EmailException{
        Date data = new Date();
        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.googlemail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("teste.email.jhonny", "123!@#teste"));
        email.setSSLOnConnect(true);
        email.setFrom("teste.email.jhonny@gmail.com");
        email.setSubject("Conserto realizado - NãoteBuk");
        email.setMsg(cliente+", o pedido nº "+pedido+" está pronto. "
                + "Já pode retirá-lo na loja.");
        email.setSentDate(data);
        email.addTo(endereco);
        email.send();
    }
    
}
