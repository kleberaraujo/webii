USE cineclub;

CREATE SEQUENCE id_filme;
CREATE SEQUENCE id_noticia;
CREATE SEQUENCE id_coment;
CREATE SEQUENCE id_promo;
CREATE SEQUENCE id_sessao;
CREATE SEQUENCE id_compra;
CREATE SEQUENCE id_ing;
CREATE SEQUENCE id_user;

create table usuario(
    coduser integer default nextval('id_user'),
    cpf varchar(11) not null,
    nome varchar(20) not null,
    sobrenome varchar(50) not null,
    rg varchar(9) not null,
    sexo char(1),
    email varchar(255) not null unique,
    senha varchar(255) not null,
    img varchar(255),
    admin varchar(1),
    CONSTRAINT pk_cliente PRIMARY KEY(coduser)
);

create table filme(
    nfilme integer default nextval('id_filme'),
    nome varchar(50),
    diretor varchar(30),
    duracao integer,
    idademin integer,
    imagem varchar(150),
    video varchar(150),
    sinopse text,
    curiosidades text,
    CONSTRAINT pk_filme PRIMARY KEY (nfilme)
);

create table noticia(
    nnot integer default nextval('id_noticia'),
    titulo varchar(200),
    imagem varchar(255),
    video varchar(150),
    texto text,
    data varchar(10),
    CONSTRAINT pk_noticia PRIMARY KEY (nnot)
);

create table genero(
    coduser integer,
    nfilme integer,
    nnot integer,
    npromo integer,
    acao char,
    animacao char,
    aventura char,
    bio char,
    comedia char,
    drama char,
    fantasia char,
    ficcao char,
    horror char,
    romance char,
    suspense char
);

create table amigo(
    coduser integer,
    amigo integer,
    CONSTRAINT pk_amigo PRIMARY KEY (coduser, amigo),
    CONSTRAINT fk_user FOREIGN KEY (coduser) REFERENCES usuario(coduser) ON DELETE CASCADE,
    CONSTRAINT fk_amigo FOREIGN KEY (amigo) REFERENCES usuario(coduser) ON DELETE CASCADE
);

create table sala(
    nsala integer not null,
    capacidade integer not null,
    CONSTRAINT pk_sala PRIMARY KEY (nsala)
);

create table promocao(
    npromo integer default nextval('id_promo'),
    descricao varchar(100),
    desconto float,
    ativo char,
    CONSTRAINT pk_promo PRIMARY KEY (npromo)
);

create table sessao(
    nsessao integer default nextval('id_sessao'),
    nfilme integer,
    nsala integer,
    data Date,
    hinit Time,
    hfin Time,
    valor float,
    CONSTRAINT pk_sessao PRIMARY KEY (nsessao),
    CONSTRAINT fk_filme FOREIGN KEY(nfilme) REFERENCES filme(nfilme) ON DELETE CASCADE,
    CONSTRAINT fk_sala FOREIGN KEY(nsala) REFERENCES sala(nsala) ON DELETE CASCADE
);

create table compra(
    ncompra integer default nextval('id_compra'),
    coduser integer,
    data Date,
    vtotal float,
    CONSTRAINT pk_compra PRIMARY KEY (ncompra),
    CONSTRAINT fk_user FOREIGN KEY (coduser) REFERENCES usuario(coduser) ON DELETE CASCADE,
);

create table compra_ing(
    ning integer default nextval('id_ing'),
    ncompra integer not null,
    nsessao integer not null,
    npromo integer,
    meia char,
    valor_ing float,
    CONSTRAINT pk_ing PRIMARY KEY (ning),
    CONSTRAINT fk_compra FOREIGN KEY (ncompra) REFERENCES compra(ncompra),
    CONSTRAINT fk_sessao FOREIGN KEY (nsessao) REFERENCES sessao(nsessao)
);

create table comentario(
    ncoment integer default nextval('id_coment'),
    nfilme integer,
    nnot integer,
    coduser integer,
    comentario text,
    CONSTRAINT pk_comentario PRIMARY KEY (ncoment),
    CONSTRAINT fk_filme FOREIGN KEY(nfilme) REFERENCES filme(nfilme),
    CONSTRAINT fk_noticia FOREIGN KEY(nnot) REFERENCES noticia(nnot),
    CONSTRAINT fk_noticia FOREIGN KEY(coduser) REFERENCES noticia(coduser)
);

ALTER SEQUENCE id_filme OWNER TO cineadmin;
ALTER SEQUENCE id_noticia OWNER TO cineadmin;
ALTER SEQUENCE id_coment OWNER TO cineadmin;
ALTER SEQUENCE id_promo OWNER TO cineadmin;
ALTER SEQUENCE id_sessao OWNER TO cineadmin;
ALTER SEQUENCE id_compra OWNER TO cineadmin;
ALTER SEQUENCE id_ing OWNER TO cineadmin;
ALTER SEQUENCE id_user OWNER TO cineadmin;
ALTER TABLE usuario OWNER TO cineadmin;
ALTER TABLE comentario OWNER TO cineadmin;
ALTER TABLE compra OWNER TO cineadmin;
ALTER TABLE compra_ing OWNER TO cineadmin;
ALTER TABLE sessao OWNER TO cineadmin;
ALTER TABLE promocao OWNER TO cineadmin;
ALTER TABLE sala OWNER TO cineadmin;
ALTER TABLE amigo OWNER TO cineadmin;
ALTER TABLE genero OWNER TO cineadmin;
ALTER TABLE noticia OWNER TO cineadmin;
ALTER TABLE filme OWNER TO cineadmin;