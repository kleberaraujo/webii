<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${acesso != 0}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${empty login}">     
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>	
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nãotebuk - Cadastro de usuários</title>
            <link rel="stylesheet" type="text/css" href="./css/style.css">
            <script type="text/javascript" src="./javascript/funcoes.js"></script>
            <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
	</head>
	<body>	
            <h1>Cadastro de Usuários</h1>
            <div id="main_box">
                <form name="cadastroPedido" id="f_cpedido" action="Processar?action=inserirUsuario" method="post">
                    <div class="f_row">
                        <label class="l_cadastro">Login:</label>
                        <input type="text" name="login" class="i_text" maxlength="80" required/>
                    </div>
                    <div class="f_row">
                        <label class="l_cadastro">Tipo de usuário:</label>
                        <select name="tipo" size="1" class="combo_user">
                                <option value="0">Gerente
                                <option value="1">Técnico			
                        </select>
                    </div>
                    <div class="f_row">
                        <label class="l_cadastro">Email:</label>
                        <input type="email" name="email" class="i_text" maxlength="80" required/>
                    </div>
                    <div class="f_row">
                        <label class="l_cadastro">Senha:</label>
                        <input type="password" name="senha" class="i_text" maxlength="80" required/>
                    </div>
                    <div class="f_row">
                        <label class="l_cadastro">Repetir senha:</label>
                        <input type="password" name="Csenha" class="i_text" maxlength="80" required/>
                    </div>
                    <div class="f_row">
                        <input type="button" id="b_salvar" onclick="cancelarCadastroGerente();" name="b_cancelar" value="Cancelar"/>
                        <input type="submit" id="b_salvar" name="b_salvar" value="Salvar" />
                    </div>
                </form>
            </div>
	</body>
</html>