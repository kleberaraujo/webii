
package Beans;

import java.io.Serializable;

public class Usuario implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private int id;
    private String login;
    private String senha;
    private String email;
    private int acesso;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public Usuario() {}

    public Usuario(String login, String senha, String email, int acesso) {
        this.login = login;
        this.senha = senha;
        this.email = email;
        this.acesso = acesso;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getters e Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAcesso() {
        return acesso;
    }

    public void setAcesso(int acesso) {
        this.acesso = acesso;
    }
    // </editor-fold>
  
}
