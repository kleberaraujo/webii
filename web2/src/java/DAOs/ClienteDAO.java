
package DAOs;

import Infra.ConnectionFactory;
import Beans.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO{
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public ClienteDAO() {}
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public void Salvar(Cliente pCliente) throws Exception{
        try {
            //java.sql.Date sqlDate = new java.sql.Date(pCliente.getDataNasc().getTime());
            Date sqlDate = new Date(pCliente.getDataNasc().getTime());
            
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("INSERT INTO Cliente (nome,cpf,dataNasc,sexo,cep,cidade,endereco,email) "
                    + "VALUES (?,?,?,?,?,?,?,?)");
            stmt.setString(1, pCliente.getNome());
            stmt.setString(2, pCliente.getCpf());
            stmt.setDate(3, sqlDate);
            stmt.setString(4, pCliente.getSexo());
            stmt.setString(5, pCliente.getCep());
            stmt.setInt(6, pCliente.getCidade());
            stmt.setString(7, pCliente.getEndereco());
            stmt.setString(8, pCliente.getEmail());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public int Verificar(String pid) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT idCliente FROM Cliente WHERE=(?)");
            rs = stmt.executeQuery();
            return rs.getInt("idCliente");
        }
        catch(Exception e){
            throw new Exception("Cliente não encontrado");
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public String BuscarEmail(int pid) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT email FROM cliente cli "
                    + "INNER JOIN conserto co ON co.idCliente = cli.idCliente "
                    + "WHERE idConserto = ?");
            stmt.setInt(1, pid);
            rs = stmt.executeQuery();
            rs.next();
            return rs.getString("email");
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<String> listarClientes() throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT idcliente ,nome FROM cliente ORDER BY nome");
            rs = stmt.executeQuery();
            List <String> lista = new ArrayList();
            while(rs.next()){
                lista.add(rs.getString("idcliente")+" - "+rs.getString("nome"));
            }
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<String> listarCidades() throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT idcidade, nome FROM cidade");
            rs = stmt.executeQuery();
            List <String> lista = new ArrayList();
            while(rs.next()){
                lista.add(rs.getString("idcidade")+" - "+rs.getString("nome"));
            }
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    // </editor-fold>
    
}
