PGDMP                          r            ntembuk    9.3.4    9.3.1 ,    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    25272    ntembuk    DATABASE     �   CREATE DATABASE ntembuk WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';
    DROP DATABASE ntembuk;
             ntembuk    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    11750    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    182            �            1259    25281    cidade    TABLE     F   CREATE TABLE cidade (
    idcidade integer NOT NULL,
    nome text
);
    DROP TABLE public.cidade;
       public         ntembuk    false    5            �            1259    25275 	   idcliente    SEQUENCE     k   CREATE SEQUENCE idcliente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
     DROP SEQUENCE public.idcliente;
       public       ntembuk    false    5            �            1259    25302    cliente    TABLE     0  CREATE TABLE cliente (
    idcliente integer DEFAULT nextval('idcliente'::regclass) NOT NULL,
    nome character varying(20),
    cpf character varying(14),
    datanasc date,
    sexo character(1),
    cep character varying(10),
    cidade integer,
    endereco text,
    email character varying(50)
);
    DROP TABLE public.cliente;
       public         ntembuk    false    171    5            �            1259    25277 
   idconserto    SEQUENCE     l   CREATE SEQUENCE idconserto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.idconserto;
       public       ntembuk    false    5            �            1259    25320    conserto    TABLE     �  CREATE TABLE conserto (
    idcliente integer NOT NULL,
    idconserto integer DEFAULT nextval('idconserto'::regclass) NOT NULL,
    idtipo integer,
    fabricante character varying(20),
    modelo character varying(30),
    dataentrada date,
    dataentregaprevisao date,
    dataentregareal date,
    valor double precision,
    problema character varying(100),
    descricao text,
    status integer NOT NULL,
    datainicial date,
    datafinal date,
    observacao text
);
    DROP TABLE public.conserto;
       public         ntembuk    false    172    5            �            1259    25279    idcidade    SEQUENCE     j   CREATE SEQUENCE idcidade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.idcidade;
       public       ntembuk    false    5            �            1259    25273    idstatus    SEQUENCE     j   CREATE SEQUENCE idstatus
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.idstatus;
       public       ntembuk    false    5            �            1259    41398    idtipo    SEQUENCE     h   CREATE SEQUENCE idtipo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.idtipo;
       public       postgres    false    5            �            1259    25294    status    TABLE     q   CREATE TABLE status (
    idstatus integer DEFAULT nextval('idstatus'::regclass) NOT NULL,
    descricao text
);
    DROP TABLE public.status;
       public         ntembuk    false    170    5            �            1259    25343    tecnicoconserto    TABLE     l   CREATE TABLE tecnicoconserto (
    idconserto integer NOT NULL,
    login character varying(50) NOT NULL
);
 #   DROP TABLE public.tecnicoconserto;
       public         ntembuk    false    5            �            1259    25315    tipoconserto    TABLE     �   CREATE TABLE tipoconserto (
    idtipo integer DEFAULT nextval('idtipo'::regclass) NOT NULL,
    descricao character varying(20)
);
     DROP TABLE public.tipoconserto;
       public         ntembuk    false    181    5            �            1259    25289    usuario    TABLE     �   CREATE TABLE usuario (
    login character varying(50) NOT NULL,
    senha character varying(32),
    email character varying(50),
    tipoacesso integer
);
    DROP TABLE public.usuario;
       public         ntembuk    false    5            �          0    25281    cidade 
   TABLE DATA                     public       ntembuk    false    174   L+       �          0    25302    cliente 
   TABLE DATA                     public       ntembuk    false    177   �,       �          0    25320    conserto 
   TABLE DATA                     public       ntembuk    false    179   .       �           0    0    idcidade    SEQUENCE SET     /   SELECT pg_catalog.setval('idcidade', 2, true);
            public       ntembuk    false    173            �           0    0 	   idcliente    SEQUENCE SET     0   SELECT pg_catalog.setval('idcliente', 4, true);
            public       ntembuk    false    171            �           0    0 
   idconserto    SEQUENCE SET     1   SELECT pg_catalog.setval('idconserto', 6, true);
            public       ntembuk    false    172            �           0    0    idstatus    SEQUENCE SET     /   SELECT pg_catalog.setval('idstatus', 4, true);
            public       ntembuk    false    170            �           0    0    idtipo    SEQUENCE SET     -   SELECT pg_catalog.setval('idtipo', 2, true);
            public       postgres    false    181            �          0    25294    status 
   TABLE DATA                     public       ntembuk    false    176   */       �          0    25343    tecnicoconserto 
   TABLE DATA                     public       ntembuk    false    180   �/       �          0    25315    tipoconserto 
   TABLE DATA                     public       ntembuk    false    178   �/       �          0    25289    usuario 
   TABLE DATA                     public       ntembuk    false    175   .0       H           2606    25288 
   pkidcidade 
   CONSTRAINT     N   ALTER TABLE ONLY cidade
    ADD CONSTRAINT pkidcidade PRIMARY KEY (idcidade);
 ;   ALTER TABLE ONLY public.cidade DROP CONSTRAINT pkidcidade;
       public         ntembuk    false    174    174            N           2606    41375    pkidcliente 
   CONSTRAINT     Q   ALTER TABLE ONLY cliente
    ADD CONSTRAINT pkidcliente PRIMARY KEY (idcliente);
 =   ALTER TABLE ONLY public.cliente DROP CONSTRAINT pkidcliente;
       public         ntembuk    false    177    177            R           2606    41383    pkidconserto 
   CONSTRAINT     T   ALTER TABLE ONLY conserto
    ADD CONSTRAINT pkidconserto PRIMARY KEY (idconserto);
 ?   ALTER TABLE ONLY public.conserto DROP CONSTRAINT pkidconserto;
       public         ntembuk    false    179    179            L           2606    41391 
   pkidstatus 
   CONSTRAINT     N   ALTER TABLE ONLY status
    ADD CONSTRAINT pkidstatus PRIMARY KEY (idstatus);
 ;   ALTER TABLE ONLY public.status DROP CONSTRAINT pkidstatus;
       public         ntembuk    false    176    176            P           2606    25319    pkidtipo 
   CONSTRAINT     P   ALTER TABLE ONLY tipoconserto
    ADD CONSTRAINT pkidtipo PRIMARY KEY (idtipo);
 ?   ALTER TABLE ONLY public.tipoconserto DROP CONSTRAINT pkidtipo;
       public         ntembuk    false    178    178            J           2606    25293    pklogin 
   CONSTRAINT     I   ALTER TABLE ONLY usuario
    ADD CONSTRAINT pklogin PRIMARY KEY (login);
 9   ALTER TABLE ONLY public.usuario DROP CONSTRAINT pklogin;
       public         ntembuk    false    175    175            S           2606    25310 
   fkidcidade    FK CONSTRAINT     i   ALTER TABLE ONLY cliente
    ADD CONSTRAINT fkidcidade FOREIGN KEY (cidade) REFERENCES cidade(idcidade);
 <   ALTER TABLE ONLY public.cliente DROP CONSTRAINT fkidcidade;
       public       ntembuk    false    177    1864    174            U           2606    41376    fkidcliente    FK CONSTRAINT     p   ALTER TABLE ONLY conserto
    ADD CONSTRAINT fkidcliente FOREIGN KEY (idcliente) REFERENCES cliente(idcliente);
 >   ALTER TABLE ONLY public.conserto DROP CONSTRAINT fkidcliente;
       public       ntembuk    false    1870    179    177            X           2606    41384    fkidconserto    FK CONSTRAINT     {   ALTER TABLE ONLY tecnicoconserto
    ADD CONSTRAINT fkidconserto FOREIGN KEY (idconserto) REFERENCES conserto(idconserto);
 F   ALTER TABLE ONLY public.tecnicoconserto DROP CONSTRAINT fkidconserto;
       public       ntembuk    false    180    179    1874            W           2606    25351 
   fkidstatus    FK CONSTRAINT     n   ALTER TABLE ONLY tecnicoconserto
    ADD CONSTRAINT fkidstatus FOREIGN KEY (login) REFERENCES usuario(login);
 D   ALTER TABLE ONLY public.tecnicoconserto DROP CONSTRAINT fkidstatus;
       public       ntembuk    false    175    180    1866            V           2606    41392 
   fkidstatus    FK CONSTRAINT     j   ALTER TABLE ONLY conserto
    ADD CONSTRAINT fkidstatus FOREIGN KEY (status) REFERENCES status(idstatus);
 =   ALTER TABLE ONLY public.conserto DROP CONSTRAINT fkidstatus;
       public       ntembuk    false    179    176    1868            T           2606    25338    fkidtipo    FK CONSTRAINT     l   ALTER TABLE ONLY conserto
    ADD CONSTRAINT fkidtipo FOREIGN KEY (idtipo) REFERENCES tipoconserto(idtipo);
 ;   ALTER TABLE ONLY public.conserto DROP CONSTRAINT fkidtipo;
       public       ntembuk    false    178    1872    179            �   �  x����N�0��>�mm�$-�Ԗ���M�~M�b��W�o��l�y1|��X/[�|��ߟ��2��B�-�;�t�������z�Q���8���t��G�јU�9%�hw/[3A��[S1A`i@��T�J!���h�Oi΀sڨzsˑҧ�cR ,i'��LS����a��\��\g7��o���C1�L�^�ǌ9�-Iq���/pk�<k9Q����<�rB	�����u�9�0K8�F�z#���+�%X'�E�ć�c�q��N��tI�.�J̕�Շ�l�sy����X���A�����W&&��̞���׬��F>g���b�\�1(P'蒋s+���2v�D���񇇔�jp�}6.��	n��>����������F��Ij�~њ#�      �   "  x����N1��<�܀d۴�)]�P�D�{�VY\Z�e�面!��&���t:�|�b�<Z�b�~�6>=���7, �=ywx+��Gl�
��g��?�kj[S��O�Q��mӎ��v�2��,`x��!����pU)�fʪ�"�aB1�s��݋R3����P%�66�u$ݔ"|�-(Qa~���n9i�޳:wq?��H|�;��-J�\aI�L�61at��f��^��6ϙ9|wL6��@�o�Ƨ�j�5�hT
i��L��BO��FF��+����U�� �ų��      �     x�ݒ�j�0��y
�҂N�u��v�!2X�ݕD�����ti;vخ#����e�ݼwo�Z��3յZ�	$��K9�A���q��s�h;�\�0 ;vx􉽣Ay�wEG����;�h:F�6^̇}�p�Iժh��LLm�o������o�0[X
���|��6��x�d\�Zg�bU響mR�f̿��8I���>	(xfc�)��׍������k5~ʿxM.{
�Z�a��y&���%���!�      �   n   x���v
Q���W(.I,)-V��L��tRR���2��5�}B]�4u��R�J��5��<I�nҞ�����G�	�@�Rs2�S�3�h�k^IQjzi*� .. ��Q�      �   
   x���          �   \   x���v
Q���W(�,�O��+N-*�W��L�uRR���2��5�}B]�4u�]R��K��5��<�1�h�_~IjR~~6�.. g�+�      �   �   x�ŐAj�0E�>�vN�ٖb�n�E.!�&�~4#'.�T,����29@!�nf`x����xڿ�Ys<��)L0t��z�\ƂuWȘ�A�gl�>=�������?�U�|��}�K�BU���1�H�Z��P@V"����B�_�����󕯟��1�Äb��	�!#₰�ܒ!U�%.ed��Άq��G���?������b��m�̻��a��W?����(DQ*�Fϫ��(��m[I%������ޝ$?����     