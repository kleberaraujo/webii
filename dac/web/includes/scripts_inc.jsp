<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" media="all" href="css/style.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->		

<!-- JS -->
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/tabs.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/jquery.columnizer.min.js"></script>

<!-- Isotope -->
<script src="js/jquery.isotope.min.js"></script>

<!-- Lof slider -->
<script src="js/jquery.easing.js"></script>
<script src="js/lof-slider.js"></script>
<link rel="stylesheet" href="css/lof-slider.css" media="all"  />
<!-- ENDS slider -->

<!-- superfish -->
<link rel="stylesheet" media="screen" href="css/superfish.css" />
<script  src="js/superfish-1.4.8/js/hoverIntent.js"></script>
<script  src="js/superfish-1.4.8/js/supersubs.js"></script>
<!-- ENDS superfish -->

<!-- prettyPhoto -->
<script  src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css"  media="screen" />
<!-- ENDS prettyPhoto -->

<!-- poshytip -->
<link rel="stylesheet" href="js/poshytip-1.1/src/tip-twitter/tip-twitter.css"  />
<link rel="stylesheet" href="js/poshytip-1.1/src/tip-yellowsimple/tip-yellowsimple.css"  />
<script  src="js/poshytip-1.1/src/jquery.poshytip.min.js"></script>
<!-- ENDS poshytip -->

<!-- JCarousel -->
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<link rel="stylesheet" media="screen" href="css/carousel.css" /> 
<!-- ENDS JCarousel -->

<!-- GOOGLE FONTS -->
<link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>

<!-- modernizr -->
<script src="js/modernizr.js"></script>

<!-- SKIN -->
<link rel="stylesheet" media="all" href="css/skin.css"/>

<!-- flexslider -->
<link rel="stylesheet" href="css/flexslider.css" >
<script src="js/jquery.flexslider.js"></script>
	