package Beans;

import java.io.Serializable;
import java.sql.Date;

public class Relatorio implements Serializable{
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    protected Cliente cliente = new Cliente();
    protected Conserto conserto = new Conserto();
    protected Usuario usuario = new Usuario();
    private int idConserto;
    private String nomeCliente;
    private String tipo;
    private String fabricante;
    private String modelo;
    private Date dataEntrada;       
    private Date dataEntregaPrevisao;
    private double valor;       
    private String problema;       
    private String descricao;      
    private String status;       
    private String tecnico;
    private Date datainicial;
    private Date datafinal;
    private String observacao;
    private Date dataEntregaReal;
    private String atraso;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public Relatorio(int idConserto, String nomeCliente, int tipo, String fabricante,
                        String modelo, Date dataEntrada, Date dataEntregaPrevisao, double valor,
                        String problema, String descricao, int status, String tecnico, Date datainicial, 
                        Date datafinal, String observacao, Date dataEntregaReal, int idCliente,
                        String cpfCliente, Date dataNascCliente, String sexoCliente, String cepCliente,
                        String cidadeCliente, String enderecoCliente, String emailCliente, int diasAtrasados) {
        this.conserto = new Conserto(idConserto, tipo, idCliente, fabricante, modelo, dataEntrada,
                dataEntregaPrevisao, dataEntregaReal, valor, problema, descricao, status, datainicial,
                datafinal, observacao, diasAtrasados);
        this.cliente = new Cliente(nomeCliente, cpfCliente, dataNascCliente, sexoCliente, cepCliente,
                cidadeCliente, enderecoCliente, emailCliente);
        this.usuario = new Usuario(tecnico, null, null, 1);
    }

    public Relatorio(int idConserto, String nomeCliente, String tipo, String modelo, Date dataEntrada, Date dataEntregaPrevisao) {
        this.idConserto = idConserto;
        this.nomeCliente = nomeCliente;
        this.tipo = tipo;
        this.modelo = modelo;
        this.dataEntrada = dataEntrada;
        this.dataEntregaPrevisao = dataEntregaPrevisao;
    }

    public Relatorio(int idConserto, String nomeCliente, double valor, String modelo, Date dataEntregaPrevisao, String atraso) {
        this.idConserto = idConserto;
        this.nomeCliente = nomeCliente;
        this.valor = valor;
        this.modelo = modelo;
        this.dataEntregaPrevisao = dataEntregaPrevisao;
        this.atraso = atraso;
    }
    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getters e Setters">
    public int getIdConserto() {
        return idConserto;
    }

    public void setIdConserto(int idConserto) {
        this.idConserto = idConserto;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getTipo() {
        return tipo;
    }

    public String getAtraso() {
        return atraso;
    }

    public void setAtraso(String atraso) {
        this.atraso = atraso;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataEntregaPrevisao() {
        return dataEntregaPrevisao;
    }

    public void setDataEntregaPrevisao(Date dataEntregaPrevisao) {
        this.dataEntregaPrevisao = dataEntregaPrevisao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTecnico() {
        return tecnico;
    }

    public void setTecnico(String tecnico) {
        this.tecnico = tecnico;
    }

    public Date getDatainicial() {
        return datainicial;
    }

    public void setDatainicial(Date datainicial) {
        this.datainicial = datainicial;
    }

    public Date getDatafinal() {
        return datafinal;
    }

    public void setDatafinal(Date datafinal) {
        this.datafinal = datafinal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDataEntregaReal() {
        return dataEntregaReal;
    }

    public void setDataEntregaReal(Date dataEntregaReal) {
        this.dataEntregaReal = dataEntregaReal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Conserto getConserto() {
        return conserto;
    }

    public void setConserto(Conserto conserto) {
        this.conserto = conserto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }   
    //</editor-fold>
    
}