package Infra;

import Beans.Cliente;
import Beans.Conserto;
import Beans.Relatorio;
import Beans.Usuario;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JasperRunManager;

@WebServlet(name = "Processar", urlPatterns = {"/Processar"})
public class Processar extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        
        // <editor-fold defaultstate="collapsed" desc="Iniciar Servelt">
        String action = "";
        String action2 = "";
        if(request.getParameter("action") != null)
            action = request.getParameter("action");
        if(action.contains(" ")){
            String actions[] = action.split(" ");
            action = actions[0];
            action2 = actions[1];
        }
        HttpSession sessao = request.getSession();
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Efetuar Login">
        if(action.equals("efetuarLogin")){
            
            try{
                //Atribuir Valores
                String login = request.getParameter("login");
                String senha = request.getParameter("senha");
                
                //Criar Sessao
                String acesso = Facade.verificaUsusario(login, senha);
                //Usuário Existente
                if(acesso != null){
                    sessao.setAttribute("login", login);
                    sessao.setAttribute("acesso", acesso);
                    
                    //Redireciona para pagina correspondente
                    if(acesso.equals("0")){
                        response.sendRedirect("./Processar?action=MainGerente");
                    }
                    else{
                        response.sendRedirect("./Processar?action=MainTecnico");
                    }    
                }
                //Usuário Inexistente
                else{
                    response.sendRedirect("/NtemBuk/");
                }
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Efetuar Logout">
        else if(action.equals("efetuarLogout")){
            
            try{
                //Destroi sessão
                sessao.invalidate();
                
                //Redireciona para Login
                response.sendRedirect("./Login.jsp");
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Verificar Login (Servlet)">
        else if(sessao.getAttribute("login") == null)
            response.sendRedirect("/NtemBuk/");
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Inserir Cliente">
        else if(action.equals("inserirCliente")){
            
            try{
                //Atribuir valores
                String nome = request.getParameter("nome");
                String cpf = request.getParameter("cpf");
                String dataTemp = request.getParameter("dataNascimento");
                String sexo = request.getParameter("sexo");
                String cep = request.getParameter("cep");
                String cidadeTemp = request.getParameter("cidade");
                String endereco = request.getParameter("endereco");
                String email = request.getParameter("email");

                String[] ct = cidadeTemp.split(" - ");
                int cidade = Integer.parseInt(ct[0]);
                dataTemp = dataTemp.substring(8, 10) + "/" + dataTemp.substring(5, 7) + "/" + dataTemp.substring(0, 4);
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date dataNasc = new Date(format.parse(dataTemp).getTime());
                
                //Instanciar
                Cliente c = new Cliente(nome,cpf,dataNasc,sexo,cep,cidade,endereco,email);

                //Salvar
                Facade.inserirCliente(c);
                
                //Redireciona
                response.sendRedirect("./Processar?action=MainTecnico");
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Inserir Conserto">
        else if(action.equals("inserirConserto")){
            
            try{
                //Atribuir Valores
                String clienteTemp = request.getParameter("cliente");
                String tipoTemp = request.getParameter("tipo");
                String fabricante = request.getParameter("fabricante");
                String modelo = request.getParameter("modelo");
                String dataEntradaTemp = request.getParameter("dataEntrada");
                String dataEntregaTemp = request.getParameter("dataEntrega");
                String valorTemp = request.getParameter("valor");
                String problema = request.getParameter("problema");
                String descricaoProblema = request.getParameter("descricaoProblema");
                
                String[] idTemp = clienteTemp.split(" - ");
                int idCliente = Integer.parseInt(idTemp[0]);
                String[] idTipoTemp = tipoTemp.split(" - ");
                int tipo = Integer.parseInt(idTipoTemp[0]);
                Data d = new Data();
                Date dataEntrada = d.ConverterData(dataEntradaTemp);
                Date dataEntrega = d.ConverterData(dataEntregaTemp);
                valorTemp = valorTemp.replace(",", ".");
                double valor = Double.parseDouble(valorTemp);
                
                //Instanciar
                Conserto c = new Conserto(tipo,idCliente,fabricante,modelo,dataEntrada,dataEntrega,valor,problema,descricaoProblema,1);
                
                //Salvar
                Facade.inserirConserto(c);
                
                request.setAttribute("Titulo", "Sucesso!");
                request.setAttribute("Mensagem", "Conserto Nº " + c.getIdConserto() + " Cadastrado!");
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
                
                //Redirecionar
                response.sendRedirect("./Processar?action=MainTecnico");
            }
            catch(Exception e){
                request.setAttribute("Titulo", "");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Inserir Usuario">
        else if(action.equals("inserirUsuario")){
            
            try{
                //Atribuir  Valores
                String login = request.getParameter("login");
                String email = request.getParameter("email");
                String senha = request.getParameter("senha");
                String tipoTemp = request.getParameter("tipo");

                int tipo = Integer.parseInt(tipoTemp);
                
                //Instanciar
                Usuario u = new Usuario(login,senha,email,tipo);
                
                //Salvar
                Facade.inserirUsuario(u);
                
                //Redirecionar
                response.sendRedirect("./Processar?action=MainGerente");
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Cadastrar Cliente">
        else if(action.equals("CadastrarCliente")){
            
            try{
                //Set Lista de Clientes
                List <String> lista = new ArrayList();
                lista = Facade.listarCidades();
                if(lista == null){
                    lista = new ArrayList();
                    lista.add("");
                    request.setAttribute("ListaCidades", lista);
                }
                else{
                    request.setAttribute("ListaCidades", lista);
                }
                        
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./CadastroCliente.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
                
        // <editor-fold defaultstate="collapsed" desc="Cadastrar Conserto">
        else if(action.equals("CadastrarConserto")){
            
            try{
                //Set Lista de Clientes
                List <String> lista = new ArrayList();
                lista = Facade.listarClientes();
                request.setAttribute("ListaClientes", lista);
                
                //Set Tipo
                lista = new ArrayList<String>();
                lista = Facade.listarTipo();
                request.setAttribute("ListaTipo", lista);
                
                //Set Data
                Data d = new Data();
                String dataAtual = d.DataAtual();
                String dataProxima = d.DataProxima();
                List <String> datas = new ArrayList();
                datas.add(dataAtual);
                datas.add(dataProxima);
                request.setAttribute("Datas", datas);
                        
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./CadastroPedido.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Main Gerente">
        else if(action.equals("MainGerente")){
            
            try{
                List<Integer> lista = new ArrayList<Integer>();
                lista = Facade.listarTotalConsertos();
                request.setAttribute("TotalConsertos", lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./MainGerente.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>    
        
        // <editor-fold defaultstate="collapsed" desc="Main Tecnico">
        else if(action.equals("MainTecnico")){
            
            try{
                
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarPedidosEmAberto();
                int total = lista.size();
                Data d = new Data();
                Date dataAtual = new Date(d.DataAtualDate().getTime());
                Date dataProxima = d.ConverterData(d.DataProximaDate());
                
                //Set Atributo
                request.setAttribute("Total",total);
                request.setAttribute("dataAtual",dataAtual);
                request.setAttribute("dataProxima",dataProxima);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./MainTecnico.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Meus Concertos">
        else if(action.equals("MeusConcertos")){
            
            try{
                String login = sessao.getAttribute("login").toString();
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarMeusConsertos(login);
                
                //Set Atributo
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./MainTecnico.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("./Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Pesquisar">
        else if(action.equals("Pesquisar")){            
            try{
                String pesquisa = request.getParameter("pesquisaId");
                List <Relatorio> lista = new ArrayList<Relatorio>();
                switch(pesquisa){
                    case "1":
                        String p = request.getParameter("pesquisaNumero");
                        try{
                            int pesquisaNumero = Integer.parseInt(p);
                            lista = Facade.pesquisarConserto(pesquisaNumero);
                            int total1 = lista.size();
                            request.setAttribute("Tipo", 1);
                            request.setAttribute("Total", total1);
                            request.setAttribute("Relatorio", lista);
                        }
                        catch(Exception e){
                            //Do Nothing
                        }
                        break;
                    case "2":
                        String d1 = request.getParameter("pesquisaDataInicial");
                        String d2 = request.getParameter("pesquisaDataFinal");
                        Data data = new Data();
                        Date dataInicial = data.ConverterData(d1);
                        Date dataFinal = data.ConverterData(d2);
                        lista = Facade.pesquisarConserto(dataFinal.toLocaleString(), dataInicial.toLocaleString());
                        int total2 = lista.size();
                        request.setAttribute("Tipo", 1);
                        request.setAttribute("Total", total2);
                        request.setAttribute("Relatorio", lista);
                        break;
                    case "3":
                        String pesquisaLivre = request.getParameter("pesquisaLivre");
                        lista = Facade.pesquisarConserto(pesquisaLivre);
                        int total3 = lista.size();
                        request.setAttribute("Tipo", 1);
                        request.setAttribute("Total", total3);
                        request.setAttribute("Relatorio", lista);
                        break;
                    case "4":
                        d1 = request.getParameter("pesquisaDataInicial");
                        d2 = request.getParameter("pesquisaDataFinal");
                        data = new Data();
                        dataInicial = data.ConverterData(d1);
                        dataFinal = data.ConverterData(d2);
                        lista = Facade.pesquisarConsertosRealz(dataFinal.toLocaleString(), dataInicial.toLocaleString());
                        total2 = lista.size();
                        request.setAttribute("Tipo", 4);
                        request.setAttribute("pdata", true);
                        request.setAttribute("d1", dataInicial);
                        request.setAttribute("d2", dataFinal);
                        request.setAttribute("Total", total2);
                        request.setAttribute("Relatorio", lista);
                        break;
                }
                //Redireciona
                String u = (String) sessao.getAttribute("acesso");
                if(u.equals("0")){
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/Relatorios.jsp");
                    rd.forward(request, response);
                }
                else{
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/MainTecnico.jsp");
                    rd.forward(request, response);
                }
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Visualizar Conserto">
        else if(action.equals("Conserto")){
            
            try{
                //Get 
                int id = Integer.parseInt(action2);
                Relatorio consertoTec = Facade.consultarConserto(id);
                List <String> status = new ArrayList<String>();
                status = Facade.listarStatus();
                String usuarioSessao = (String) sessao.getAttribute("login");
                //Set
                request.setAttribute("Conserto", consertoTec);
                request.setAttribute("Usuario", usuarioSessao);
                request.setAttribute("Status", status);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./FinalizarPedido.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Alterar Conserto">
        else if(action.equals("AlterarConserto")){
            
            try{
                //Atribuir Valores
                String idTemp = request.getParameter("id");
                String statusTemp = request.getParameter("s_status");
                String tecnico = request.getParameter("tecnico");
          
                int id = Integer.parseInt(idTemp);
                int status = Integer.parseInt(statusTemp);
                Data data = new Data();
                
                switch(status){
                    case 1:
                        Facade.alterarPedidoAberto(id);
                        break;
                    case 2:
                        String data1Temp = request.getParameter("dataInicial");
                        Date dataInicial = data.ConverterData2(data1Temp);
                        Facade.alterarConsertoAndamento(id, dataInicial, tecnico, status);
                        break;
                    case 3:
                        String data2Temp = request.getParameter("dataFinal");
                        String observacao = request.getParameter("observacao");
                        Date dataFinal = data.ConverterData2(data2Temp);
                        Facade.alterarConsertoRealizado(id, dataFinal, observacao, status);
                        String cliente = request.getParameter("cliente");
                        Email.enviarEmail(Facade.buscarEmail(id), Integer.toString(id), cliente);
                        break;
                    case 4:
                        String data3Temp = request.getParameter("dataEntregaReal");
                        Date dataEntrega = data.ConverterData2(data3Temp);
                        Facade.alterarConsertoEntregue(id, dataEntrega, status);
                        break;
                }
                request.setAttribute("Titulo", "Mensagem");
                request.setAttribute("Mensagem", "Salvo com sucesso,");
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Mensagem");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Pedidos Abertos">
        else if(action.equals("PedidosAbertos")){
            
            try{
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarPedidosAbertos();
                int total = lista.size();
                
                //Set Atributo
                request.setAttribute("Tipo",1);
                request.setAttribute("Total",total);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./Relatorios.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Consertos em andamento">
        else if(action.equals("ConsertosEmAndamento")){
            
            try{
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarConsertosEmAndamento();
                int total = lista.size();
                
                //Set Atributo
                request.setAttribute("Tipo",2);
                request.setAttribute("Total",total);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./Relatorios.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Consertos realizados">
        else if(action.equals("ConsertosRealizados")){
            
            try{
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarConsertosRealizados();
                int total = lista.size();
                
                //Set Atributo
                request.setAttribute("Tipo",3);
                request.setAttribute("Total",total);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./Relatorios.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Consertos entregues">
        else if(action.equals("ConcertosEntregues")){
            
            try{
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarConsertosEntregues();
                int total = lista.size();
                
                //Set Atributo
                request.setAttribute("Tipo",4);
                request.setAttribute("Total",total);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./Relatorios.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Consertos Atrasados">
        else if(action.equals("ConcertosAtrasados")){
            
            try{
                List<Relatorio> lista = new ArrayList<Relatorio>();
                lista = Facade.listarConsertosAtrasados();
                int total = lista.size();
                
                //Set Atributo
                request.setAttribute("Tipo",5);
                request.setAttribute("Total",total);
                request.setAttribute("Relatorio",lista);
                
                //Redireciona
                RequestDispatcher rd = request.getRequestDispatcher("./Relatorios.jsp");                
                rd.forward(request, response);
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
            
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Gerar Relatorio">
        else if(action.equals("GerarRelatorio")){            
            try{
                String jasper = "";
                String tipo = request.getParameter("type");
                String d1 = "", d2 = "";
                d1 = request.getParameter("d2");
                d2 = request.getParameter("d1");
                HashMap<String, Object> params = new HashMap<String, Object>();
                //response.getWriter().printf(tipo);
                // Caminho físico do relatório compilado
                if(tipo.equals("atrasados")){
                    jasper = request.getContextPath() + "/relatorios/atrasados.jasper";
                }
                else if(tipo.equals("retirar")){                    
                    if(d1 == null && d2 == null ){
                        jasper = request.getContextPath() + "/relatorios/retirar.jasper";
                    }
                }
                else if(tipo.equals("datas")){
                    jasper = request.getContextPath() + "/relatorios/geral.jasper";
                    Data data = new Data();
                    Date dataInicial = data.ConverterData(d1);
                    Date dataFinal = data.ConverterData(d2);
                    params.put("dinit", dataInicial);
                    params.put("dfin", dataFinal);
                }
                //Host onde o servlet esta executando
                String host = "http://" + request.getServerName()+":"+request.getServerPort();
                Connection con = ConnectionFactory.getConnection();
                URL jasperURL = new URL(host + jasper);
                byte[] bytes = JasperRunManager.runReportToPdf(jasperURL.openStream(), params, con);
                if (bytes != null) {
                    // A página será mostrada em PDF
                    response.setContentType("relatorio.pdf");
                    // Envia o PDF para o Cliente
                    OutputStream ops = response.getOutputStream();
                    ops.write(bytes);
                }
            }
            catch(Exception e){
                request.setAttribute("Titulo", "Erro");
                request.setAttribute("Mensagem", e);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
                rd.forward(request, response);
            }
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Action Vazio">
        else{
            request.setAttribute("Titulo", "Erro");
            request.setAttribute("Mensagem", "Página não encontrada");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/Erro.jsp");
            rd.forward(request, response);
        }
        // </editor-fold>
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }
        catch(Exception e){
            Logger.getLogger(Processar.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }
        catch(Exception e){
            Logger.getLogger(Processar.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
