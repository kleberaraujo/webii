package Model;

import Controller.Comentario;
import Controller.Compra;
import Controller.Filme;
import Controller.Ingresso;
import Controller.Noticia;
import Controller.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GenericDAO {

    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public GenericDAO(){}

    public void InsereGenero(String id, String dado, List<String> generos) throws Exception {

        try {
            con = FactoryConnection.getConnection();
            //Insere as Tags
            String into = "INSERT INTO genero (" + dado ;
            String values = ") VALUES ( '" + id + " '";

            for (String opt : generos) {
                switch (opt) {
                    case "1":
                        into = into + ",acao";
                        values = values + ",'X'";
                        break;
                    case "2":
                        into = into + ",animacao";
                        values = values + ",'X'";
                        break;
                    case "3":
                        into = into + ",aventura";
                        values = values + ",'X'";
                        break;
                    case "4":
                        into = into + ",bio";
                        values = values + ",'X'";
                        break;
                    case "5":
                        into = into + ",comedia";
                        values = values + ",'X'";
                        break;
                    case "6":
                        into = into + ",drama";
                        values = values + ",'X'";
                        break;
                    case "7":
                        into = into + ",fantasia";
                        values = values + ",'X'";
                        break;
                    case "8":
                        into = into + ",ficcao";
                        values = values + ",'X'";
                        break;
                    case "9":
                        into = into + ",horror";
                        values = values + ",'X'";
                        break;
                    case "10":
                        into = into + ",romance";
                        values = values + ",'X'";
                        break;
                    case "11":
                        into = into + ",suspense";
                        values = values + ",'X'";
                        break;
                }
            }           
            stmt = con.prepareStatement(into + values + ");");
            stmt.executeUpdate();            
        }
        catch (Exception ex) {
            throw new Exception(ex);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

    List<String> getGeneros(String dado, String id) throws Exception {
        try{
            List<String> generos = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM genero WHERE " + dado + " = " + id);
            rs = stmt.executeQuery();
            while(rs.next()){
                if(rs.getString("acao")!=null){
                    generos.add("1");
                }
                if(rs.getString("animacao")!=null){
                    generos.add("2");
                }
                if(rs.getString("aventura")!=null){
                    generos.add("3");
                }
                if(rs.getString("bio")!=null){
                    generos.add("4");
                }
                if(rs.getString("comedia")!=null){
                    generos.add("5");
                }
                if(rs.getString("drama")!=null){
                    generos.add("6");
                }
                if(rs.getString("fantasia")!=null){
                    generos.add("7");
                }
                if(rs.getString("ficcao")!=null){
                    generos.add("8");
                }
                if(rs.getString("horror")!=null){
                    generos.add("9");
                }
                if(rs.getString("romance")!=null){
                    generos.add("10");
                }
                if(rs.getString("suspense")!=null){
                    generos.add("11");
                }
            }
            return generos;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void AtualizaGenero(String id, String dado, List<String> generos) throws Exception {

        try {
            con = FactoryConnection.getConnection();
            //Insere as Tags
            String sql = "UPDATE genero SET ";
            String act = "", anm = "", avt = "", bio = "", cmd = "", drm = "", fts = "", fcc = "";
            String hrr = "", rmc = "", sps = "";
            for (String opt : generos) {
                switch (opt) {
                    case "1":
                        act = "acao = 'X',";
                        break;
                    case "2":
                        anm = "animacao = 'X',";
                        break;
                    case "3":
                        avt = "aventura = 'X',";
                        break;
                    case "4":
                        bio = "bio = 'X',";
                        break;
                    case "5":
                        cmd = "comedia = 'X',";
                        break;
                    case "6":
                        drm = "drama = 'X',";
                        break;
                    case "7":
                        fts = "fantasia = 'X',";
                        break;
                    case "8":
                        fcc = "ficcao = 'X',";
                        break;
                    case "9":
                        hrr = "horror = 'X',";
                        break;
                    case "10":
                        rmc = "romance = 'X',";
                        break;
                    case "11":
                        sps = "suspense = 'X'";
                        break;
                }
            }
            
            //atualiza os que não foram marcados para que no banco fique vazio
            sql = act.equals("") ? sql + "acao = ' '," : sql + act ;
            sql = anm.equals("") ? sql + "animacao = ' '," : sql + anm ;
            sql = avt.equals("") ? sql + "aventura = ' '," : sql + avt ;
            sql = bio.equals("") ? sql + "bio = ' '," : sql + bio ;
            sql = cmd.equals("") ? sql + "comedia = ' '," : sql + cmd ;
            sql = drm.equals("") ? sql + "drama = ' '," : sql +drm ;
            sql = fts.equals("") ? sql + "fantasia = ' '," : sql + fts ;
            sql = fcc.equals("") ? sql + "ficcao = ' '," : sql + fcc ;
            sql = hrr.equals("") ? sql + "horror = ' '," : sql + hrr ;
            sql = rmc.equals("") ? sql + "romance = ' '," : sql + rmc ;
            sql = sps.equals("") ? sql + "suspense = ' '" : sql + sps ;
            
            sql = sql + "WHERE " + dado + " = " + id;
            
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();            
        }
        catch (Exception ex) {
            throw new Exception(ex);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void setComentario(Comentario c) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("INSERT INTO comentario (nfilme, nnot, coduser, comentario) VALUES (?,?,?,?)");
            stmt.setInt(1, c.getNfilme());
            stmt.setInt(2, c.getNnot());
            stmt.setInt(3, c.getCoduser());
            stmt.setString(4, c.getComentario());
            stmt.executeUpdate();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Comentario> getComenarios(String id, String dado ) throws Exception {
        try{
            List<Comentario> lc = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT c.*, u.nome, u.img FROM comentario c JOIN usuario u USING (coduser) WHERE " + dado + " = ?; ");
            stmt.setInt(1, Integer.parseInt(id));
            rs = stmt.executeQuery();
            while(rs.next()){
                Comentario c = new Comentario();
                Pessoa p = new Pessoa();
                c.setNcoment(rs.getInt("ncoment"));
                c.setNnot(rs.getInt("nnot"));
                c.setComentario(rs.getString("comentario"));
                p.setId(rs.getInt("coduser"));
                p.setNome(rs.getString("nome"));
                p.setImg(rs.getString("img"));
                c.setUser(p);
                lc.add(c);
            }
            return lc;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Noticia> getUltComentadas() throws Exception{
        try{
            List<Noticia> lc = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("select n.nnot, n.titulo, c.ncoment from noticia n join comentario c using (nnot) order by c.ncoment desc limit 4;");
            rs = stmt.executeQuery();
            while(rs.next()){
                Noticia n = new Noticia();
                n.setId(rs.getInt("nnot"));
                n.setTitulo(rs.getString("titulo"));
                lc.add(n);
            }
            return lc;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void setCompra(Compra c) throws Exception{
        try{
            con = FactoryConnection.getConnection();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt1 = new java.sql.Date(ft.parse(c.getData()).getTime());
            stmt = con.prepareStatement("INSERT INTO compra (coduser,data,vtotal) VALUES (?,?,?) ", Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, c.getCoduser());
            stmt.setDate(2, dt1);
            stmt.setDouble(3, c.getValor());
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                c.setNcompra(rs.getInt(1));
            }
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public void setCompraIng(Compra c) throws Exception{
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("INSERT INTO compra_ing(ncompra,nsessao,npromo,meia,valor_ing) "
                    + "VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            for(Ingresso i : c.getIngressos()){
                System.out.print(i.getValor());
                stmt.setInt(1, c.getNcompra());
                stmt.setInt(2, i.getNsessao());
                stmt.setInt(3, i.getNpromo());
                if(i.isMeia()){
                    stmt.setString(4, "X");
                }
                else{
                    stmt.setString(4, "");
                }
                stmt.setDouble(5, i.getValor());
                stmt.executeUpdate();
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    i.setNing(rs.getInt(1));
                }
            }
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Compra> getUltCompras() throws Exception{
        try{
            List<Compra> lc = new ArrayList();
            con = FactoryConnection.getConnection();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            stmt = con.prepareStatement("SELECT c.*, COUNT(i.ncompra) AS total FROM compra c JOIN compra_ing i USING(ncompra) "
                    + " GROUP BY ncompra ORDER BY data DESC LIMIT 8;");
            rs = stmt.executeQuery();
            while(rs.next()){
                Compra c = new Compra();
                List<Ingresso> li = new ArrayList();
                Ingresso i = new Ingresso();
                c.setNcompra(rs.getInt("ncompra"));
                c.setCoduser(rs.getInt("coduser"));
                c.setData(ft.format(rs.getDate("data")).toString());
                c.setValor(rs.getDouble("vtotal"));
                i.setNcompra(rs.getInt("total"));
                li.add(i);
                c.setIngressos(li);
                lc.add(c);
            }
            return lc;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Filme> getMaisAssistidos() throws Exception{
        try{
            List<Filme> lf = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT f.nome, COUNT(i.nsessao) AS total FROM filme f "
                    + "JOIN sessao s USING(nfilme) JOIN compra_ing i ON i.nsessao = s.nsessao "
                    + "Group by nome ORDER BY total DESC LIMIT 8;");
            rs = stmt.executeQuery();
            while(rs.next()){
                Filme f = new Filme();
                f.setTitulo(rs.getString("nome"));
                lf.add(f);
            }
            return lf;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

    public List<Compra> getCompraChart() throws Exception {
        try{
            List<Compra> lc = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT c.data, COUNT(i.ncompra) AS total FROM compra c JOIN compra_ing i USING(ncompra) "
                    + " GROUP BY data ORDER BY data DESC;");
            rs = stmt.executeQuery();
            while(rs.next()){
                Compra c = new Compra();
                Ingresso i = new Ingresso();
                c.setNcompra(rs.getInt("total"));
                c.setData(rs.getDate("data").toString());
                lc.add(c);
            }
            return lc;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Integer> getGenPerc() throws Exception{
        try{
            List<Integer> list = new ArrayList<Integer>();
            int tot = 0, act = 0, anm = 0, avt = 0, bio = 0, cmd = 0, drm = 0;
            int fts = 0, fcc = 0, hrr = 0, rmc = 0, sps = 0;
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("select s.nsessao, g.acao, g.animacao, g.aventura, g.bio, g.comedia, g.drama,\n" +
                    " g.fantasia, g.ficcao, g.horror, g.romance, g.suspense from sessao s join genero g using(nfilme) join compra_ing using(nsessao)");
            rs = stmt.executeQuery();
            while(rs.next()){
                if(rs.getString("acao")!=null && rs.getString("acao").equals("X")){
                    act++;
                }
                if(rs.getString("animacao")!=null && rs.getString("animacao").equals("X")){
                    anm++;
                }
                if(rs.getString("aventura")!=null && rs.getString("aventura").equals("X")){
                    avt++;
                }
                if(rs.getString("bio")!=null && rs.getString("bio").equals("X")){
                    bio++;
                }
                if(rs.getString("comedia")!=null && rs.getString("comedia").equals("X")){
                    cmd++;
                }
                if(rs.getString("drama")!=null && rs.getString("drama").equals("X")){
                    drm++;
                }
                if(rs.getString("fantasia")!=null && rs.getString("fantasia").equals("X")){
                    fts++;
                }
                if(rs.getString("ficcao")!=null && rs.getString("ficcao").equals("X")){
                    fcc++;
                }
                if(rs.getString("horror")!=null && rs.getString("horror").equals("X")){
                    hrr++;
                }
                if(rs.getString("romance")!=null && rs.getString("romance").equals("X")){
                    rmc++;
                }
                if(rs.getString("suspense")!=null && rs.getString("suspense").equals("X")){
                    sps++;
                }
                tot++;
            }
            list.add(tot); list.add(act); list.add(anm);
            list.add(avt); list.add(bio); list.add(cmd);
            list.add(drm); list.add(fts); list.add(fcc);
            list.add(hrr); list.add(rmc); list.add(sps);
            return list;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

}

