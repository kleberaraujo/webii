package Controller;

import java.util.List;

public class Compra {
    
    private int ncompra;
    private int coduser;
    private String data;
    private double valor;
    private List<Ingresso> ingressos;
    
    public Compra(){
        
    }

    public int getNcompra() {
        return ncompra;
    }

    public void setNcompra(int ncompra) {
        this.ncompra = ncompra;
    }

    public int getCoduser() {
        return coduser;
    }

    public void setCoduser(int coduser) {
        this.coduser = coduser;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<Ingresso> getIngressos() {
        return ingressos;
    }

    public void setIngressos(List<Ingresso> ingressos) {
        this.ingressos = ingressos;
    }
}
