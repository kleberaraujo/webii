$(document).ready( function() {
    $(".data").mask("(99/99/9999");
    $("#i_cpf").mask("999.999.999-99");
    
    $("#i_cep").mask("99.999-999");
    
    $("#i_valor").maskMoney({
        symbol: "R$",
        decimal: ",", 
        thousands: ".",
        showSymbol: true
    });
});

function valida_pedido(){
    var nome  = document.getElementById("i_nome").value;
    var data1 = document.getElementById("date1").value;
    var data2 = document.getElementById("date2").value;
    var ardt  = new Array;
    var ardt2 = new Array;
    var desc1 = document.getElementById("i_sdesc").value;
    var desc2 = document.getElementById("i_fdesc").value;
    
    ardt  = data1;
    ardt2 = data2;
    
    ardt  = ardt.split("/");
    ardt2 = ardt2.split("/");
    
    if (nome == ""){
        alert("Necessário informar o email do Cliente");
        document.getElementById("i_nome").focus();
        return false;
    }
    else if ( (ardt2[0] < ardt[0] && ardt2[1] <= ardt[1] && ardt2[2] <= ardt[2]) ||
              (ardt2[1] < ardt[1] && ardt2[2] <= ardt[2] ) ||
              (ardt2[2] < ardt[2])
            ) {
        alert("A data de Entrega fica no passado");
        document.getElementById("date2").focus();
        return false;
    }
    else if( (desc1 == "") && (desc2 == "") ){
        alert("Necessário informar o problema ou sua descrição completa");
        document.getElementById("i_sdesc").focus();
        return false;
    }
    else{
        alert("Pedido número 100 criado com sucesso!");
        return true;
    }
}

function finaliza_pedido(id){
    var l = 600;
    var a = 740;
    var conserto = id;
    var x = parseInt((screen.width-l)/2);
    var y = parseInt((screen.height-a)/2);
    var win = window.open('./Processar?action=Conserto '+conserto, '_blank', 'width=600,height=740,location=no');
    win.moveTo(x,y);
}
function cancelar(){
    if(window.confirm("Deseja cancelar?")){
        window.close();
    }
}

function definecampos(){    
    
    var opt = document.getElementById("s_status").value;
    
    if(opt == 1){
        document.getElementById("opt2").style.display="none";
        document.getElementById("opt2-2").style.display="none";
        document.getElementById("opt3").style.display="none";
        document.getElementById("opt3-3").style.display="none";
        document.getElementById("opt4").style.display="none";
    }
    else if(opt == 2){
        document.getElementById("opt2").style.display="";
        document.getElementById("opt2-2").style.display="";
        document.getElementById("opt3").style.display="none";
        document.getElementById("opt3-3").style.display="none";
        document.getElementById("opt4").style.display="none";
        addData("i_dinit");
    }
    else if(opt == 3){
        document.getElementById("opt2").style.display="none";
        document.getElementById("opt2-2").style.display="none";
        document.getElementById("opt3").style.display="";
        document.getElementById("opt3-3").style.display="";
        document.getElementById("opt4").style.display="none";
        addData("i_dfin");
    }
    else {
        document.getElementById("opt2").style.display="none";
        document.getElementById("opt2-2").style.display="none";
        document.getElementById("opt3").style.display="none";
        document.getElementById("opt3-3").style.display="none";
        document.getElementById("opt4").style.display="";
        addData("i_dent");
    }
}

function addData(campo){
    var data = new Date();
    var dia = (data.getDate())+"";
    if(dia.length==1)
        dia="0" +dia;
    var mes = (data.getMonth()+1)+"";
    if(mes.length==1)
        mes="0" +mes;
    var ano = data.getFullYear();
    data = dia+"/"+mes+"/"+ano;
    document.getElementById(campo).value = data;
}

function define_pesq(){
    
    var opt = document.getElementById("s_tpesq").value;
    if( opt == 1 ){
        document.getElementById("pesq_livre").style.display="none";
        document.getElementById("pesq_num").style.display="";
        document.getElementById("b_pesq").style.display="";
        document.getElementById("pesq_dinit").style.display="none";
        document.getElementById("pesq_dfin").style.display="none";
        document.getElementById("s_selc").style.display="none";
    }
    else if( opt == 2 ){
        document.getElementById("b_pesq").style.display="none";
        document.getElementById("pesq_livre").style.display="none";
        document.getElementById("pesq_num").style.display="none";
        document.getElementById("pesq_dinit").style.display="";
        document.getElementById("pesq_dfin").style.display="";
        document.getElementById("s_selc").style.display="";
        set_mask();
    }
    else if(opt == 4 ){
        document.getElementById("b_pesq").style.display="none";
    }
    else{
        document.getElementById("pesq_livre").style.display="";
        document.getElementById("pesq_num").style.display="none";
        document.getElementById("b_pesq").style.display="";
        document.getElementById("pesq_dinit").style.display="none";
        document.getElementById("pesq_dfin").style.display="none";
        document.getElementById("s_selc").style.display="none";
    }
}

function cancelarCadastroGerente(){
    if(window.confirm("Deseja cancelar  o cadastro?")){
        window.alert("Cadastro Cancelado");
        window.location.replace("./Processar?action=MainGerente");
    }
};

function cancelarCadastroTecnico(){
    if(window.confirm("Deseja cancelar  o cadastro?")){
        window.alert("Cadastro Cancelado");
        window.location.replace("./Processar?action=MainTecnico");
    }
};

function voltarMainTecnico(){
    window.location.replace("./Processar?action=MainTecnico");
};

function voltarMainGerente(){
    window.location.replace("./Processar?action=MainGerente");
};