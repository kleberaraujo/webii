﻿CREATE SEQUENCE idStatus;
CREATE SEQUENCE idCliente;
CREATE SEQUENCE idConserto;
CREATE SEQUENCE idCidade;
CREATE SEQUENCE idTipo;

create table Cidade(
	idCidade integer nextval('idCidade') not null,
	nome text,
	CONSTRAINT pkIdCidade PRIMARY KEY (idCidade)
);

create table Usuario(
	login varchar(50) unique not null,
	senha varchar(32),
	email varchar(50),
	tipoAcesso integer,
	CONSTRAINT pkLogin PRIMARY KEY (login)
);

create table Status(
	idStatus integer nextval('idStatus') not null,
	descricao text,
	CONSTRAINT pkIdStatus PRIMARY KEY (idStatus)
);

create table Cliente(
	idCliente integer nextval('idCliente') not null,
	nome varchar(20),
	cpf char(14),
	dataNasc Date,
	sexo char(1),
	cep char(10),
	cidade integer,
	endereco text,
	email varchar(50),
	CONSTRAINT pkIdCliente PRIMARY KEY (idCliente),
	CONSTRAINT fkIdCidade FOREIGN KEY (cidade) REFERENCES Cidade (idCidade)
);

create table TipoConserto(
	idTipo integer nextval('idTipo'),
	descricao varchar(20),
	CONSTRAINT pkIdTipo PRIMARY KEY (idTipo)
);

create table Conserto(
	idConserto integer nextval('idConserto') not null,
	idCliente integer not null,
	idTipo integer,
	fabricante varchar(20),
	modelo varchar(30),
	dataEntrada Date,
	dataEntregaPrevisao Date,
	dataEntregaReal Date,
	valor float,
	problema varchar(100),
	descricao text,
	status integer not null,
	dataInicial Date,
	dataFinal Date,
	observacao text,
	CONSTRAINT pkIdConserto PRIMARY KEY (idConserto),
	CONSTRAINT fkIdCliente FOREIGN KEY (idCliente) REFERENCES Cliente(idCliente),
	CONSTRAINT fkIdStatus FOREIGN KEY (status) REFERENCES Status(idStatus),
	CONSTRAINT fkIdTipo FOREIGN KEY (idTipo) REFERENCES TipoConserto(idTipo)
);

create table TecnicoConserto(
	idConserto integer not null,
	login varchar(50) not null,
	CONSTRAINT fkIdConserto FOREIGN KEY (idConserto) REFERENCES Conserto(idConserto),
	CONSTRAINT fkIdStatus FOREIGN KEY (login) REFERENCES Usuario(login)
);