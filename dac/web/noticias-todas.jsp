<%@ page language="java" contentType="text/html charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="noti" class="Model.NoticiaDAO" />
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>CineClub | Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="font-awesome/css/bootstrap-dialog.css">
        <link rel="stylesheet" href="font-awesome/css/bootstrap-dialog.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
    </head>

    <body id="admin">
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-8">
                        <h1>Notícias</h1>
                    </div>
                    <div class="col-lg-4">
                        <p class="pesquisa">
                            <form method="POST" action="noticias?action=pesq" >
                                <input name="pesq" class="form-control" placeholder="Buscar noticias...">
                                <!--<button type="submit" class="btn btn-primary">Pesquisar</button>-->
                            </form>
                        </p>
                    </div>
                </div>

                <c:forEach var="n" items="${noticias}" >
                    <div class="row">
                        <!-- <div class="col-lg-12"> -->
                        <div>    
                            <div class="bs-example">
                                <div class="jumbotron">
                                    <div class="row" id="ptext">
                                        <div class="col-lg-4 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body-img">
                                                    <c:if test="${not empty n.video }">
                                                        <iframe class="panel-body-img" src="http://www.youtube.com/embed/${n.video}"></iframe>
                                                    </c:if>
                                                    <c:if test="${empty n.video }">
                                                        <img class="panel-body-img" src="${n.imagem}"/>
                                                    </c:if>
                                                </div>
                                            </div>            
                                        </div>
                                        <h2 id="h2">
                                            ${n.titulo}
                                        </h2>
                                        <!--
                                        <div class="col-lg-8 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="panel-texto">
                                                    <p>
                                                        
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        -->
                                        ${n.texto}
                                        <a id="bt_remover" onclick="return f_delete();" href="noticias?action=remover&id=${n.id}">Remover</a>
                                        <a id="bt_alterar" href="noticias?action=setAlterar&id=${n.id}">Alterar</a>
                                    </div>     
                                </div>
                            </div>
                        </c:forEach>
                        
                        <!-- <div class="col-lg-12">
                            <ul class="pagination">
                                <li class="disabled"><a href="#">&laquo;</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div> -->

                    </div>
                </div><!-- /.row -->

            </div><!-- /#page-wrapper -->

        </div><!-- /#wrapper -->

        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>

        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/bootstrap-dialog.js"></script>
        <script src="js/bootstrap-dialog.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
