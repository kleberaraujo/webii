package Servlets;

import Controller.Compra;
import Controller.Filme;
import Controller.Ingresso;
import Controller.Mediator;
import Controller.Pessoa;
import Controller.Sessao;
import Model.GenericDAO;
import Model.PromoDAO;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ProcessaSessao", urlPatterns = {"/processaSessao"})
public class ProcessaSessao extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        Mediator mt = new Mediator();
        String action = request.getParameter("action");
        switch (action) {
            case "novaSessao":
                try {
                    List<String> salas = mt.getSalas();
                    List<Filme> filmes = mt.ListarFilmes();
                    request.setAttribute("salas", salas);
                    request.setAttribute("filmes", filmes);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/nova-sessao.jsp");
                    rd.forward(request, response);
                } catch (Exception erro) {
                    response.getWriter().print(erro);
                }
                break;
            case "inserir":
                try {
                    Sessao s = new Sessao();
                    s.setNfilme(Integer.parseInt(request.getParameter("filme")));
                    s.setNsala(Integer.parseInt(request.getParameter("sala")));
                    s.setHinit(request.getParameter("hinit"));
                    String data = request.getParameter("data");
                    s.setData(data.substring(8, 10) + "/" + data.substring(5, 7) + "/" + data.substring(0, 4));
                    s.setValor(Double.parseDouble(request.getParameter("valor").replace(",", ".")));
                    SimpleDateFormat ft = new SimpleDateFormat("HH:mm");
                    GregorianCalendar dt1 = new GregorianCalendar();
                    dt1.setTime(ft.parse(s.getHinit()));
                    dt1.add(Calendar.HOUR, 3);
                    s.setHfin(dt1.getTime().toString().substring(10, 16));
                    //verifica se a sala já vai estar ocupada no horário escolhido
                    boolean x = mt.dispSala(s);
                    if (x) {
                        //a sala está ocupada durante o período
                        request.setAttribute("tipoerro", "inserir uma nova Sessão");
                        request.setAttribute("descerro", "Esta sala estará indisponível durante esse horário, "
                                + "o período de indisponibilidade de uma sala é de três horas para cada sessão, /n"
                                + "para limpeza e preparação dos filmes a serem exibidos.");
                        RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin-erro.jsp");
                        rd.forward(request, response);
                    } else {
                        mt.setSessao(s);
                        synchronized (response) {
                            response.wait(2000);
                        }
                        response.sendRedirect("./admin.jsp");
                    }
                } catch (Exception erro) {
                    response.getWriter().print(erro);
                }
                break;
            case "carrinho":
                HttpSession session = request.getSession();
                List<Ingresso> ling = (List<Ingresso>) session.getAttribute("carrinho");
                if (ling == null) {
                    ling = new ArrayList();
                }
                String option = request.getParameter("option");
                if (option.equals("add")) {
                    Enumeration parametros = request.getParameterNames();
                    String id = "", ant = "";
                    int qtdnum = 0;
                    double valor = 0;
                    int qtdInt = 0, qtdMeia = 0, count = 0;
                    while (parametros.hasMoreElements()) {
                        String nomeParam = (String) parametros.nextElement();
                        String value = request.getParameter(nomeParam);
                        //define cada item pelo param fselec(X)
                        if (nomeParam.contains("fselec")) {
                            if (count == 0) {
                                //primeira vez
                                count++;
                            } else {
                                if (qtdInt > 0 || qtdMeia > 0) {
                                    //adiciona todos os ingressos inteiros
                                    if (qtdInt > 0) {
                                        for (int i = 0; i < qtdInt; i++) {
                                            Ingresso ing = new Ingresso();
                                            ing.setNsessao(Integer.parseInt(id));
                                            ing.setMeia(false);
                                            ling.add(ing);
                                            ing.setValor(valor);
                                        }
                                        qtdInt = 0;
                                    }
                                    if (qtdMeia > 0) {
                                        for (int i = 0; i < qtdMeia; i++) {
                                            Ingresso ing = new Ingresso();
                                            ing.setValor(valor / 2);
                                            ing.setNsessao(Integer.parseInt(id));
                                            ing.setMeia(false);
                                            ling.add(ing);
                                        }
                                    }
                                    qtdMeia = 0;
                                }
                            }
                            id = nomeParam.substring(6, nomeParam.length());
                            qtdnum = nomeParam.substring(6, nomeParam.length()).length();
                        }
                        if ((nomeParam.substring(nomeParam.length() - qtdnum, nomeParam.length()).equals(id))) {
                            //os itens que entrarem aqui fazem parte da seção escolhida
                            if (nomeParam.contains("rvalue")) {
                                valor = Double.parseDouble(value);
                            } else if (nomeParam.contains("fint")) {
                                if (value.equals("")) {
                                    qtdInt = 0;
                                } else {
                                    qtdInt = Integer.parseInt(value);
                                }
                            } else if (nomeParam.contains("fmeia")) {
                                if (value.equals("")) {
                                    qtdMeia = 0;
                                } else {
                                    qtdMeia = Integer.parseInt(value);
                                }
                            }
                        }
                    }
                    if (qtdInt > 0 || qtdMeia > 0) {
                        //caso foi setado pega o último
                        if (qtdInt > 0) {
                            for (int i = 0; i < qtdInt; i++) {
                                Ingresso ing = new Ingresso();
                                ing.setValor(valor);
                                ing.setNsessao(Integer.parseInt(id));
                                ing.setMeia(false);
                                ling.add(ing);
                            }
                        }
                        if (qtdMeia > 0) {
                            for (int i = 0; i < qtdMeia; i++) {
                                Ingresso ing = new Ingresso();
                                ing.setValor(valor / 2);
                                ing.setNsessao(Integer.parseInt(id));
                                ing.setMeia(true);
                                ling.add(ing);
                            }
                        }
                    }
                    session.setAttribute("carrinho", ling);
                    response.sendRedirect("./carrinho.jsp");
                }
                if (option.equals("del")) {
                    int x = Integer.parseInt(request.getParameter("id"));
                    ling.remove(x-1);
                    session.setAttribute("carrinho", ling);
                    response.sendRedirect("./carrinho.jsp");
                }
                break;
            case "finalizar":{
                double total = 0;
                PromoDAO pd = new PromoDAO();
                HttpSession ss = request.getSession();
                ling = (List<Ingresso>) ss.getAttribute("carrinho");
                Pessoa p = (Pessoa) ss.getAttribute("usuario");
                Compra c = new Compra();
                c.setCoduser(p.getId());
                for(Ingresso i : ling ){
                    double desc = pd.getDescBySessao(i.getNsessao());
                    i.setNpromo(pd.getPromoBySessao(i.getNsessao()));
                    i.setValor(i.getValor()-(i.getValor()*desc/100));
                    total += i.getValor();
                }
                c.setValor(total);
                c.setIngressos(ling);
                DateFormat df = DateFormat.getDateInstance();
                java.util.Date data1 = new java.util.Date();
                c.setData(df.format(data1));
                GenericDAO gd = new GenericDAO();
                gd.setCompra(c);
                gd.setCompraIng(c);
                ss.removeAttribute("carrinho");
                response.sendRedirect("./index.jsp");
                //response.getWriter().print(s);
                break;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaSessao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaSessao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
