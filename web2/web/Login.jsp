<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Nãotebuk - Concerto de Equipamentos - Login</title>
		<link rel="stylesheet" type="text/css" href="./css/login.css">
		<link rel="stylesheet" type="text/css" href="./css/style.css">
                <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
	</head>
	<body>
		<div id="login_box">
                    <h1>Nãotebuk - Login</h1>
			<form name="login" method="post" action="Processar?action=efetuarLogin">
				<div class="f_row">
					<label class="l_login">Usuário:</label>
					<input type="text" name="login" class="login_input" maxlength="80" required/>
				</div>
				<div class="f_row">
					<label class="l_login">Senha:</label>
					<input type="password" name="senha" class="login_input" maxlength="80" required/>
				</div>
				<div class="f_row">
					<input type="submit" id="b_logar" name="logar" value="Logar"/>
				</div>
			</form>
			<div id="login_bottom">
				<!-- <a href="#">Esqueci minha senha</a> -->
			</div>
		</div>
	</body>
</html>