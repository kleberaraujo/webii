<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${empty login}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${acesso == 0}">
    <c:redirect url="./"/>
</c:if>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nãotebuk - Cadastro de Clientes</title>
            <link rel="stylesheet" type="text/css" href="./css/style.css">
            <link rel="stylesheet" type="text/css" href="./css/jquery-ui-1.10.4.custom.min.css">
            <script type="text/javascript" src="./javascript/jquery-1.10.2.min.js"></script>
            <script type="text/javascript" src="./javascript/jquery-ui-1.10.4.custom.js"></script>
            <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
            <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
            <script type="text/javascript" src="./javascript/funcoes.js"></script>
            <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
	</head>
	<body>
		<h1>Cadastro de Clientes</h1>
		
		<div id="main_box">
			<form name="cadastroCliente" action="Processar?action=inserirCliente" method="post">
                            <div class="f_row">
                                <label class="l_cadastro">Nome:</label>
                                <input type="text" name="nome" class="i_text" maxlength="80" required  />
                            </div>
				
                            <div class="f_row">
                                <label class="l_cadastro">CPF:</label>
                                <input type="text" id="i_cpf" name="cpf" class="i_text" maxlength="14" required />
                            </div>
				
                            <div class="f_row">
                                <label class="l_cadastro">Data de Nascimento:</label>
                                <input type="date" name="dataNascimento" id="dataNascimento" class="i_text" required/>
                            </div>
				
                            <div class="f_row">
                                <label class="l_cadastro">Sexo:</label>
                                <select name="sexo" size="1" class="combo_user">
                                    <option value="M">Masculino
                                    <option value="F">Feminino		
                                </select>
                            </div>
				
                            <div class="f_row">
                                 <label class="l_cadastro">CEP:</label>
                                 <input type="text" name="cep" id="i_cep" class="i_text" required/>
                            </div>
				
                            <div class="f_row">
                                <label class="l_cadastro">Cidade:</label>
                                <select id="listaCidades" name="cidade" size="1" class="combo_user">
                                    <c:forEach items="${ListaCidades}" var="Cidade">
                                        <option value="${Cidade}"/>${Cidade}
                                    </c:forEach>			
                                </select>
                            </div>
                            
                            <div class="f_row">
                                <label class="l_cadastro">Endereço:</label>
                                <input type="text" name="endereco" class="i_text" maxlength="50" required/>
                            </div>
				
                            <div class="f_row">
                                 <label class="l_cadastro">Email:</label>
                                 <input type="email" name="email" class="i_text" maxlength="50" required/>
                            </div>
				
                            <div class="f_row">
                                <input type="button" id="b_salvar" onclick="cancelarCadastroTecnico();" name="b_cancelar" value="Cancelar"/>
                                <input type="submit" id="b_salvar" name="b_salvar" value="Salvar"/>
                            </div>
			</form>
		</div>
	</body>
</html>