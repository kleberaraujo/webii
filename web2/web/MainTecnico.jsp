<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${empty login}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${acesso == 0}">
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nãotebuk - Área Técnica </title>
            <link rel="stylesheet" type="text/css" href="./css/style.css">
            <link rel="stylesheet" type="text/css" href="./css/jquery-ui-1.10.4.custom.min.css">
            <script type="text/javascript" src="./javascript/jquery-1.10.2.min.js"></script>
            <script type="text/javascript" src="./javascript/jquery-ui-1.10.4.custom.js"></script>
            <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
            <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
            <script type="text/javascript" src="./javascript/animacoes.js"></script>
            <script type="text/javascript" src="./javascript/funcoes.js"></script>
            <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
	</head>
        <body onload="define_pesq();">
            <form name="f_pesq_pedido" action="./Processar?action=Pesquisar" method="post" >
                <!-- pesquisa livre -->
                <input type="text" id="pesq_livre" name="pesquisaLivre" class="i_pesq" maxlength="80" placeholder="Busca Livre..." />
                
                <!-- pesquisa por data -->
                <input class="pesq_data" type="date" name="pesquisaDataInicial" id="pesq_dinit" value="1" />
                <span id="s_selc"> até </span>                    
                <input class="pesq_data" type="date" name="pesquisaDataFinal" id="pesq_dfin" value="1" />
                
                <!-- pesquisa por número -->
                <input id="pesq_num" name="pesquisaNumero" class="i_pesq" maxlength="80" placeholder="Busca por Número..."/>
                <button type="submit" id="b_pesq" class="b_buscar">Buscar</button>
                
                <select name="pesquisaId" id="s_tpesq" size="1" onchange="define_pesq();">
                    <option value="1">Pesquisa por Número
                    <option value="2">Pesquisa por Data
                    <option value="3">Pesquisa Livre
                </select>
                <input type="hidden" name="Tipo" value="${Tipo}">
            </form>
            
            <h1><a href="./Processar?action=MainTecnico">Área Técnica - Consertos ${Total}</a></h1>

            <ul class="ul_menu">
                    <li id="li_cadastro"><a title="Cadastro de Cliente" href="./Processar?action=CadastrarCliente">Novo Cliente</a></li>
                    <li id="li_concerto"><a title="Cadastro de Pedido" href="./Processar?action=CadastrarConserto">Novo Pedido</a></li>
                    <li id="li_concerto"><a title="Meus Concertos" href="./Processar?action=MeusConcertos">Meus Concertos</a></li>
                    <li id="li_logout"><a href="./Processar?action=efetuarLogout" title="sair">Logout</a></li>
            </ul>

            <div class="table_container">
                <div class="header_3">
                    <table width="100%" border="0" class="t_header" >
                        <tr>
                            <td width="120px" title="Número do Pedido" >Nº Pedido</td>
                            <td width="300px" title="Nome do Cliente" >Cliente</td>
                            <td width="150px" title="Tipo de Equipamento" >Tipo</td>
                            <td width="150px" title="Modelo do Equipamento" >Modelo</td>
                            <td width="140px" title="Valor">Valor</td>
                            <td id="td_last"  title="Data de Entrega">Data de Entrega</td> 
                        </tr>
                    </table>
                </div>

                <div class="body">
                    <div class="t_body">
                        <c:forEach var="relatorio" items="${Relatorio}">
                            <c:choose>
                                <c:when test="${relatorio.conserto.dataEntregaPrevisao < dataAtual}">
                                    <div class="d_id_a" title="Número do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.idConserto}</div>
                                    <div class="d_cliente_a" title="Cliente" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.cliente.nome}</div>
                                    <div class="d_tipo_a" title="Tipo do Equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})" ><c:if test="${relatorio.conserto.tipo == 1}">Desktop</c:if><c:if test="${relatorio.conserto.tipo == 2}">Notebook</c:if></div>
                                    <div class="d_mod_a" title="Modelo do equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.modelo}</div>
                                    <div class="d_entrada_a" title="Data do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})"><fmt:formatNumber value="${relatorio.conserto.valor}" type="Currency" /></div>	
                                    <div class="d_saida_a" title="Entrega prevista" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">
                                        <c:if test="${empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaPrevisao}"/></div><br>
                                        </c:if>
                                        <c:if test="${not empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaReal}"/></div><br>
                                        </c:if>
                                </c:when>
                                <c:when test="${(relatorio.conserto.dataEntregaPrevisao <= dataProxima) && (relatorio.conserto.dataEntregaPrevisao >= dataAtual)}">
                                    <div class="d_id_f" title="Número do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.idConserto}</div>
                                    <div class="d_cliente_f" title="Cliente" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.cliente.nome}</div>
                                    <div class="d_tipo_f" title="Tipo do Equipamento" onClick="finaliza_pedido(${relatorioconserto.idConserto})" ><c:if test="${relatorio.conserto.tipo == 1}">Desktop</c:if><c:if test="${relatorio.conserto.tipo == 2}">Notebook</c:if></div>
                                    <div class="d_mod_f" title="Modelo do equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.modelo}</div>
                                    <div class="d_entrada_f" title="Data do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})"><fmt:formatNumber value="${relatorio.conserto.valor}" type="Currency" /></div>	
                                    <div class="d_saida_f" title="Entrega prevista" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">
                                        <c:if test="${empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaPrevisao}"/></div><br>
                                        </c:if>
                                        <c:if test="${not empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaReal}"/></div><br>
                                        </c:if>
                                </c:when>
                                <c:otherwise>
                                    <div class="d_id" title="Número do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.idConserto}</div>
                                    <div class="d_cliente" title="Cliente" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.cliente.nome}</div>
                                    <div class="d_tipo" title="Tipo do Equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})" ><c:if test="${relatorio.conserto.tipo == 1}">Desktop</c:if><c:if test="${relatorio.conserto.tipo == 2}">Notebook</c:if></div>
                                    <div class="d_mod" title="Modelo do equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.modelo}</div>
                                    <div class="d_entrada" title="Data do Pedido" onClick="finaliza_pedido(${relatorioconserto.idConserto})"><fmt:formatNumber value="${relatorio.conserto.valor}" type="Currency" /></div>	
                                    <div class="d_saida" title="Entrega prevista" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">
                                        <c:if test="${empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaPrevisao}"/></div><br>
                                        </c:if>
                                        <c:if test="${not empty relatorio.conserto.dataEntregaReal}" >
                                            <fmt:formatDate value="${relatorio.conserto.dataEntregaReal}"/></div><br>
                                        </c:if>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </div>
                </div>
            </div>
	</body>
</html>