<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:setProperty name="noti" property="*" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" charset="UTF-8" content="width=device-width, initial-scale=1.0">
        <title> CineClub | Admin </title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script>
            tinymce.init({selector: 'textarea'});
        </script>        
    </head>    
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <form onsubmit="f_message()" action="noticias?action=${param.action == null ? "inserir" : param.action }" enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="id" value="${noti.id}" />
                    <input type="hidden" name="antimg" value="${noti.imagem}" />
                    <input type="hidden" name="antdata" value="${noti.data}" />
                    <div class="row">
                        <div class="col-lg-12">
                            <h1>Cadastro de Notícia</h1>
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" placeholder="Título da notícia..." name="titulo" value="${noti.titulo}" >
                            </div>

                            <div class="form-group">
                                <label>Imagem</label>
                                <input type="file" name="imgagem" > 
                                <c:if test="${noti.id != 0}" >
                                    <span class="notas">
                                        *Nota: Caso não seja selecionado nenhuma imagem a notícia permanecerá com a imagem atual
                                    </span>
                                </c:if>
                            </div>

                            <div class="form-group">
                                <label>Vídeo</label>
                                <input name="video" class="form-control" placeholder="ID do vídeo no YouTube..." value="${noti.video}" />
                            </div>

                            <div class="form-group">
                                <label>Tags</label>
                                <p>
                                    <input type="hidden" id="gen" value="${noti.generos}">
                                    <input type="checkbox" name="p_act" id="p_act" > Ação &nbsp
                                    <input type="checkbox" name="p_anm" id="p_anm" > Animação &nbsp
                                    <input type="checkbox" name="p_avt" id="p_avt" > Aventura &nbsp
                                    <input type="checkbox" name="p_bio" id="p_bio" > Biografias &nbsp
                                    <input type="checkbox" name="p_cmd" id="p_cmd" > Comédia &nbsp
                                    <input type="checkbox" name="p_drm" id="p_drm" > Drama &nbsp
                                    <input type="checkbox" name="p_fts" id="p_fts" > Fantasia &nbsp
                                    <input type="checkbox" name="p_fcc" id="p_fcc" > Ficção &nbsp
                                    <input type="checkbox" name="p_hrr" id="p_hrr" > Horror &nbsp
                                    <input type="checkbox" name="p_rmc" id="p_rmc" > Romance &nbsp
                                    <input type="checkbox" name="p_sps" id="p_sps" > Suspense
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Corpo da notícia</label>
                                <textarea id="texto" name="texto" rows="10" >${noti.texto}</textarea>
                            </div>
                            <c:if test="${not empty noti.id && noti.id != 0 && noti.id != '' }">
                                <button type="submit" class="btn btn-default">Atualizar</button>
                            </c:if>
                            <c:if test="${empty noti.id || noti.id == 0 || noti.id == null}" >
                                <button type="submit" class="btn btn-default">Cadastrar</button>
                            </c:if>
                        </div>
                    </div><!-- /.row -->
                </form>
            </div><!-- /#page-wrapper -->
        
        </div><!-- /#wrapper -->
        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/tablesorter/tables.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/popup/jquery.blockUI.js"></script>
    </body>
</html>