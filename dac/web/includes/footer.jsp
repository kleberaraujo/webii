<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="cli" class="Model.ClienteDAO"/>
<jsp:useBean id="ult" class="Model.GenericDAO"/>
<!-- FOOTER -->
<footer>
    <div class="wrapper cf">
        <!-- widgets -->
        <ul  class="widget-cols cf">
            <li class="first-col">

                <div class="widget-block">
                    <h4>�LTIMAS COMENTADAS</h4>
                    <div class="recent-post cf">
                        <ul>
                            <c:forEach var="u" items="${ult.ultComentadas}" >
                                <!--<a href="./noticias.jsp" class="thumb"><img src="img/dummies/54x54.gif" alt="Post" /></a>-->
                                <li class="cat-item">
                                    <a href="noticias?action=single&noticia=${u.id}">${u.titulo}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </li>

            <li class="second-col">

                <div class="widget-block">
                    <h4>CINECLUB</h4>
                    <p>
                        - Assista aos melhores filmes com a qualidade que voc� conhece.
                    </p>

                    <p>
                        - Fa�a amigos que curtem os mesmos filmes que voc�!
                        <a href="./cadastro.jsp">Cadastre-se aqui</a>.
                    </p>
                </div>

            </li>

            <li class="third-col">

                <div class="widget-block">
                    <div id="tweets" class="footer-col tweet">
                        <h4>NOVOS CLUBERS</h4>
                    </div>
                    <br/>
                    <c:set var="nm" value="0"></c:set>
                    <c:forEach var="u" items="${cli.ult}">
                        <c:if test="${nm < 3}">
                            <div class="recent-post cf">
                                <a href="#" class="thumb"><img src="./${u.img}" /></a>
                                <div class="post-head">
                                    <a href="usuarios?action=exibir&id=${u.id}">${u.nome} ${u.sobrenome}</a>
                                </div>
                            </div>
                            <br/>
                        </c:if>
                        <c:set var="nm" value="${nm+1}"></c:set>
                    </c:forEach>
                </div>

            </li>

            <li class="fourth-col">

                <div class="widget-block">
                    <h4>SITES</h4>
                    <ul>
                        <li class="cat-item"><a href="./index.jsp">Principal</a></li>
                        <li class="cat-item"><a href="./usuarios?action=club&id=${usuario.cpf}">Club+</a></li>
                        <li class="cat-item"><a href="./noticias.jsp">Not�cias</a></li>
                        <li class="cat-item"><a href="./promocoes.jsp">Promo��es</a></li>
                        <li class="cat-item"><a href="./filmes?action=em-cartaz">Em Cartaz</a></li>
                    </ul>
                </div>

            </li>	
        </ul>
        <!-- ENDS widgets -->	
        <!-- bottom -->
        <div class="footer-bottom">
            <div class="left">Universidade federal do Paran� -
                <a>Desenvolvimento de Aplica��es Corporativas</a>
            </div>
            <ul id="social-bar" class="cf sb">
                <li><a href="./cadastro.jsp" title="Cadastre-se" class="plus"></a></li>
            </ul>
        </div>	
        <!-- ENDS bottom -->
    </div>
</footer>
<!-- ENDS FOOTER -->
