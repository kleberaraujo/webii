package Controller;

import Model.ClienteDAO;
import Model.FilmeDAO;
import Model.GenericDAO;
import Model.NoticiaDAO;
import Model.PromoDAO;
import java.util.ArrayList;
import java.util.List;

public class Mediator {
    
    ClienteDAO clienteDAO = new ClienteDAO();
    NoticiaDAO noticiaDAO = new NoticiaDAO();
    FilmeDAO   filmeDAO   = new FilmeDAO();
    PromoDAO   promoDAO   = new PromoDAO();
    
    public void InsereCliente(Pessoa c) throws Exception{
        try {
            clienteDAO.InsereCliente(c);
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
    }
    
    public void InsereNoticia(Noticia n) throws Exception{
        try{
            noticiaDAO.InsereNoticia(n);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public void AtualizaNoticia(Noticia n) throws Exception {
        try{
            noticiaDAO.AtualizaNoticia(n);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public void InsereFilme(Filme f) throws Exception {
        try{
            filmeDAO.InsereFilme(f);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public void AtualizaFilme(Filme f) throws Exception {
        try{
            filmeDAO.AtualizaFilme(f);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public List<String> getTotDados() throws Exception{
        try{
            List<String> l = new ArrayList();
            int i = noticiaDAO.getTotNoticias();
            l.add(0, String.valueOf(i));
            i = clienteDAO.getTotClientes();
            l.add(1, String.valueOf(i));
            i = filmeDAO.getTotFilmes();
            l.add(2, String.valueOf(i));
            i = promoDAO.getTotPromo();
            l.add(3, String.valueOf(i));
            return l;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
   public List<Filme> ListarFilmes() throws Exception {
        try{
            return filmeDAO.getLista();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        
    }
    
    public List<Pessoa> ListarPessoas() throws Exception {

        try{
            return clienteDAO.listarCliente();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public void inserePromo(Promocao p) throws Exception{
        try{
            promoDAO.inserePromo(p);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public int totPromoAtiva(int i, Promocao p) throws Exception{
        try{
            return promoDAO.totPromoAtiva(i, p);
        } catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public void removePromo(String id) throws Exception {
        try{
            promoDAO.removePromo(id);
        } catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public Promocao getPromo(String id) throws Exception {
        try{
            return promoDAO.getPromo(id);
        } catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public void updatePromo(Promocao p) throws Exception {
        try{
            promoDAO.updatePromo(p);
        } catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public Pessoa processaLogin(String email, String senha) throws Exception {
        return clienteDAO.getCliente(email, senha);
    }

    public void updateUser(Pessoa p) throws Exception {
        try{
            clienteDAO.updateCliente(p);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getSujAmigos(Pessoa x) throws Exception {
        try{
            return clienteDAO.getSujestao(x);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getAmigos(Pessoa p) throws Exception {
        try{
            return clienteDAO.getAmigos(p);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public Pessoa getPessoa(String id) throws Exception {
        try{
            return clienteDAO.getPessoa(id);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public void addFriend(int id1, int id2) throws Exception {
        try{
            clienteDAO.addFriend(id1,id2);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public boolean isAmigo(int id1, int id2) throws Exception {
        try{
            return clienteDAO.isFriend(id1,id2);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getUlt() throws Exception {
        try{
            return clienteDAO.getUlt();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public boolean verifEmail(String email) throws Exception {
        try{
            return clienteDAO.verifEmail(email);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public boolean verifCpf(String cpf) throws Exception {
        try{
            return clienteDAO.verifCpf(cpf);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getUsers() throws Exception {
        try{
            return clienteDAO.getUsers();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getClientes() throws Exception {
        try{
            return clienteDAO.getClientes();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public List<String> getSalas() throws Exception{
        try{
            return filmeDAO.getSalas();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public boolean dispSala(Sessao s) throws Exception {
        try{
            return filmeDAO.dispSala(s);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public void setSessao(Sessao s) throws Exception {
        try{
            filmeDAO.setSessao(s);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Filme> getFilmesemcartaz() throws Exception {
        try{
            return filmeDAO.getFSessoes();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<String> getDataSessoes(String id) throws Exception {
        try{
            return filmeDAO.getDataSessoes(id);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Sessao> getHSessoes(String id, String data) throws Exception {
        try{
            return filmeDAO.getHSessoes(id, data);
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<String> getCompras() throws Exception {
        try{
            List<String> l = new ArrayList();
            GenericDAO gd = new GenericDAO();
            int i = 0;
            for(Compra c :gd.getCompraChart()){
                l.add(i, c.getData());
                i++;
                l.add(i, String.valueOf(c.getNcompra()));
                i++;
            }
            return l;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public List<Integer> getGenPerc() throws Exception{
        try{
            GenericDAO gd = new GenericDAO();
            List<Integer> list = gd.getGenPerc();
            return list;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
}
