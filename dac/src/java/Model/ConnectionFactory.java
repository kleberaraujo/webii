
package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:postgresql:://localhost/DAC", "postgres", "postgres");
        } catch (SQLException e) {
            throw new RuntimeException("Não foi possivel estabelecer uma comunicação com o banco de dados");
        }
    }
}