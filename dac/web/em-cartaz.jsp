<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html class="no-js">
    <head>
        <meta charset="utf-8"/>
        <title> CineClub | Em cartaz </title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="blog">
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">
            <div class="wrapper cf">
                <!-- masthead -->
                <div class="masthead cf"> EM CARTAZ </div>
                <!-- ENDS masthead -->
                <!-- posts list -->
                <div id="posts-list" class="cf">    					
                    
                    <c:forEach var="f" items="${filmes}" >
                        
                        <!-- Standard -->
                        <article class="format-video">
                            <div class="feature-image">
                                <img src="./${f.imagem}" />
                            </div>
                            <div class="box cf">
                                <div class="excerpt">
                                    <a href="#" class="post-heading" >${f.titulo}</a>
                                    <p><a href="./filmes?action=sessoes&filme=${f.id}&title=${f.titulo}" class="learnmore">Quero Comprar</a></p>
                                </div>
                                
                                <div class="meta">
                                    <span class="format">${f.diretor}</span>
                                    <span class="tempo">${f.duracao} min.</span>
                                    <span class="classif">${f.idademin} anos.</span>
                                </div>
                                
                            </div>
                        </article>
                        <!-- ENDS  Standard -->
                    
                     </c:forEach>
                        
                </div>
                <!-- ENDS posts list -->
                <!-- sidebar -->
                <aside id="sidebar">
                    <!-- <ul>
                            <li class="block"> </li>
                    </ul> -->
                </aside>
                <!-- ENDS sidebar -->
            </div><!-- ENDS WRAPPER -->
        </div>
        <!-- ENDS MAIN -->
        <%@include file="./includes/footer.jsp" %>		
    </body>

</html>