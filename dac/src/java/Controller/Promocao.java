package Controller;

import java.util.List;

public class Promocao {
    
    private int npromo;
    private String descricao;
    private double desconto;
    private boolean ativo;
    private List<String> generos;

    public int getNpromo() {
        return npromo;
    }

    public void setNpromo(int npromo) {
        this.npromo = npromo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }    

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    
    public List<String> getGeneros() {
        return generos;
    }

    public void setGeneros(List<String> generos) {
        this.generos = generos;
    }
}
