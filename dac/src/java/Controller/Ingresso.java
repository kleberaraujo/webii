package Controller;

public class Ingresso {
    
    private int ning;
    private int ncompra;
    private int nsessao;
    private int npromo;
    private boolean meia;
    private double valor;
    
    public Ingresso(){
        
    }

    public int getNing() {
        return ning;
    }

    public void setNing(int ning) {
        this.ning = ning;
    }

    public int getNcompra() {
        return ncompra;
    }

    public void setNcompra(int ncompra) {
        this.ncompra = ncompra;
    }

    public int getNsessao() {
        return nsessao;
    }

    public void setNsessao(int nsessao) {
        this.nsessao = nsessao;
    }

    public int getNpromo() {
        return npromo;
    }

    public void setNpromo(int npromo) {
        this.npromo = npromo;
    }

    public boolean isMeia() {
        return meia;
    }

    public void setMeia(boolean meia) {
        this.meia = meia;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }    
}
