
package Beans;

import java.io.Serializable;
import java.sql.Date;

public class Cliente implements Serializable{
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
        private int id;
	private String nome;
	private String cpf;
	private Date dataNasc;
	private String sexo;
	private String cep;
	private int cidade;
        private String cidadeString;
	private String endereco;
	private String email;	
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public Cliente(){}

    public Cliente(String nome, String cpf, Date dataNasc, String sexo, String cep, int cidade, String endereco, String email) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNasc = dataNasc;
        this.sexo = sexo;
        this.cep = cep;
        this.cidade = cidade;
        this.endereco = endereco;
        this.email = email;
    }
    
    public Cliente(String nome, String cpf, Date dataNasc, String sexo, String cep, String cidade, String endereco, String email) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNasc = dataNasc;
        this.sexo = sexo;
        this.cep = cep;
        this.cidadeString = cidade;
        this.endereco = endereco;
        this.email = email;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getters e Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public int getCidade() {
        return cidade;
    }

    public void setCidade(int cidade) {
        this.cidade = cidade;
    }
    
    public String getCidadeString() {
        return cidadeString;
    }

    public void setCidade(String cidade) {
        this.cidadeString = cidade;
    }
    
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    // </editor-fold>

}
