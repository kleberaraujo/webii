<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${acesso != 0}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${empty login}">     
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nãotebuk - Relatórios</title>
            <link rel="stylesheet" type="text/css" href="./css/style.css">
            <link rel="stylesheet" type="text/css" href="./css/jquery-ui-1.10.4.custom.min.css">
            <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
            <script type="text/javascript" src="./javascript/jquery-1.10.2.min.js"></script>
            <script type="text/javascript" src="./javascript/jquery-ui-1.10.4.custom.js"></script>
            <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
            <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
            <script type="text/javascript" src="./javascript/animacoes.js"></script>
            <script type="text/javascript" src="./javascript/funcoes.js"></script>
	</head>
	<body class="b_reports" onload="define_pesq();">
            <c:catch var="exception">
                <c:if test="${pdata}" >
                    <c:set var="type" value="datas"></c:set>
                    <input type="hidden" id="d1" value="${d1}" />
                    <input type="hidden" id="d2" value="${d2}" />
                </c:if>
            </c:catch>
            <c:if test="${Tipo ne 5 and Tipo ne 1 and Tipo ne 2}" >
                <form name="f_pesq_pedido" action="./Processar?action=Pesquisar" method="post" >
                    <!-- pesquisa livre 
                    <input type="text" id="pesq_livre" name="pesquisaLivre" class="i_pesq" maxlength="80" placeholder="Busca Livre..." />

                    <!-- pesquisa por data -->
                    <input class="pesq_data" type="date" required="required" name="pesquisaDataInicial" id="pesq_dfin" value="1" />
                    <span id="s_selc"> até </span>
                    <input class="pesq_data" type="date" required="required" name="pesquisaDataFinal" id="pesq_dinit" value="1" />

                    <!-- pesquisa por número 
                    <input type="text" id="pesq_num" name="pesquisaNumero" class="i_pesq" maxlength="80" placeholder="Busca por Número..."/>-->
                    <button type="submit" id="b_pesq" class="b_buscar">Buscar</button> 

                    <select name="pesquisaId" id="s_tpesq" size="1" onchange="define_pesq();">
                        <!--<option value="1">Pesquisa por Número-->
                        <option value="4">Pesquisa por Data
                        <!--<option value="3">Pesquisa Livre-->
                    </select>
                </form>
            </c:if>
            <c:if test="${Tipo eq 5}" >
                <c:set var="type" value="atrasados"></c:set>
            </c:if>
            <c:if test="${Tipo eq 3}" >
                <c:set var="type" value="retirar"></c:set>
            </c:if>
            <h1>Total de Consertos ${Total}
                <c:if test="${Tipo eq 5 or Tipo eq 3 or Tipo eq 4 }" >
                    <c:if test="${not empty d1}">
                        <c:set var="datas" value="&d1=${d1}&d2=${d2}"></c:set>
                    </c:if>
                    | Exportar: <a href="./Processar?action=GerarRelatorio&type=${type}${datas}"><img src="./images/file_pdf.png" class="export"/></a>
                </c:if>
            </h1>
		<ul class="ul_menu">
                    <li id="li_logout"><a title="Voltar para o Menu Gerencial" href="./Processar?action=MainGerente" >Voltar</a></li>
		</ul>
		<div class="table_container">
                    <div class="header_3">
                        <table width="100%" border="0" class="t_header" >
                            <tr>
                                <td width="120px" title="Número do Pedido" >Nº Pedido</td>
                                <td width="300px" title="Nome do Cliente" >Cliente</td>
                                <c:if test="${Tipo != 5}">
                                    <td width="150px" title="Tipo de Equipamento" >Tipo</td>
                                </c:if>
                                <c:if test="${(Tipo == 1) || (Tipo == 5)}">
                                    <td width="150px" title="Modelo do Equipamento" >Modelo</td>
                                </c:if>
                                <c:if test="${Tipo == 5}">
                                    <td width="150px" title="Valor" >Valor</td>
                                </c:if>
                                <c:if test="${(Tipo == 2) || (Tipo == 3) || (Tipo == 4)}">
                                    <td width="150px" title="Técnico" >Técnico</td>
                                </c:if>
                                <c:if test="${(Tipo == 1) || (Tipo == 2) || (Tipo == 3) || (Tipo == 4)}">
                                    <td width="140px" title="Data do Pedido">Entrada</td>
                                </c:if>
                                <c:if test="${Tipo == 5}">
                                    <td width="140px" title="Dias em Atraso">Dias em atraso</td>
                                </c:if>
                                <c:if test="${(Tipo == 1 ) || (Tipo == 2) || (Tipo == 5)}">
                                    <td id="td_last"  title="Data de Entrega">Entrega prevista</td> 
                                </c:if>
                                <c:if test="${(Tipo == 3) || (Tipo == 4)}">
                                    <td id="td_last"  title="Data de Entrega">Entrega</td> 
                                </c:if>
                            </tr>
                        </table>
                    </div>

                    <div class="body">
                        <div class="t_body">
                            <c:forEach var="relatorio" items="${Relatorio}">
                                <div class="d_id" title="Número do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.idConserto}</div>
                                <div class="d_cliente" title="Cliente" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.cliente.nome}</div>
                                <c:if test="${Tipo != 5}">
                                    <div class="d_tipo" title="Tipo do Equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})" ><c:if test="${relatorio.conserto.tipo == 1}">Desktop</c:if><c:if test="${relatorio.conserto.tipo == 2}">Notebook</c:if></div>
                                </c:if>
                                <c:if test="${(Tipo == 1) || (Tipo == 5)}">
                                    <div class="d_mod" title="Modelo do equipamento" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.modelo}</div>
                                </c:if>
                                <c:if test="${Tipo == 5}">
                                    <div class="d_tipo" title="Valor Conserto" onClick="finaliza_pedido(${relatorio.conserto.idConserto})" ><fmt:formatNumber value="${relatorio.conserto.valor}" type="currency"/></div>
                                </c:if>
                                <c:if test="${(Tipo == 2) || (Tipo == 3) || (Tipo == 4)}">
                                    <div class="d_mod" title="Técnico" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.modelo}</div>
                                </c:if>
                                <c:if test="${(Tipo == 1) || (Tipo == 2) || (Tipo == 3) || (Tipo == 4)}">
                                    <div class="d_entrada" title="Data do Pedido" onClick="finaliza_pedido(${relatorio.conserto.idConserto})"><fmt:formatDate value="${relatorio.conserto.dataEntrada}"/></div>	
                                </c:if>
                                <c:if test="${Tipo == 5}">
                                    <div class="d_entrada" title="Dias em atraso" onClick="finaliza_pedido(${relatorio.conserto.idConserto})">${relatorio.conserto.diasAtrasados}</div>	
                                </c:if>
                                <c:if test="${(Tipo == 1 ) || (Tipo == 2) || (Tipo == 5)}">
                                     <div class="d_saida" title="Entrega prevista" onClick="finaliza_pedido(${relatorio.conserto.idConserto})"><fmt:formatDate value="${relatorio.conserto.dataEntregaPrevisao}"/></div><br>
                                </c:if>
                                <c:if test="${(Tipo == 3) || (Tipo == 4)}">
                                    <div class="d_saida" title="Entrega" onClick="finaliza_pedido(${relatorio.conserto.idConserto})"><fmt:formatDate value="${relatorio.conserto.dataEntregaPrevisao}"/></div><br>
                                </c:if>
                            </c:forEach>
                        </div>
                    </div>
		</div>	
	</body>
</html>