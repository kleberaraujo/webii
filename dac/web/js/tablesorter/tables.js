$(function() {
    $("table").tablesorter({debug: true});
});

function f_message() {
    $(document).ready(function() {
        $.blockUI({
            message: '<h2>Salvando...</h2>',
            timeout: 2000
        });
    });
}

function f_voltar(){
    window.history.back(1);
}

var a = document.getElementById("gen");
var s1 = a.value.replace("[", "");
s1 = s1.replace("]", "");
var s = s1.split(",");
for (var a in s) {
    if (s[a].match("Acao")||s[a] == 1) {
        $("#p_act").attr("checked", true);
    }
    else if (s[a].match("Animacao")||s[a] == 2) {
        $("#p_anm").attr("checked", true);
    }
    else if (s[a].match("Aventura")||s[a] == 3) {
        $("#p_avt").attr("checked", true);
    }
    else if (s[a].match("Biografia")||s[a] == 4) {
        $("#p_bio").attr("checked", true);
    }
    else if (s[a].match("Comedia")||s[a] == 5) {
        $("#p_cmd").attr("checked", true);
    }
    else if (s[a].match("Drama")||s[a] == 6) {
        $("#p_drm").attr("checked", true);
    }
    else if (s[a].match("Fantasia")||s[a] == 7) {
        $("#p_fts").attr("checked", true);
    }
    else if (s[a].match("Ficcao")||s[a] == 8) {
        $("#p_fcc").attr("checked", true);
    }
    else if (s[a].match("Horror")||s[a] == 9) {
        $("#p_hrr").attr("checked", true);
    }
    else if (s[a].match("Romance")||s[a] == 10) {
        $("#p_rmc").attr("checked", true);
    }
    else if (s[a].match("Suspense")||s[a] == 11) {
        $("#p_sps").attr("checked", true);
    }
}

var i = document.getElementById("isAtivo").value;
if(i.match("true")){
    $("#p_ativo").attr("checked", true);
}

function f_delete(){
    return confirm("Tem certeza que deseja Excluir?");
}