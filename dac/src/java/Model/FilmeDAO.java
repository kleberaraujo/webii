package Model;

import Controller.Filme;
import Controller.Sessao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FilmeDAO {

    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;

    public void InsereFilme(Filme f) throws Exception {

        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            if (con != null) {
                stmt = con.prepareStatement("INSERT INTO filme (nome,diretor,duracao,idademin,imagem,video,sinopse,curiosidades)"
                        + " VALUES (?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, f.getTitulo());
                stmt.setString(2, f.getDiretor());
                stmt.setInt(3, f.getDuracao());
                stmt.setInt(4, f.getIdademin());
                stmt.setString(5, f.getImagem());
                stmt.setString(6, f.getVideo());
                stmt.setString(7, f.getSinopse());
                stmt.setString(8, f.getCuriosidades());
                stmt.executeUpdate();
                ResultSet rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    i = rs.getInt(1);
                }
                f.setId(i);
            } else {
                throw new Exception("Erro ao Conectar com o Banco de Dados");
            }
        } catch (Exception erro) {
            throw new Exception(erro);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public void AtualizaFilme(Filme f) throws Exception {

        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            List<String> generos = new ArrayList();
            if (con != null) {
                stmt = con.prepareStatement("UPDATE filme SET nome = ?, diretor = ?, duracao = ?,"
                        + " idademin = ?, imagem = ?, video = ?, sinopse = ?, "
                        + " curiosidades = ? WHERE nfilme = ?");
                stmt.setString(1, f.getTitulo());
                stmt.setString(2, f.getDiretor());
                stmt.setInt(3, f.getDuracao());
                stmt.setInt(4, f.getIdademin());
                stmt.setString(5, f.getImagem());
                stmt.setString(6, f.getVideo());
                stmt.setString(7, f.getSinopse());
                stmt.setString(8, f.getCuriosidades());
                stmt.setInt(9, f.getId());
                stmt.executeUpdate();
            } else {
                throw new Exception("Erro ao Conectar com o Banco de Dados");
            }
        } catch (Exception erro) {
            throw new Exception(erro);
        }
    }

    public int getTotFilmes() throws Exception {
        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM filme;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                i = rs.getInt("total");
            }
            return i;
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public List<Filme> getLista() throws Exception {
        try {
            List<Filme> lfilme = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT f.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + " FROM filme f INNER JOIN genero g ON g.nfilme = f.nfilme ORDER BY nfilme DESC;");
            rs = stmt.executeQuery();
            List<String> lgeneros = new ArrayList();
            while (rs.next()) {
                Filme f = new Filme();
                f.setId(Integer.parseInt(rs.getString("nfilme")));
                f.setTitulo(rs.getString("nome"));
                f.setImagem(rs.getString("imagem"));
                f.setVideo(rs.getString("video"));
                f.setIdademin(rs.getInt("idademin"));
                f.setDuracao(rs.getInt("duracao"));
                f.setSinopse(rs.getString("sinopse"));
                f.setCuriosidades(rs.getString("curiosidades"));
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                f.setGeneros(lgeneros);
                lfilme.add(f);
            }
            return lfilme;
        } catch (Exception erro) {
            throw new Exception(erro);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public Filme getFilme(String id) throws Exception {
        try {
            Filme f = new Filme();
            List<String> lgeneros = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT f.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + " FROM filme f INNER JOIN genero g ON g.nfilme = f.nfilme WHERE f.nfilme = (?);");
            stmt.setInt(1, Integer.parseInt(id));
            rs = stmt.executeQuery();
            while (rs.next()) {
                f.setId(Integer.parseInt(rs.getString("nfilme")));
                f.setTitulo(rs.getString("nome"));
                f.setImagem(rs.getString("imagem"));
                f.setVideo(rs.getString("video"));
                f.setDiretor(rs.getString("diretor"));
                f.setIdademin(rs.getInt("idademin"));
                f.setDuracao(rs.getInt("duracao"));
                f.setSinopse(rs.getString("sinopse"));
                f.setCuriosidades(rs.getString("curiosidades"));

                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("Acao");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("Animacao");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("Aventura");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("Biografia");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("Comedia");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("Drama");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("Fantasia");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("Ficcao");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("Horror");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("Romance");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("Suspense");
                }
                f.setGeneros(lgeneros);
            }
            return f;
        } catch (Exception erro) {
            throw new Exception(erro);
        } finally{
            stmt.close();
            con.close();
        }
    }

    public List<String> getSalas() throws Exception {
        try {
            List<String> lsalas = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM sala;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                String sala = rs.getInt("nsala") + " - " + rs.getInt("capacidade");
                lsalas.add(sala);
            }
            return lsalas;
        } catch (Exception erro) {
            throw new Exception(erro);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public boolean dispSala(Sessao s) throws Exception {
        try {
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            //List<Sessao> sl = new ArrayList();
            int total = 0;
            java.sql.Date data = new java.sql.Date(ft.parse(s.getData()).getTime());
            ft = new SimpleDateFormat("HH:mm");
            java.sql.Time hora1 = new java.sql.Time(ft.parse(s.getHinit()).getTime());
            java.sql.Time hora2 = new java.sql.Time(ft.parse(s.getHfin()).getTime());
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM sessao WHERE nsala = ? AND data = ? AND ( (? >= hinit AND ? <= hfin) OR (? >= hinit AND ? <= hfin) ) ;");
            stmt.setInt(1, s.getNsala());
            stmt.setDate(2, data);
            stmt.setTime(3, hora1);
            stmt.setTime(4, hora1);
            stmt.setTime(5, hora2);
            stmt.setTime(6, hora2);
            rs = stmt.executeQuery();
            while (rs.next()) {
                total = rs.getInt("total");
            }
            return total > 0;
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public void setSessao(Sessao s) throws Exception {
        try{
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date data = new java.sql.Date(ft.parse(s.getData()).getTime());
            ft = new SimpleDateFormat("HH:mm");
            java.sql.Time hora1 = new java.sql.Time(ft.parse(s.getHinit()).getTime());
            java.sql.Time hora2 = new java.sql.Time(ft.parse(s.getHfin()).getTime());
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("INSERT INTO sessao (nfilme, nsala, data, hinit, hfin, valor) VALUES (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, s.getNfilme());
            stmt.setInt(2, s.getNsala());
            stmt.setDate(3, data);
            stmt.setTime(4, hora1);
            stmt.setTime(5, hora2);
            stmt.setDouble(6, s.getValor());
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                s.setNsessao(rs.getInt(1));
            }
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public List<Filme> getFSessoes() throws Exception {
        try{
            List<Filme> lfilmes = new ArrayList();
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            int dia = Integer.parseInt(s.substring(0, 2));
            dia+=7;
            String s2 = dia + s.substring(2, s.length());
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt1 = new java.sql.Date(ft.parse(s).getTime());
            java.sql.Date dt2 = new java.sql.Date(ft.parse(s2).getTime());
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM filme WHERE nfilme IN (SELECT nfilme FROM sessao WHERE data BETWEEN ? AND ? ORDER BY nsessao DESC LIMIT 5 ) ORDER BY nfilme DESC ;");
            stmt.setDate(1, dt1);
            stmt.setDate(2, dt2);
            rs = stmt.executeQuery();
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("nfilme"));
                f.setTitulo(rs.getString("nome"));
                f.setDiretor(rs.getString("diretor"));
                f.setDuracao(rs.getInt("duracao"));
                f.setIdademin(rs.getInt("idademin"));
                f.setImagem(rs.getString("imagem"));
                f.setVideo(rs.getString("video"));
                f.setSinopse(rs.getString("sinopse"));
                f.setCuriosidades(rs.getString("curiosidades"));
                lfilmes.add(f);
            }
            return lfilmes;
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public List<String> getDataSessoes(String id) throws Exception {
        try{
            List<String> ldias = new ArrayList();
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            int dia = Integer.parseInt(s.substring(0, 2));
            dia+=7;
            String s2 = dia + s.substring(2, s.length());
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt1 = new java.sql.Date(ft.parse(s).getTime());
            java.sql.Date dt2 = new java.sql.Date(ft.parse(s2).getTime());
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT DISTINCT data FROM sessao WHERE nfilme = ? AND data BETWEEN ? AND ?;");
            stmt.setInt(1, Integer.parseInt(id));
            stmt.setDate(2, dt1);
            stmt.setDate(3, dt2);
            rs = stmt.executeQuery();
            while(rs.next()){
                ldias.add(ft.format(rs.getDate("data")).toString());
            }
            return ldias;
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public List<Sessao> getHSessoes(String id, String data) throws Exception {
        try{
            List<Sessao> lsessoes = new ArrayList();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt = new java.sql.Date(ft.parse(data).getTime());
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM sessao WHERE nfilme = ? AND data = ?;");
            stmt.setInt(1, Integer.parseInt(id));
            stmt.setDate(2, dt);
            rs = stmt.executeQuery();
            while(rs.next()){
                Sessao s = new Sessao();
                s.setNsessao(rs.getInt("nsessao"));
                s.setNfilme(rs.getInt("nfilme"));
                s.setNsala(rs.getInt("nsala"));
                s.setData(ft.format(rs.getDate("data")).toString());
                s.setHinit(rs.getTime("hinit").toString());
                s.setHfin(rs.getTime("hfin").toString());
                s.setValor(rs.getDouble("valor"));
                lsessoes.add(s);
            }
            return lsessoes;
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }
    
    public Filme getFilmeBySessao(int id) throws Exception{
        try{
            Filme f = new Filme();
            List <String> lgeneros = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT f.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + " FROM filme f INNER JOIN genero g USING (nfilme) WHERE nfilme = ( SELECT nfilme FROM sessao WHERE nsessao = ? );");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while(rs.next()){
                f.setId(rs.getInt("nfilme"));
                f.setTitulo(rs.getString("nome"));
                f.setDiretor(rs.getString("diretor"));
                f.setDuracao(rs.getInt("duracao"));
                f.setIdademin(rs.getInt("idademin"));
                f.setImagem(rs.getString("imagem"));
                f.setVideo(rs.getString("video"));
                f.setSinopse(rs.getString("sinopse"));
                f.setCuriosidades(rs.getString("curiosidades"));
                
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
            }
            f.setGeneros(lgeneros);
            return f;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }
    
    public Sessao getSessao(int id) throws Exception{
        try{
            Sessao s = new Sessao();
            con = FactoryConnection.getConnection();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            stmt = con.prepareStatement("SELECT * FROM sessao WHERE nsessao = ? ;");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while(rs.next()){
                s.setNsessao(rs.getInt("nsessao"));
                s.setNfilme(rs.getInt("nfilme"));
                s.setNsala(rs.getInt("nsala"));
                s.setData(ft.format(rs.getDate("data")).toString());
                s.setHinit(rs.getTime("hinit").toString().substring(0, 5));
                s.setHfin(rs.getTime("hfin").toString().substring(0, 5));
                s.setValor(rs.getDouble("valor"));
            }
            return s;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public List<Filme> pesqFilme(String pesq) throws Exception {
        try {
            List<Filme> lfilme = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT f.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + " FROM filme f INNER JOIN genero g ON g.nfilme = f.nfilme WHERE lower(f.nome) LIKE lower(?) ORDER BY nfilme DESC;");
            stmt.setString(1, '%'+pesq+'%');
            rs = stmt.executeQuery();
            List<String> lgeneros = new ArrayList();
            while (rs.next()) {
                Filme f = new Filme();
                f.setId(Integer.parseInt(rs.getString("nfilme")));
                f.setTitulo(rs.getString("nome"));
                f.setImagem(rs.getString("imagem"));
                f.setVideo(rs.getString("video"));
                f.setIdademin(rs.getInt("idademin"));
                f.setDuracao(rs.getInt("duracao"));
                f.setSinopse(rs.getString("sinopse"));
                f.setCuriosidades(rs.getString("curiosidades"));
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                f.setGeneros(lgeneros);
                lfilme.add(f);
            }
            return lfilme;
        } catch (Exception erro) {
            throw new Exception(erro);
        } finally {
            stmt.close();
            con.close();
        }
    }
}
