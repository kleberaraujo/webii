package Model;

import Controller.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO {

    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;

    public boolean InsereCliente(Pessoa pessoa) throws Exception {

        try {
            con = FactoryConnection.getConnection();
            if (con != null) {
                int i = 0;
                stmt = con.prepareStatement("INSERT INTO usuario (cpf,nome,sobrenome,rg,sexo,email,senha,img,admin)"
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, pessoa.getCpf());
                stmt.setString(2, pessoa.getNome());
                stmt.setString(3, pessoa.getSobrenome());
                stmt.setString(4, pessoa.getRg());
                stmt.setString(5, pessoa.getSexo());
                stmt.setString(6, pessoa.getEmail());
                stmt.setString(7, pessoa.getSenha());
                stmt.setString(8, pessoa.getImg());
                if (pessoa.isAdmin()) {
                    stmt.setString(9, "X");
                } else {
                    stmt.setString(9, "");
                }
                stmt.executeUpdate();
                ResultSet rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    i = rs.getInt(1);
                }
                pessoa.setId(i);
            } else {
                throw new Exception("Erro ao Conectar com o Banco de Dados");
            }
        } catch (Exception erro) {
            throw new Exception(erro);
        }
        return true;
    }

    public int getTotClientes() throws Exception {
        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM usuario WHERE admin != 'X';");
            rs = stmt.executeQuery();
            while (rs.next()) {
                i = rs.getInt("total");
            }
            return i;
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            con.close();
            stmt.close();
        }
    }

    public List<Pessoa> listarCliente() throws Exception {
        try {
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT c.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + "FROM usuario c INNER JOIN genero g ON g.cpf = c.cpf;");
            rs = stmt.executeQuery();
            List<String> lgeneros = new ArrayList();
            while (rs.next()) {
                Pessoa p = new Pessoa();
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                p.setGeneros(lgeneros);
                lpessoa.add(p);
            }
            if (lpessoa == null) {
                throw new Exception("vazio");
            }
            return lpessoa;
        } 
        catch (Exception erro) {
            throw new Exception(erro);
        }
    }

    public Pessoa getCliente(String email, String senha) throws Exception {
        try {
            Pessoa p = new Pessoa();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT c.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + "FROM usuario c FULL OUTER JOIN genero g USING (coduser) WHERE c.email = ? AND c.senha = ? ;");
            stmt.setString(1, email);
            stmt.setString(2, senha);
            rs = stmt.executeQuery();
            while (rs.next()) {
                List<String> lgeneros = new ArrayList();
                p.setId(Integer.parseInt(rs.getString("coduser")));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    p.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    p.setAdmin(true);
                }
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                p.setGeneros(lgeneros);
            }
            return p;
        } 
        catch (Exception erro) {
            throw new Exception(erro);
        }
    }

    public void updateCliente(Pessoa pessoa) throws Exception {
        try {
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("UPDATE usuario SET cpf = ?, nome = ?, sobrenome = ?, rg = ?, sexo = ?,"
                    + "email = ?, senha = ?, img = ?, admin = ? WHERE coduser = ?;");
            stmt.setString(1, pessoa.getCpf());
            stmt.setString(2, pessoa.getNome());
            stmt.setString(3, pessoa.getSobrenome());
            stmt.setString(4, pessoa.getRg());
            stmt.setString(5, pessoa.getSexo());
            stmt.setString(6, pessoa.getEmail());
            stmt.setString(7, pessoa.getSenha());
            stmt.setString(8, pessoa.getImg());
            if (pessoa.isAdmin()) {
                stmt.setString(9, "X");
            } else {
                stmt.setString(9, "");
            }
            stmt.setInt(10, pessoa.getId());
            stmt.executeUpdate();
        } catch (Exception erro) {
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getSujestao(Pessoa pessoa) throws Exception {
        try {
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT u.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + "FROM usuario u JOIN genero g USING (coduser) WHERE coduser != ? AND coduser not in ("
                    + " SELECT amigo from amigo WHERE coduser = ? ) ;");
            stmt.setInt(1, pessoa.getId());
            stmt.setInt(2, pessoa.getId());
            rs = stmt.executeQuery();
            while (rs.next()) {
                Pessoa l = new Pessoa();
                List<String> lgeneros = new ArrayList();
                l.setId(Integer.parseInt(rs.getString("coduser")));
                l.setCpf(rs.getString("cpf"));
                l.setEmail(rs.getString("email"));
                l.setImg(rs.getString("img"));
                l.setNome(rs.getString("nome"));
                l.setRg(rs.getString("rg"));
                l.setSobrenome(rs.getString("sobrenome"));
                l.setSexo(rs.getString("sexo"));
                l.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    l.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    l.setAdmin(true);
                }
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                l.setGeneros(lgeneros);
                lpessoa.add(l);
            }
            int n = lpessoa.size();
            int[][] comp = new int[2][n];
            int i = 0;
            List<Pessoa> lpessoa_aux = lpessoa;
            for (Pessoa a : lpessoa) {
                int qtdig = 0;
                for (String ag : a.getGenero()) {
                    for (String pg : pessoa.getGenero()) {
                        if (pg.equals(ag)) {
                            qtdig++;
                        }
                    }
                }
                if(qtdig > 0){
                    comp[0][i] = a.getId();
                    comp[1][i] = qtdig;
                }
                i++;
            }
            if (n > 1) {
                for (int x = 0; x < n; x++) {
                    if (x + 1 <= n - 1) {
                        if (comp[1][x] < comp[1][x + 1]) {
                            int l = comp[1][x];
                            int l2 = comp[0][x];
                            comp[1][x] = comp[1][x + 1];
                            comp[0][x] = comp[0][x + 1];
                            comp[1][x + 1] = l;
                            comp[0][x + 1] = l2;
                        }
                    }
                }
            }
            List<Pessoa> lfpessoa = new ArrayList();
            for (int x = 0; x < n; x++) {
                if(x<6){
                    for(Pessoa u : lpessoa_aux){
                        if(u.getId() == comp[0][x]){
                            lfpessoa.add(u);
                        }
                    }
                }
            }
            return lfpessoa;
        } 
        catch (Exception erro) {
            throw new Exception(erro);
        }
        
    }

    public List<Pessoa> getAmigos(Pessoa p) throws Exception {
        try{
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT u.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + "FROM usuario u JOIN genero g USING (coduser) WHERE coduser != ? AND coduser in ("
                    + " SELECT amigo from amigo WHERE coduser = ? ) ;");
            stmt.setInt(1, p.getId());
            stmt.setInt(2, p.getId());
            rs = stmt.executeQuery();
            while (rs.next()) {
                Pessoa l = new Pessoa();
                List<String> lgeneros = new ArrayList();
                l.setId(Integer.parseInt(rs.getString("coduser")));
                l.setCpf(rs.getString("cpf"));
                l.setEmail(rs.getString("email"));
                l.setImg(rs.getString("img"));
                l.setNome(rs.getString("nome"));
                l.setRg(rs.getString("rg"));
                l.setSobrenome(rs.getString("sobrenome"));
                l.setSexo(rs.getString("sexo"));
                l.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    l.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    l.setAdmin(true);
                }
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                l.setGeneros(lgeneros);
                lpessoa.add(l);
            }
            return lpessoa;
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
    }

    public Pessoa getPessoa(String id) throws Exception {
        try{
            Pessoa p = new Pessoa();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT c.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + "FROM usuario c JOIN genero g USING (coduser) WHERE coduser = ? ;");
            stmt.setInt(1, Integer.parseInt(id));
            rs = stmt.executeQuery();
            while (rs.next()) {
                List<String> lgeneros = new ArrayList();
                p.setId(Integer.parseInt(rs.getString("coduser")));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    p.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    p.setAdmin(true);
                }
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                p.setGeneros(lgeneros);
            }
            return p;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public boolean verifEmail(String email) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM usuario WHERE email = ? ;");
            stmt.setString(1, email);
            rs = stmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i = rs.getInt("total");
            }
            return i > 0;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public boolean verifCpf(String cpf) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM usuario WHERE cpf = ? ;");
            stmt.setString(1, cpf);
            rs = stmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i = rs.getInt("total");
            }
            return i > 0;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public void addFriend(int id1, int id2) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("INSERT INTO amigo VALUES( ? , ? )");
            stmt.setInt(1, id1);
            stmt.setInt(2, id2);
            stmt.executeUpdate();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public boolean isFriend(int id1, int id2) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM amigo WHERE coduser = ? AND amigo = ?");
            stmt.setInt(1, id1);
            stmt.setInt(2, id2);
            rs = stmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i = rs.getInt("total");
            }
            if(i > 0){
                return true;
            }
            else{
                return false;
            }
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public List<Pessoa> getUlt() throws Exception{
        try{
            List<Pessoa> lp = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT u.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + "FROM usuario u JOIN genero g USING (coduser) WHERE u.admin != 'X' ORDER BY coduser DESC LIMIT 3;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                Pessoa p = new Pessoa();
                List<String> lgeneros = new ArrayList();
                p.setId(Integer.parseInt(rs.getString("coduser")));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    p.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    p.setAdmin(true);
                }
                if ("X".equals(rs.getString("ac"))) {
                    lgeneros.add("1");
                }
                if ("X".equals(rs.getString("an"))) {
                    lgeneros.add("2");
                }
                if ("X".equals(rs.getString("av"))) {
                    lgeneros.add("3");
                }
                if ("X".equals(rs.getString("b"))) {
                    lgeneros.add("4");
                }
                if ("X".equals(rs.getString("co"))) {
                    lgeneros.add("5");
                }
                if ("X".equals(rs.getString("d"))) {
                    lgeneros.add("6");
                }
                if ("X".equals(rs.getString("fa"))) {
                    lgeneros.add("7");
                }
                if ("X".equals(rs.getString("fi"))) {
                    lgeneros.add("8");
                }
                if ("X".equals(rs.getString("h"))) {
                    lgeneros.add("9");
                }
                if ("X".equals(rs.getString("r"))) {
                    lgeneros.add("10");
                }
                if ("X".equals(rs.getString("s"))) {
                    lgeneros.add("11");
                }
                p.setGeneros(lgeneros);
                lp.add(p);
            }
            return lp;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }

    public List<Pessoa> getUsers() throws Exception {
        try{
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM usuario WHERE admin = 'X';");
            rs = stmt.executeQuery();
            while(rs.next()){
                Pessoa p = new Pessoa();
                p.setId(Integer.parseInt(rs.getString("coduser")));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    p.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    p.setAdmin(true);
                }
                lpessoa.add(p);
            }
            return lpessoa;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public List<Pessoa> getClientes() throws Exception {
        try{
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM usuario WHERE admin != 'X';");
            rs = stmt.executeQuery();
            while(rs.next()){
                Pessoa p = new Pessoa();
                p.setId(Integer.parseInt(rs.getString("coduser")));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                if (rs.getString("admin") == null || rs.getString("admin").equals("")) {
                    p.setAdmin(false);
                } else if (rs.getString("admin").equals("X")) {
                    p.setAdmin(true);
                }
                lpessoa.add(p);
            }
            return lpessoa;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }    

    public List<Pessoa> getPesq(String pesq) throws Exception {
        try {
            List<Pessoa> lpessoa = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT * FROM usuario c WHERE admin != 'X' AND ("
                            + "lower(nome) LIKE lower(?) OR lower(sobrenome) LIKE lower(?)) ;");
            stmt.setString(1, '%'+pesq+'%');
            stmt.setString(2, '%'+pesq+'%');
            rs = stmt.executeQuery();
            while (rs.next()) {
                Pessoa p = new Pessoa();
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setImg(rs.getString("img"));
                p.setNome(rs.getString("nome"));
                p.setRg(rs.getString("rg"));
                p.setSobrenome(rs.getString("sobrenome"));
                p.setSexo(rs.getString("sexo"));
                p.setSenha(rs.getString("senha"));
                lpessoa.add(p);
            }
            if (lpessoa == null) {
                throw new Exception("vazio");
            }
            return lpessoa;
        } 
        catch (Exception erro) {
            throw new Exception(erro);
        }
    }
}
