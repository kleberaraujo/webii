
package Infra;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Data {
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    SimpleDateFormat format;
    GregorianCalendar dataTemp;
    Date date;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutor">
    public Data(){
        date = new Date();
        format = new SimpleDateFormat("yyyy-MM-dd");
        dataTemp = new GregorianCalendar();
        dataTemp.setTime(date);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public String DataAtual(){
        String data = format.format(dataTemp.getTime());
        return data;
    }
    
    public String DataProxima(){
        for(int i = 0; i < 5; i++){
            if(dataTemp.DAY_OF_WEEK == 1){
                dataTemp.add(GregorianCalendar.DAY_OF_MONTH, 1);
            }
            if(dataTemp.DAY_OF_WEEK == 7){
                dataTemp.add(GregorianCalendar.DAY_OF_MONTH, 2);
            }
            dataTemp.add(GregorianCalendar.DAY_OF_MONTH, -1);
        }
        String data = format.format(dataTemp.getTime());
        return data;
    }
    
    public Date DataAtualDate(){
        Date data = new Date();
        return data;
    }
    
    public String DataProximaDate(){
        dataTemp.add(dataTemp.DAY_OF_MONTH, +2);
        String d = format.format(dataTemp.getTime());
        return d;
    }
    
    public java.sql.Date ConverterData(String d) throws ParseException{
        d = d.substring(8, 10) + "/" + d.substring(5, 7) + "/" + d.substring(0, 4);
        format = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date data = new java.sql.Date(format.parse(d).getTime());
        return data;
    }
    
    public java.sql.Date ConverterData2(String d) throws ParseException{
        format = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date data = new java.sql.Date(format.parse(d).getTime());
        return data;
    }
    // </editor-fold>
    
}
