package Beans;

public class Usuario {
	
	private String nome;
	private String email;
	private int    tp_user;
	private String senha;

	public Usuario(){
		
	}
	
	public Usuario(String nome, String email, int tp_user, String senha){
		this.nome  	 = nome;
		this.email 	 = email;
		this.tp_user = tp_user;
		this.senha   = senha;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTp_user() {
		return tp_user;
	}
	public void setTp_user(int tp_user) {
		this.tp_user = tp_user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
