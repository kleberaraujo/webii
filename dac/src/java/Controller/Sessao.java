
package Controller;

public class Sessao {
    
    private int nsessao;
    private int nfilme;
    private int nsala;
    private String data;
    private String hinit;
    private String hfin;
    private double valor;
    
    public Sessao(){
        
    }

    /**
     * @return the nsessao
     */
    public int getNsessao() {
        return nsessao;
    }

    /**
     * @param nsessao the nsessao to set
     */
    public void setNsessao(int nsessao) {
        this.nsessao = nsessao;
    }

    /**
     * @return the nfilme
     */
    public int getNfilme() {
        return nfilme;
    }

    /**
     * @param nfilme the nfilme to set
     */
    public void setNfilme(int nfilme) {
        this.nfilme = nfilme;
    }

    /**
     * @return the nsala
     */
    public int getNsala() {
        return nsala;
    }

    /**
     * @param nsala the nsala to set
     */
    public void setNsala(int nsala) {
        this.nsala = nsala;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the hora
     */
    public String getHinit() {
        return hinit;
    }

    /**
     * @param hora the hora to set
     */
    public void setHinit(String hora) {
        this.hinit = hora;
    }

    /**
     * @return the hora
     */
    public String getHfin() {
        return hfin;
    }

    /**
     * @param hora the hora to set
     */
    public void setHfin(String hora) {
        this.hfin = hora;
    }
    
    /**
     * @return the valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    
}
