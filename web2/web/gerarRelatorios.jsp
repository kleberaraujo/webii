<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<c:if test="${acesso != 0}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${empty login}">     
    <c:redirect url="./"/>
</c:if>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nãotebuk - Gerar Relatórios </title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>
    <body>
        <div id="dgrlt">
            <form action="./Processar?action=GerarRelatorio" method="POST">    
                <input type="radio" checked="checked" name="type" value="1"> Consertos em Atraso <br><br>
                <input type="radio" name="type" value="2"> Consertos por Período <br><br>
                <input type="radio" name="type" value="3"> Consertos a serem Retirados <br><br>
                <input type="submit" id="b_salvar" onclick="cancelar()" name="b_salvar" value="Gerar"/>
            </form>
        </div>
    </body>
</html>
