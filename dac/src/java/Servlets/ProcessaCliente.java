package Servlets;

import Controller.Mediator;
import Controller.Pessoa;
import Model.ClienteDAO;
import Model.GenericDAO;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "processaCliente", urlPatterns = {"/usuarios"})
public class ProcessaCliente extends HttpServlet {

    private String filePath;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        File file;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;

        String nome = "", sobrenome = "", cpf = "", email = "", antimg = "", img = "", senha = "", rg = "", sexo = "";

        List<String> generos = new ArrayList();

        filePath = this.getServletContext().getRealPath("") + "\\img\\pictures\\";

        String action = request.getParameter("action");
        Mediator mt = new Mediator();

        if (action == null || (action.equals(""))) {
            List<Pessoa> lpessoa = mt.ListarPessoas();
            request.setAttribute("cliente", lpessoa);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/usuarios.jsp");
            rd.forward(request, response);

        } 
        else if (action.equals("exibir")) {
            Pessoa p = new Pessoa();
            if (request.getSession().getValue("usuario") != null) {
                p = (Pessoa) request.getSession().getValue("usuario");
            }
            String id = request.getParameter("id");
            if (p.getCpf() != null) {
                if (id.equals(p.getCpf())) {
                    //página do usuário
                    request.setAttribute("see", p);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/people.jsp");
                    rd.forward(request, response);
                } else {
                    Pessoa people = mt.getPessoa(id);
                    boolean isamigo = mt.isAmigo(p.getId(), people.getId());
                    request.setAttribute("see", people);
                    request.setAttribute("isamigo", isamigo);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/people.jsp");
                    rd.forward(request, response);
                }
            } else {
                //nao está logado
                Pessoa people = mt.getPessoa(id);
                request.setAttribute("see", people);
                request.setAttribute("isamigo", false);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/people.jsp");
                rd.forward(request, response);
            }
        } 
        else if (action.equals("edit")) {
            if (request.getSession().getValue("usuario") != null) {
                Pessoa p = (Pessoa) request.getSession().getValue("usuario");
                request.setAttribute("p", p);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/cadastro.jsp");
                rd.forward(request, response);
            }
        } 
        else if (action.equals("inserir") || action.equals("atualizar")) {
            Pessoa p = new Pessoa();
            String contentType = request.getContentType();
            if ((contentType.indexOf("multipart/form-data") >= 0)) {

                DiskFileItemFactory factory = new DiskFileItemFactory();

                factory.setSizeThreshold(maxMemSize);

                factory.setRepository(new File("c:\\temp"));

                ServletFileUpload upload = new ServletFileUpload(factory);

                upload.setSizeMax(maxFileSize);
                try {
                    List fileItems = upload.parseRequest(request);

                    Iterator i = fileItems.iterator();

                    while (i.hasNext()) {
                        FileItem fi = (FileItem) i.next();

                        if (fi.isFormField()) {
                            switch (fi.getFieldName()) {
                                case "antid":
                                    if (!fi.getString().equals("")) {
                                        p.setId(Integer.parseInt(fi.getString()));
                                    }
                                    break;
                                case "antimg":
                                    antimg = fi.getString();
                                    break;
                                case "cpf":
                                    cpf = fi.getString();
                                    p.setCpf(cpf);
                                    break;
                                case "nome":
                                    nome = fi.getString();
                                    p.setNome(nome);
                                    break;
                                case "sobrenome":
                                    sobrenome = fi.getString();
                                    p.setSobrenome(sobrenome);
                                    break;
                                case "rg":
                                    rg = fi.getString();
                                    p.setRg(rg);
                                    break;
                                case "sexo":
                                    sexo = fi.getString();
                                    p.setSexo(sexo);
                                    break;
                                case "senha":
                                    senha = fi.getString();
                                    p.setSenha(senha);
                                    break;
                                case "email":
                                    email = fi.getString();
                                    p.setEmail(email);
                                    break;
                                case "p_act":
                                    generos.add("1");
                                    break;
                                case "p_anm":
                                    generos.add("2");
                                    break;
                                case "p_avt":
                                    generos.add("3");
                                    break;
                                case "p_bio":
                                    generos.add("4");
                                    break;
                                case "p_cmd":
                                    generos.add("5");
                                    break;
                                case "p_drm":
                                    generos.add("6");
                                    break;
                                case "p_fts":
                                    generos.add("7");
                                    break;
                                case "p_fcc":
                                    generos.add("8");
                                    break;
                                case "p_hrr":
                                    generos.add("9");
                                    break;
                                case "p_rmc":
                                    generos.add("10");
                                    break;
                                case "p_sps":
                                    generos.add("11");
                                    break;
                            }
                        } else {
                            if (action.equals("inserir")) {
                                DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                                Calendar cal = Calendar.getInstance();
                                String hora = dateFormat.format(cal.getTime());
                                MessageDigest m = MessageDigest.getInstance("MD5");
                                m.update(hora.getBytes(), 0, hora.length());
                                img = new BigInteger(1, m.digest()).toString(16) + ".jpg";
                                filePath = filePath + img;
                                img = "\\img\\pictures\\" + img;
                                file = new File(filePath);
                                fi.write(file);
                            } else if (fi.getString().equals("") || fi.getString() == null) {
                                img = antimg;
                            } else {
                                img = antimg;
                                filePath = filePath + img.substring(14);
                                file = new File(filePath);
                                fi.write(file);
                                img = filePath.replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web", "");
                            }
                            p.setImg(img);
                        }
                    }
                    p.setGeneros(generos);
                    GenericDAO gen = new GenericDAO();
                    if (action.equals("atualizar")) {
                        mt.updateUser(p);
                        String id = String.valueOf(p.getId());
                        gen.AtualizaGenero(id, "coduser", p.getGenero());
                        HttpSession session = request.getSession();
                        session.setAttribute("usuario", p);
                        response.sendRedirect("./usuarios?action=exibir&id=" + p.getCpf());
                    } else {
                        mt.InsereCliente(p);
                        String id = "" + p.getId();
                        gen.InsereGenero(id, "coduser", p.getGenero());
                        response.sendRedirect("./login.jsp");
                    }
                } catch (Exception ex) {
                    response.getWriter().println(ex);
                }
            }
        } 
        else if (action.equals("club")) {
            try {
                if (request.getSession().getValue("usuario") != null) {
                    Pessoa p = (Pessoa) request.getSession().getValue("usuario");
                    List<Pessoa> lsujamigos = mt.getSujAmigos(p);
                    List<Pessoa> lamigos = mt.getAmigos(p);
                    HttpSession session = request.getSession();
                    session.setAttribute("amigos", lamigos);
                    session.setAttribute("sujamigos", lsujamigos);
                    response.sendRedirect("./amigos.jsp");
                } else {
                    //não está logado
                    HttpSession session = request.getSession();
                    List<Pessoa> lamigos = mt.getUlt();
                    session.setAttribute("amigos", lamigos);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/amigos.jsp");
                    rd.forward(request, response);
                }
            } catch (Exception erro) {
                response.getWriter().println(erro);
            }
        } 
        else if (action.equals("addFriend")) {
            try {
                if (request.getSession().getValue("usuario") != null) {
                    int id = Integer.parseInt(request.getParameter("id"));
                    Pessoa p = (Pessoa) request.getSession().getValue("usuario");
                    mt.addFriend(p.getId(), id);
                    response.sendRedirect("./usuarios?action=club");
                }
            } catch (Exception erro) {
                response.getWriter().println(erro);
            }
        }
        else if (action.equals("inserirAdm")) {
            cpf = request.getParameter("cpf");
            nome = request.getParameter("nome");
            sobrenome = request.getParameter("sobrenome");
            email = request.getParameter("email");
            senha = request.getParameter("senha");
            boolean x = mt.verifEmail(email);
            cpf = cpf.replaceAll("[^0-9]", "");
            if (x) {
                request.setAttribute("tipoerro", "criar um novo usuário");
                request.setAttribute("descerro", "Já existe um usuário cadastrado para o email: " + email);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin-erro.jsp");
                rd.forward(request, response);
            } else {
                x = mt.verifCpf(cpf);
                if (x) {
                    request.setAttribute("tipoerro", "criar um novo usuário");
                    cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
                    request.setAttribute("descerro", "Já existe um usuário cadastrado para o cpf: " + cpf);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin-erro.jsp");
                    rd.forward(request, response);
                } else {
                    Pessoa p = new Pessoa(cpf, nome, sobrenome, email, senha, true);
                    mt.InsereCliente(p);
                    synchronized (response) {
                        response.wait(2000);
                    }
                    response.sendRedirect("./admin.jsp");
                }
            }
        } 
        else if (action.equals("usuarios")) {
            List<Pessoa> usuarios = mt.getUsers();
            for (Pessoa p : usuarios) {
                String x = p.getCpf();
                p.setCpf(x.substring(0, 3) + "." + x.substring(3, 6) + "." + x.substring(6, 9) + "-" + x.substring(9, 11));
            }
            request.setAttribute("usuarios", usuarios);
            request.setAttribute("tpusers", "Administradores");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/usuarios.jsp");
            rd.forward(request, response);
        } 
        else if (action.equals("clientes")) {
            List<Pessoa> usuarios = mt.getClientes();
            for (Pessoa p : usuarios) {
                String x = p.getCpf();
                p.setCpf(x.substring(0, 3) + "." + x.substring(3, 6) + "." + x.substring(6, 9) + "-" + x.substring(9, 11));
            }
            request.setAttribute("usuarios", usuarios);
            request.setAttribute("tpusers", "Clientes");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/usuarios.jsp");
            rd.forward(request, response);
        } 
        else if (action.equals("login")) {
            try {
                email = request.getParameter("i_login");
                senha = request.getParameter("i_senha");

                Pessoa p = mt.processaLogin(email, senha);
                if (p.getId() == 0) {
                    //ir para página de erro
                    request.setAttribute("lerro", "X");
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp?login=erro");
                    rd.forward(request, response);
                } else {
                    HttpSession session = request.getSession();
                    session.setAttribute("usuario", p);
                    if (p.isAdmin()) {
                        response.sendRedirect("./admin.jsp");
                    } else {
                        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
                        rd.forward(request, response);
                    }
                }

            } catch (Exception erro) {
                response.getWriter().println(erro);
            }
        } 
        else if (action.equals("logout")) {
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("./index.jsp");
        }
        else if (action.equals("pesq")){
            ClienteDAO cd = new ClienteDAO();
            String pesq = request.getParameter("pesq");
            List<Pessoa> usuarios = cd.getPesq(pesq);
            request.setAttribute("tpusers", "Clientes");
            request.setAttribute("usuarios", usuarios);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/usuarios.jsp");
            rd.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            response.getWriter().println(ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            response.getWriter().println(ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
