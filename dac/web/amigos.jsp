<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
    <head>
        <title>CineClub - Seu Club</title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="single">
        <!-- HEADER -->
    <header>
        <div class="wrapper cf">
            <%@include file="./includes/header.jsp" %>
        </div>
    </header>
    <!-- ENDS HEADER -->

    <!-- MAIN -->
    <div id="main">
        <div class="wrapper cf">

            <!-- posts list -->
            <div id="posts-list" class="cf">
                <!-- masthead -->
                <c:choose>
                    <c:when test="${not empty usuario}">
                        <div class="masthead cf">MEU CLUB+</div>
                    </c:when>
                    <c:otherwise>
                        <div class="masthead cf"><a href="./cadastro.jsp">Clique aqui</a> e Faça parte do Club</div>
                    </c:otherwise>
                </c:choose>
                <!-- ENDS masthead -->

                <c:forEach var="a" items="${amigos}">
                    
                    <div class="bg_img">
                        <a href="./people.jsp" title="Kleber Araujo" >
                            <img src="./${a.img}" />
                        </a>
                    </div>
                        
                    <div class="bg_amg">
                        <div class="pname"><a href="./usuarios?action=exibir&id=${a.id}">${a.nome} ${a.sobrenome}</a></div>
                        <div class="d_tags">
                            Preferências:
                            <p>
                                <span class="tags">
                                    <c:forEach var="g" items="${a.genero}">
                                        <c:choose>
                                            <c:when test="${g == 1 }"><a>Ação</a></c:when>
                                            <c:when test="${g == 2 }"><a>Animação</a></c:when>
                                            <c:when test="${g == 3 }"><a>Aventura</a></c:when>
                                            <c:when test="${g == 4 }"><a>Biografia</a></c:when>
                                            <c:when test="${g == 5 }"><a>Comédia</a></c:when>
                                            <c:when test="${g == 6 }"><a>Drama</a></c:when>
                                            <c:when test="${g == 7 }"><a>Fantasia</a></c:when>
                                            <c:when test="${g == 8 }"><a>Ficção</a></c:when>
                                            <c:when test="${g == 9 }"><a>Horror</a></c:when>
                                            <c:when test="${g == 10}"><a>Romance</a></c:when>
                                            <c:when test="${g == 12}"><a>Suspense</a></c:when>
                                        </c:choose>
                                    </c:forEach>
                                </span>
                            </p>
                        </div>
                    </div>
                </c:forEach>
                <!-- page-navigation -->
                <div class="page-navigation cf">
                    <!-- <div class="nav-next"><a href="#">&#8592; Notícias mais antigas </a></div>
                    <div class="nav-previous"><a href="#">Newer Entries &#8594;</a></div>-->
                </div>
                <!--ENDS page-navigation -->
            </div>
            <!-- ENDS posts list -->

            <!-- sidebar -->
            <aside id="sidebar">
                <ul>
                    <c:if test="${not empty usuario}">
                        <li class="block">
                            <h4>SUGESTÃO DE AMIGOS</h4>

                            <c:forEach var="s" items="${sujamigos}" >
                                <div class="amg_imgbox">
                                    <a href="./usuarios?action=exibir&id=${s.id}" title="${s.nome}" >
                                        <img src="./${s.img}" />
                                    </a>
                                </div>

                                <div class="amg_sjbox">
                                    <div class="pname_sjbox"><a href="./usuarios?action=exibir&id=${s.id}">${s.nome} ${s.sobrenome}</a></div>
                                </div>
                            </c:forEach>
                        </li>
                    </c:if>
                </ul>	
            </aside>
            <!-- ENDS sidebar -->	
        </div><!-- ENDS WRAPPER -->

    </div>
    <!-- ENDS MAIN -->

    <%@include file="./includes/footer.jsp" %>

</body>
</html>
