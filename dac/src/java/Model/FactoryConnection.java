package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FactoryConnection {
    
    public static Connection getConnection() throws Exception{
        try{
            Connection con;
            Class.forName("org.postgresql.Driver");
            String URL     = "jdbc:postgresql://localhost:5432/cineclub";
            String usuario = "cineadmin";
            String senha   = "cineclub";
            con = DriverManager.getConnection(URL, usuario, senha);
            return con;
        }
        catch(ClassNotFoundException | SQLException erro){
            throw new Exception(erro);
        }
    }
}
