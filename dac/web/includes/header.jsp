<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Controller.Pessoa"%>
<% String local = request.getRequestURI();%>

<div id="logo">
    <a href="index.jsp"><img  src="img/cineclub.png" id="logo" alt="Simpler"></a>
</div>

<!-- nav -->
<ul id="nav" class="sf-menu"> 
    <li <% if(local.equals("/dac/index.jsp")){ %> 
            class="current-menu-item"
         <% } %> >
        <a href="index.jsp">CineClub</a>
    </li>
    <li <% if(local.equals("/dac/em-cartaz.jsp") || local.equals("/dac/comprar_ingressos.jsp") ) { %> 
            class="current-menu-item"
         <% } %> >
        <a href="./filmes?action=em-cartaz">Ingressos</a>
    </li>
    <!-- 
    <li><a href="page.html">NOT�CIAS</a>
            <ul>
                    <li><a href="page-elements.html">Elements</a></li>
                    <li><a href="page-icons.html">Icons</a></li>
                    <li><a href="page-typography.html">Typography</a></li>
            </ul>
    </li>
    
    <li><a href="contact.html">CONTACT</a></li>
    -->
    <li <% if(local.equals("/dac/noticias.jsp")){ %> 
            class="current-menu-item"
         <% } %> >
        <a href="noticias.jsp">Not�cias</a>      
    </li>
    
    <li <% if(local.equals("/dac/amigos.jsp")){ %> 
            class="current-menu-item"
         <% } %> >
        <a href="./usuarios?action=club&id=${usuario.cpf}">Club+</a>
    </li>

    <li <% if(local.equals("/dac/promocoes.jsp")){ %> 
            class="current-menu-item"
         <% } %> >
        <a href="./promocoes.jsp">Promo��es</a>
    </li>
    
    <li <% if(local.equals("/dac/carrinho.jsp")){ %> 
            class="current-menu-item"
         <% } %> >
        <a href="./carrinho.jsp">Carrinho</a>
    </li>
    
    <c:choose>
        
        <c:when test="${usuario.cpf == null || usuario.cpf == '' }">
            
            <li <% if(local.equals("/dac/login.jsp")){%> 
                    class="current-menu-item"
                 <% } %> >
                <a href="./login.jsp">Login</a>
            </li>
            
        </c:when>
            
        <c:otherwise>
            
            <li <% if(local.equals("/dac/people.jsp") || local.equals("/dac/cadastro.jsp") ){%> 
                    class="current-menu-item"
                 <% } %> >
                <a href="usuarios?action=exibir&id=${usuario.cpf}">${usuario.nome}</a>
            </li>
            
            <li>
                <a href="usuarios?action=logout">logout</a>
            </li>
        </c:otherwise>
        
    </c:choose>
        
</ul>

<div id="combo-holder"></div>
<!-- ends nav -->

