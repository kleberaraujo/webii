<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<jsp:useBean id="gd" class="Model.GenericDAO"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">        
        <title>CineClub | Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="js/morris/morris.css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>

            <div id="page-wrapper">

                <div class="row">
                    <div class="col-lg-12">
                        <h1>CineClub Management</h1>
                    </div>
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-users fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading">${totdados[1]}</p>
                                        <p class="announcement-text">Clientes</p>
                                    </div>
                                </div>
                            </div>
                            <a href="./usuarios?action=clientes">
                                <div class="panel-footer announcement-bottom">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            Listar
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-bar-chart-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading">${totdados[0]}</p>
                                        <p class="announcement-text">Notícias</p>
                                    </div>
                                </div>
                            </div>
                            <a href="./noticias?action=todas">
                                <div class="panel-footer announcement-bottom">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            Listar
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-film fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading">${totdados[2]}</p>
                                        <p class="announcement-text">Filmes</p>
                                    </div>
                                </div>
                            </div>
                            <a href="./filmes?action=todos">
                                <div class="panel-footer announcement-bottom">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            Listar
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-credit-card fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading">${totdados[3]}</p>
                                        <p class="announcement-text">Promoções</p>
                                    </div>
                                </div>
                            </div>
                            <a href="promocao-todas.jsp">
                                <div class="panel-footer announcement-bottom">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            Listar
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div><!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Estatística de Vendas </h3>
                                <input type="hidden" value="${totcompras}" id="chart" />
                            </div>
                            <div class="panel-body">
                                <div id="morris-chart-area"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Estatística por Gênero </h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-chart-donut"></div>
                                <div class="text-right">
                                    <input type="hidden" value="${percgen}" id="percgen" />
                                    <!-- <a href="./relatorios?action=vrgen"> Gerar Relatório <i class="fa fa-arrow-circle-right"></i></a> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money"></i> Block Busters CineClub+ </h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <c:forEach var="f" items="${gd.maisAssistidos}">
                                        <a class="list-group-item">
                                            <!-- <span class="badge">4 minutes ago</span> -->
                                            <i class="fa fa-video-camera"></i> ${f.titulo}
                                        </a>
                                    </c:forEach>
                                </div>
                                <div class="text-right">
                                    <a href="#"> Gerar Relatório <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money"></i> Últimas Compras </h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped tablesorter">
                                        <thead>
                                            <tr>
                                                <th>Qtde <i class="fa fa-sort"></i> </th>
                                                <th>Data <i class="fa fa-sort"></i> </th>
                                                <th>Valor <i></i> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="s" items="${gd.ultCompras}" >
                                                <tr>
                                                    <c:forEach var="g" items="${s.ingressos}">
                                                        <td>
                                                            ${g.ncompra}
                                                        </td>
                                                    </c:forEach>
                                                    <td>${s.data}</td>
                                                    <td><fmt:formatNumber value="${s.valor}" type="currency"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="./relatorios?action=vgeral"> Gerar Relatório <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                                    
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-film"></i>  Overview dos filmes em cartaz em Curitiba</h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-chart-area">
                                    <c:forEach var="of" items="${othersfilm}">
                                        <a class="list-group-item">
                                            <!-- <span class="badge">4 minutes ago</span> -->
                                            <i class="fa fa-video-camera"></i> ${of.titulo}
                                        </a>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div><!-- /.row -->
                                    
                </div><!-- /.row -->

            </div><!-- /#page-wrapper -->

        </div><!-- /#wrapper -->

        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>

        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="js/morris/morris-0.4.3.js"></script>
        <script src="js/morris/morris-min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
