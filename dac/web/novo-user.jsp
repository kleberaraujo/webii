<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CineClub | Admin</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.mask.js"></script>
        <script src="js/jquery.mask.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script>
            $(document).ready(function(){
               $('#cpf').mask('999.999.999-99');
            });
            function f_message(){
                $.blockUI({
                    message: '<h2>Salvando...</h2>',
                    timeout: 2000
                });
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Cadastro de Usuários</h1>
                        <br />
                        <form onsubmit="f_message()" action="usuarios?action=${param.action == null ? "inserirAdm" : param.action }" method="POST">
                            <div class="form-group">
                                <label>CPF</label>
                                <input type="text" class="form-control" id="cpf" placeholder="CPF..." name="cpf" />
                            </div>
                            
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" placeholder="Nome do Usuário..." name="nome" />
                            </div>
                            
                            <div class="form-group">
                                <label>Sobrenome</label>
                                <input type="text" class="form-control" placeholder="Sobrenome..." name="sobrenome" />
                            </div>
                            
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email..." name="email" />
                            </div>
                            
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" class="form-control" placeholder="Senha..." name="senha" />
                            </div>
                            
                            <div class="form-group">
                                <label>Administrador</label>
                                <p>
                                    <input type="checkbox" class="form-control-feedback" checked="checked" disabled="disabled" name="admin" /> Sim
                                </p>
                            </div>
                            <button type="submit" class="btn btn-default">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/popup/jquery.blockUI.js"></script>
    </body>
</html>
