package Servlets;

import Controller.Mediator;
import Controller.Promocao;
import Model.GenericDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ProcessaPromo", urlPatterns = {"/promocoes"})
public class ProcessaPromo extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        Mediator mediator = new Mediator();
        List<String> generos = new ArrayList();
        String action = request.getParameter("action");
        if (action.equals("inserir") || action.equals("alterar")) {
            Promocao p = new Promocao();
            if (!request.getParameter("npromo").equals("")) {
                p.setNpromo(Integer.parseInt(request.getParameter("npromo")));
            } else {
                p.setNpromo(0);
            }
            p.setDescricao(request.getParameter("descricao"));
            if (!request.getParameter("desconto").equals("")) {
                p.setDesconto(Double.parseDouble(request.getParameter("desconto")));
            } else {
                p.setDesconto(0);
            }
            if (request.getParameter("p_ativo") != null) {
                p.setAtivo(true);
            } else {
                p.setAtivo(false);
            }
            if (request.getParameter("p_act") != null) {
                generos.add("1");
            }
            if (request.getParameter("p_anm") != null) {
                generos.add("2");
            }
            if (request.getParameter("p_avt") != null) {
                generos.add("3");
            }
            if (request.getParameter("p_bio") != null) {
                generos.add("4");
            }
            if (request.getParameter("p_cmd") != null) {
                generos.add("5");
            }
            if (request.getParameter("p_drm") != null) {
                generos.add("6");
            }
            if (request.getParameter("p_fts") != null) {
                generos.add("7");
            }
            if (request.getParameter("p_fcc") != null) {
                generos.add("8");
            }
            if (request.getParameter("p_hrr") != null) {
                generos.add("9");
            }
            if (request.getParameter("p_rmc") != null) {
                generos.add("10");
            }
            if (request.getParameter("p_sps") != null) {
                generos.add("11");
            }
            p.setGeneros(generos);
            boolean perro = false;
            if (p.isAtivo()) {
                int totPromoAtiva = 0;
                PrintWriter out = response.getWriter();
                try {
                    int i = p.getNpromo();
                    totPromoAtiva = mediator.totPromoAtiva(i, p);
                    System.out.println(totPromoAtiva);
                    if (totPromoAtiva > 0) {
                        perro = true;
                        request.setAttribute("tipoerro", "inserir uma nova promoção");
                        request.setAttribute("descerro", "Ao inserir/modificar uma promoção, tenha certeza de que"
                                + " não existe nenhuma outra promoção aplicável ao(s) mesmo(s) gênero(s)."
                                + " Pois só é permitido uma promoção ativa por gênero.");
                        RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin-erro.jsp");
                        rd.forward(request, response);
                    }
                } catch (Exception erro) {
                    response.getWriter().print(erro);
                }
            }

            if (!perro) {
                GenericDAO gen = new GenericDAO();
                if (p.getNpromo() == 0) {
                    try {
                        mediator.inserePromo(p);
                        String sid = "" + p.getNpromo();
                        gen.InsereGenero(sid, "npromo", p.getGeneros());
                        synchronized(response){
                            response.wait(2000);
                        }
                        response.sendRedirect("./promocao-todas.jsp");
                    } catch (Exception erro) {
                        response.getWriter().print(erro);
                    }
                }
                else{
                    try{
                        mediator.updatePromo(p);
                        String sid = "" + p.getNpromo();
                        gen.AtualizaGenero(sid, "npromo", p.getGeneros());
                        response.sendRedirect("./promocao-todas.jsp");
                    } catch (Exception erro) {
                        response.getWriter().print(erro);
                    }
                }
            }
            
        } else if (action.equals("remover")) {
            String id = request.getParameter("id");
            try {
                mediator.removePromo(id);
                response.sendRedirect("./promocao-todas.jsp");
            } catch (Exception erro) {
                response.getWriter().print(erro);
            }
        } else if (action.equals("setAlterar")){
            String id = request.getParameter("id");
            Promocao p = mediator.getPromo(id);
            request.setAttribute("promo", p);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/nova-promocao.jsp?action=alterar");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaPromo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaPromo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
