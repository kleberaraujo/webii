<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CineClub Administrador">
        <meta name="author" content="Kleber Araujo">
        <title>CineClub | Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.mask.js"></script>
        <script src="js/jquery.mask.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#cpf').mask('999.999.999-99');
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-8">
                        <h1>${tpusers}</h1>
                    </div>
                    <div class="col-lg-4">
                        <p class="pesquisa">
                            <form method="POST" action="usuarios?action=pesq" >
                                <input name="pesq" class="form-control" placeholder="Buscar...">
                                <!--<button type="submit" class="btn btn-primary">Pesquisar</button>-->
                            </form>
                        </p>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${tpusers eq 'Administradores'}">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bs-example">
                                    <c:forEach var="u" items="${usuarios}">
                                        <div class="jumbotron">
                                            <div class="row">
                                                <h2 id="h2">
                                                    ${u.nome} ${u.sobrenome}
                                                </h2>
                                                <p id="tabcpf">
                                                    <b>CPF:</b> ${u.cpf}
                                                </p>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div class="bs-example">
                                <c:forEach var="u" items="${usuarios}">
                                    <div class="jumbotron">
                                        <div class="row" id="ptext">
                                            <div class="text-center" id="ftuser">
                                                <div class="panel panel-default" id="ftuser" >
                                                    <div class="panel-body-img">
                                                        <img class="panel-body-img" src="./${u.img}"/>
                                                    </div>
                                                </div>            
                                            </div>
                                            <h2 id="h2">
                                                ${u.nome} ${u.sobrenome}
                                            </h2>
                                            <p>
                                                <b>RG:</b> ${u.rg}<br/>
                                                <b>CPF:</b> ${u.cpf}<br/>
                                                <b>Email:</b> ${u.email}<br/>
                                            </p>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div><!--
            <div class="col-lg-12">
                <ul class="pagination">
                    <li class="disabled"><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            </div>-->
        </div>
        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
