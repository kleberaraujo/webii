<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${acesso != 0}">
    <c:redirect url="./"/>
</c:if>
<c:if test="${empty login}">     
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nãotebuk - Área Gerencial</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <script type="text/javascript" src="./javascript/jquery-1.3.2.js"></script>
        <script type="text/javascript" src="./javascript/animacoes.js"></script>
        <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
        <script type="text/javascript" src="./javascript/funcoes.js"></script>
    </head>
    <body>
        <h1>Management Area</h1>

        <ul class="ul_menu">
                <li id="li_cadastro"><a title="cadastro" href="CadastroUsuario.jsp">Novo usuário</a></li>
                <li id="li_logout"><a title="sair" href="./Processar?action=efetuarLogout">Logout</a></li>
        </ul>

        <div id="main_box">
                <a class="b_option" href="./Processar?action=PedidosAbertos"> Pedidos Abertos Hoje <br> ${TotalConsertos[0]}</a>

                <a class="b_option" href="./Processar?action=ConsertosEmAndamento"> Consertos em Aberto <br> ${TotalConsertos[1]}</a>

                <a class="b_option" href="./Processar?action=ConsertosRealizados"> Consertos Realizados <br> ${TotalConsertos[2]}</a>

                <!-- <a class="b_option" href="./Processar?action=ConcertosEntregues"> Consertos entregues <br> ${TotalConsertos[3]}</a> -->
                
                <a class="b_option" href="./Processar?action=ConcertosAtrasados"> Consertos Atrasados <br> ${TotalConsertos[4]}</a>
        </div>
    </body>
</html>