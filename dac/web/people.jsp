<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html class="no-js">
    <head>
        <meta charset="utf-8"/>
        <title> CineClub | People</title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="blog">
        
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">
            <div class="wrapper cf">

                <!-- posts list -->
                <div id="posts-list" class="cf">        					

                    <!-- Standard -->
                    <article class="format-video">
                        <div class="feature-image">
                            <div class="fundo_fimg">
                                <span>
                                    ${see.nome} ${see.sobrenome}
                                </span>
                            </div>
                        </div>

                        <div class="box cf">

                            <div class="p_img_ft">
                                <img src="./${see.img}" />
                            </div>
                            
                            <div class="excerpt">
                                <br />
                                <br />
                                <c:forEach var="g" items="${see.genero}">
                                    <c:choose>
                                        <c:when test="${g == 1 }"><a class="post-heading">&#8594 Ação</a></c:when>
                                        <c:when test="${g == 2 }"><a class="post-heading">&#8594 Animação</a></c:when>
                                        <c:when test="${g == 3 }"><a class="post-heading">&#8594 Aventura</a></c:when>
                                        <c:when test="${g == 4 }"><a class="post-heading">&#8594 Biografia</a></c:when>
                                        <c:when test="${g == 5 }"><a class="post-heading">&#8594 Comédia</a></c:when>
                                        <c:when test="${g == 6 }"><a class="post-heading">&#8594 Drama</a></c:when>
                                        <c:when test="${g == 7 }"><a class="post-heading">&#8594 Fantasia</a></c:when>
                                        <c:when test="${g == 8 }"><a class="post-heading">&#8594 Ficção</a></c:when>
                                        <c:when test="${g == 9 }"><a class="post-heading">&#8594 Horror</a></c:when>
                                        <c:when test="${g == 10}"><a class="post-heading">&#8594 Romance</a></c:when>
                                        <c:when test="${g == 12}"><a class="post-heading">&#8594 Suspense</a></c:when>
                                    </c:choose>
                                </c:forEach>
                                
                                <c:choose >
                                    <c:when test="${see.cpf == usuario.cpf}">
                                        <a class="bt_alterar" href="usuarios?action=edit">Editar</a>
                                    </c:when>
                                    <c:when test="${see.cpf != usuario.cpf && usuario.cpf != null }" >
                                        <c:if test="${!isamigo}">
                                            <a class="bt_alterar" href="usuarios?action=addFriend&id=${see.id}">Adicionar</a>
                                        </c:if>
                                    </c:when>
                                </c:choose>
                                        
                            </div>

                            <div class="meta">
                                
                            </div>
                        </div>
                    </article>
                    <!-- ENDS  Standard -->    

                </div>
                <!-- ENDS posts list -->

                <!-- sidebar -->
                <aside id="sidebar">

                    <!-- <ul>
                            <li class="block">
                                    <h4>Text Widget</h4>
                                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. 
                            </li>
                            
                    </ul> -->

                </aside>
                <!-- ENDS sidebar -->


            </div><!-- ENDS WRAPPER -->
        </div>
        <!-- ENDS MAIN -->
        <%@include file="./includes/footer.jsp" %>
    </body>
</html>