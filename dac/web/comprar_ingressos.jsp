<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CineClub - Comprar Ingressos</title>
        <%@include file="./includes/scripts_inc.jsp" %>
        <link rel="stylesheet" href="css/compra.css" />
        <script src="js/ajax/ajax.js"></script>
    </head>
    <body class="blog" onload="f_getHorarios(${idf});">
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">            
            <div class="wrapper cf" id="backmain">
                <div id="posts-list" class="cf">

                    <!-- Standard -->
                    <article class="format-video">
                        <div class="feature-image">
                            <img src="./${filme.imagem}" alt="Alt text" />
                        </div>
                        <div class="box cf">

                            <div class="excerpt">
                                <a class="post-heading">${filme.titulo}</a>
                                <p></p>
                                <hr style="float: left; width: 95%; background-color: lightgrey; height: 1px; border: 0px; margin-bottom: 40px; ">
                                <form method="post" id="commentform" action="processaSessao?action=carrinho&option=add&filme=${idf}" >
                                    <p class="comment-form-comment">
                                        Dia:
                                        <select name="data" id="data" onchange="f_getHorarios(${idf});">
                                            <c:forEach var="d" items="${dataSessoes}">
                                                <option value="${d}">${d}</option>
                                            </c:forEach>
                                        </select>
                                    </p>

                                    <div id="dcompra">
                                        <div id="dheader">
                                            <div class="dhselec">Selec.</div>
                                            <div id="dhsala">Sala</div>
                                            <div id="dhorario">Horário</div>
                                            <div id="dhvalor">Valor</div>
                                            <div class="dhqtde">Qtd. Inteira</div>
                                            <div class="dhqtde2">Qtd. Meia</div>
                                        </div>
                                        <div id="ditens"> </div>
                                        <div id="dfooter">
                                            <h4 id="subtotal">
                                                Subtotal: R$ 0,00
                                            </h4>
                                        </div>
                                    </div>

                                    <br/>
                                    <p>
                                        <input type="submit"  class="learnmore" value="Adicionar ao carrinho" >
                                    </p>
                                </form>

                            </div>

                            <div class="meta">
                                <span class="genero"></span>
                                <span class="format">${filme.diretor}</span>
                                <span class="tempo">${filme.duracao} min.</span>
                                <span class="classif">${filme.idademin} anos</span>
                            </div>
                        </div>
                    </article>
                    <!-- ENDS  Standard -->
                </div>
            </div>
        </div>
        <!-- corpo -->
        <!-- ENDS MAIN -->
        <%@include file="./includes/footer.jsp" %>
        <script>

            function f_bloq(v) {
                var f = document.getElementById("fselec" + v);
                var i = document.getElementById("fint" + v);
                var m = document.getElementById("fmeia" + v);
                if (f.checked) {
                    i.removeAttribute('disabled');
                    m.removeAttribute('disabled');
                }
                else {
                    i.value = null;
                    m.value = null;
                    i.setAttribute('disabled', 'disabled');
                    m.setAttribute('disabled', 'disabled');
                }
                f_sum();
            }
            ;

            function verifBloq(v) {
                if (!document.getElementById("fselec" + v).checked) {
                    alert("Selecione a Sessao para selecionar \na quantidade de ingressos");
                }
            }
            ;

            function f_sum() {
                var div = document.getElementById("ditens");
                var inputs = div.getElementsByTagName("input");
                var valor = 0;
                var total = 0;
                for (i = 0; i < inputs.length; i++) {
                    if (inputs[i].id == 'rvalue') {
                        valor = inputs[i].value;
                    }
                    var campo = inputs[i].id.substring(0, (inputs[i].id.length - 2));
                    if (campo == 'fint') {
                        total += valor * inputs[i].value;
                    }
                    else if (campo == 'fmeia') {
                        total += (valor / 2) * inputs[i].value;
                    }
                }
                total = formatReal(total);
                document.getElementById("subtotal").innerHTML = 'Subtotal: R$ ' + total;
            }

            function formatReal(num)
            {
                x = 0;
                if (num < 0) {
                    num = Math.abs(num);
                    x = 1;
                }
                if (isNaN(num))
                    num = "0";
                cents = Math.floor((num * 100 + 0.5) % 100);
                num = Math.floor((num * 100 + 0.5) / 100).toString();
                if (cents < 10)
                    cents = "0" + cents;
                for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                    num = num.substring(0, num.length - (4 * i + 3)) + '.'
                            + num.substring(num.length - (4 * i + 3));
                ret = num + ',' + cents;
                if (x == 1)
                    ret = ' - ' + ret;
                return ret;
            }
        </script>
    </body>
</html>
