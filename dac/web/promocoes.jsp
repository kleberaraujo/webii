<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="promo" class="Model.PromoDAO"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> CineClub | Promoções </title>
        <%@include file="./includes/scripts_inc.jsp" %>
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">
            <div class="wrapper cf">
                <!-- page content-->
                <div id="page-content-sb" class="cf">        	
                    <!-- entry-content -->	
                    <div class="entry-content cf">
                        <div id="posts-list" class="cf">
                            <!-- Standard -->
                            <article class="format-video">
                                <div class="feature-image">
                                    <img src="img/dummies/promo.jpg" />
                                </div>
                                <div class="box cf">
                                    <c:forEach var="p" items="${promo.lista}">
                                        <h2 id="h2">
                                            ${p.descricao}
                                        </h2>
                                        <br/>
                                        <b id="b">Desconto de:</b> ${p.desconto}%
                                        <br/>
                                        <b id="b">Para todos os filmes de:</b>
                                        <c:forEach var="g" items="${p.generos}">
                                             <c:choose>
                                                <c:when test="${g == 'Acao' }"> Ação</c:when>
                                                <c:when test="${g == 'Animacao' }"> Animação</c:when>
                                                <c:when test="${g == 'Comedia' }"> Comédia</c:when>
                                                <c:when test="${g == 'Ficcao' }"> Ficção</c:when>
                                                <c:otherwise>${g}</c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                        <br/>
                                        <hr style="width: 65%; float: left; margin-top: 20px; background-color: #EFEEEE;">
                                        <br/>
                                    </c:forEach>
                                </div>
                            </article>
                            <!-- ENDS  Standard -->
                        </div>
                    </div>
                    <!-- ENDS entry content -->
                </div>
                <!-- ENDS page content-->
                <!-- sidebar -->
                <aside id="sidebar">
                    <ul>
                        <li class="block">
                            <h4>Aproveite!</h4>
                            <p>
                                Confira as promoções que estão rolando.
                                Fique sabendo em primeira mão e assista os filmes que você
                                gosta com desconto.
                            </p>
                        </li>
                    </ul>
                </aside>
                <!-- ENDS sidebar -->
            </div><!-- ENDS WRAPPER -->
        </div>
        <!-- ENDS MAIN -->        
        <%@include file="./includes/footer.jsp" %>
    </body>
</html>
