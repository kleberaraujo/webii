/*
Navicat PGSQL Data Transfer

Source Server         : Postgres
Source Server Version : 90304
Source Host           : localhost:5432
Source Database       : ntembuk
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90304
File Encoding         : 65001

Date: 2014-05-26 19:54:30
*/


-- ----------------------------
-- Sequence structure for idcidade
-- ----------------------------
DROP SEQUENCE "public"."idcidade";
CREATE SEQUENCE "public"."idcidade"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."idcidade"', 2, true);

-- ----------------------------
-- Sequence structure for idcliente
-- ----------------------------
DROP SEQUENCE "public"."idcliente";
CREATE SEQUENCE "public"."idcliente"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"public"."idcliente"', 5, true);

-- ----------------------------
-- Sequence structure for idconserto
-- ----------------------------
DROP SEQUENCE "public"."idconserto";
CREATE SEQUENCE "public"."idconserto"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."idconserto"', 7, true);

-- ----------------------------
-- Sequence structure for idstatus
-- ----------------------------
DROP SEQUENCE "public"."idstatus";
CREATE SEQUENCE "public"."idstatus"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."idstatus"', 4, true);

-- ----------------------------
-- Sequence structure for idtipo
-- ----------------------------
DROP SEQUENCE "public"."idtipo";
CREATE SEQUENCE "public"."idtipo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."idtipo"', 2, true);

-- ----------------------------
-- Table structure for cidade
-- ----------------------------
DROP TABLE IF EXISTS "public"."cidade";
CREATE TABLE "public"."cidade" (
"idcidade" int4 NOT NULL,
"nome" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cidade
-- ----------------------------
INSERT INTO "public"."cidade" VALUES ('1', 'Adrianópolis');
INSERT INTO "public"."cidade" VALUES ('2', 'Agudos do Sul');
INSERT INTO "public"."cidade" VALUES ('3', 'Almirante Tamandaré');
INSERT INTO "public"."cidade" VALUES ('4', 'Araucária');
INSERT INTO "public"."cidade" VALUES ('5', 'Balsa Nova');
INSERT INTO "public"."cidade" VALUES ('6', 'Bocaiúva do Sul');
INSERT INTO "public"."cidade" VALUES ('7', 'Campina Grande do Sul');
INSERT INTO "public"."cidade" VALUES ('8', 'Campo do Tenente');
INSERT INTO "public"."cidade" VALUES ('9', 'Campo Largo');
INSERT INTO "public"."cidade" VALUES ('10', 'Campo Magro');
INSERT INTO "public"."cidade" VALUES ('11', 'Cerro Azul');
INSERT INTO "public"."cidade" VALUES ('12', 'Colombo');
INSERT INTO "public"."cidade" VALUES ('13', 'Contenda');
INSERT INTO "public"."cidade" VALUES ('14', 'Curitiba');
INSERT INTO "public"."cidade" VALUES ('15', 'Doutor Ulysses');
INSERT INTO "public"."cidade" VALUES ('16', 'Fazendo Rio Grande');
INSERT INTO "public"."cidade" VALUES ('17', 'Itaperuçu');
INSERT INTO "public"."cidade" VALUES ('18', 'Lapa');
INSERT INTO "public"."cidade" VALUES ('19', 'Mandirituba');
INSERT INTO "public"."cidade" VALUES ('20', 'Piên');
INSERT INTO "public"."cidade" VALUES ('21', 'Pinhais');
INSERT INTO "public"."cidade" VALUES ('22', 'Piraquara');
INSERT INTO "public"."cidade" VALUES ('23', 'Quatro Barras');
INSERT INTO "public"."cidade" VALUES ('24', 'Quitandinha');
INSERT INTO "public"."cidade" VALUES ('25', 'Rio Branco do Sul');
INSERT INTO "public"."cidade" VALUES ('26', 'Rio Negro');
INSERT INTO "public"."cidade" VALUES ('27', 'São José dos Pinhais');
INSERT INTO "public"."cidade" VALUES ('28', 'Tijucas do Sul');

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS "public"."cliente";
CREATE TABLE "public"."cliente" (
"idcliente" int4 DEFAULT nextval('idcliente'::regclass) NOT NULL,
"nome" varchar(20) COLLATE "default",
"cpf" varchar(14) COLLATE "default",
"datanasc" date,
"sexo" char(1) COLLATE "default",
"cep" varchar(10) COLLATE "default",
"cidade" int4,
"endereco" text COLLATE "default",
"email" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for conserto
-- ----------------------------
DROP TABLE IF EXISTS "public"."conserto";
CREATE TABLE "public"."conserto" (
"idcliente" int4 NOT NULL,
"idconserto" int4 DEFAULT nextval('idconserto'::regclass) NOT NULL,
"idtipo" int4,
"fabricante" varchar(20) COLLATE "default",
"modelo" varchar(30) COLLATE "default",
"dataentrada" date,
"dataentregaprevisao" date,
"dataentregareal" date,
"valor" float8,
"problema" varchar(100) COLLATE "default",
"descricao" text COLLATE "default",
"status" int4 NOT NULL,
"datainicial" date,
"datafinal" date,
"observacao" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS "public"."status";
CREATE TABLE "public"."status" (
"idstatus" int4 DEFAULT nextval('idstatus'::regclass) NOT NULL,
"descricao" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of status
-- ----------------------------
INSERT INTO "public"."status" VALUES ('1', 'Aberto');
INSERT INTO "public"."status" VALUES ('2', 'Andamento');
INSERT INTO "public"."status" VALUES ('3', 'Realizado');
INSERT INTO "public"."status" VALUES ('4', 'Entregue');

-- ----------------------------
-- Table structure for tecnicoconserto
-- ----------------------------
DROP TABLE IF EXISTS "public"."tecnicoconserto";
CREATE TABLE "public"."tecnicoconserto" (
"idconserto" int4 NOT NULL,
"login" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tecnicoconserto
-- ----------------------------

-- ----------------------------
-- Table structure for tipoconserto
-- ----------------------------
DROP TABLE IF EXISTS "public"."tipoconserto";
CREATE TABLE "public"."tipoconserto" (
"idtipo" int4 DEFAULT nextval('idtipo'::regclass) NOT NULL,
"descricao" varchar(20) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tipoconserto
-- ----------------------------
INSERT INTO "public"."tipoconserto" VALUES ('1', 'Desktop');
INSERT INTO "public"."tipoconserto" VALUES ('2', 'Notebook');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS "public"."usuario";
CREATE TABLE "public"."usuario" (
"login" varchar(50) COLLATE "default" NOT NULL,
"senha" varchar(32) COLLATE "default",
"email" varchar(50) COLLATE "default",
"tipoacesso" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO "public"."usuario" VALUES ('Jhonny', '81dc9bdb52d04dc20036dbd8313ed055', 'jhonny@vale.com.br', '0');
INSERT INTO "public"."usuario" VALUES ('Lucas', '81dc9bdb52d04dc20036dbd8313ed055', 'lucas@canestraro.com', '1');
INSERT INTO "public"."usuario" VALUES ('Kleber', '81dc9bdb52d04dc20036dbd8313ed055', 'kleber@araujo.com', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table cidade
-- ----------------------------
ALTER TABLE "public"."cidade" ADD PRIMARY KEY ("idcidade");

-- ----------------------------
-- Primary Key structure for table cliente
-- ----------------------------
ALTER TABLE "public"."cliente" ADD PRIMARY KEY ("idcliente");

-- ----------------------------
-- Primary Key structure for table conserto
-- ----------------------------
ALTER TABLE "public"."conserto" ADD PRIMARY KEY ("idconserto");

-- ----------------------------
-- Primary Key structure for table status
-- ----------------------------
ALTER TABLE "public"."status" ADD PRIMARY KEY ("idstatus");

-- ----------------------------
-- Primary Key structure for table tipoconserto
-- ----------------------------
ALTER TABLE "public"."tipoconserto" ADD PRIMARY KEY ("idtipo");

-- ----------------------------
-- Primary Key structure for table usuario
-- ----------------------------
ALTER TABLE "public"."usuario" ADD PRIMARY KEY ("login");

-- ----------------------------
-- Foreign Key structure for table "public"."cliente"
-- ----------------------------
ALTER TABLE "public"."cliente" ADD FOREIGN KEY ("cidade") REFERENCES "public"."cidade" ("idcidade") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."conserto"
-- ----------------------------
ALTER TABLE "public"."conserto" ADD FOREIGN KEY ("idcliente") REFERENCES "public"."cliente" ("idcliente") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."conserto" ADD FOREIGN KEY ("idtipo") REFERENCES "public"."tipoconserto" ("idtipo") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."conserto" ADD FOREIGN KEY ("status") REFERENCES "public"."status" ("idstatus") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."tecnicoconserto"
-- ----------------------------
ALTER TABLE "public"."tecnicoconserto" ADD FOREIGN KEY ("idconserto") REFERENCES "public"."conserto" ("idconserto") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."tecnicoconserto" ADD FOREIGN KEY ("login") REFERENCES "public"."usuario" ("login") ON DELETE NO ACTION ON UPDATE NO ACTION;
