package Model;

import Controller.Noticia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class NoticiaDAO {

    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public void InsereNoticia(Noticia n) throws Exception {
        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            if (con != null) {
                stmt = con.prepareStatement("INSERT INTO noticia (titulo,imagem,video,texto,data) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, n.getTitulo());
                stmt.setString(2, n.getImagem());
                stmt.setString(3, n.getVideo());
                stmt.setString(4, n.getTexto());
                stmt.setString(5, n.getData());
                stmt.executeUpdate();
                ResultSet rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    i = rs.getInt(1);
                }
                n.setId(i);
            } else {
                throw new Exception("Erro ao Conectar com o Banco de Dados");
            }
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

    public void AtualizaNoticia(Noticia n) throws Exception {
        try {
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("UPDATE noticia SET titulo = ?, imagem = ?, video = ?, texto = ?, data = ? WHERE nnot = ?");
            stmt.setString(1, n.getTitulo());
            stmt.setString(2, n.getImagem());
            stmt.setString(3, n.getVideo());
            stmt.setString(4, n.getTexto());
            stmt.setString(5, n.getData());
            stmt.setInt(6, n.getId());
            stmt.executeUpdate();            
        }
        catch (Exception erro) {
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

    public List<Noticia> getLista() throws Exception {
        try{
            List<Noticia> lnoticia = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT n.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + " FROM noticia n INNER JOIN genero g ON g.nnot = n.nnot ORDER BY nnot DESC;");
            rs = stmt.executeQuery();
            while(rs.next()){
                List <String> lgeneros = new ArrayList();
                Noticia n = new Noticia();
                n.setId(Integer.parseInt(rs.getString("nnot")));
                n.setTitulo(rs.getString("titulo"));
                if(rs.getString("imagem")==null){
                    n.setImagem("");
                }
                else{
                    String img = rs.getString("imagem").replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web\\", ".\\");
                    n.setImagem("\\dac\\"+img);
                }
                if(rs.getString("video")==null){
                    n.setVideo("");
                }
                else{
                    n.setVideo(rs.getString("video"));
                }
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                
                n.setTexto(rs.getString("texto"));
                n.setData(rs.getString("data"));
                n.setGeneros(lgeneros);
                
                lnoticia.add(n);
            }
            if(lnoticia == null){
                throw new Exception("vazio");
            }
            return lnoticia;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public int getTotNoticias() throws Exception{
        try{
            int i = 0;
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM noticia;");
            rs = stmt.executeQuery();
            while(rs.next()){
                i = rs.getInt("total");
            }
            return i;
        }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public Noticia getNoticia(String id) throws Exception{
        try{
            Noticia n = new Noticia();
            List<Noticia> lnoticia = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT n.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s\n"
                    + " FROM noticia n INNER JOIN genero g ON g.nnot = n.nnot WHERE n.nnot = (?);");
            stmt.setInt(1, Integer.parseInt(id));
            rs = stmt.executeQuery();
            while(rs.next()){
                List <String> lgeneros = new ArrayList();
                n.setId(Integer.parseInt(rs.getString("nnot")));
                n.setTitulo(rs.getString("titulo"));
                if(rs.getString("imagem")==null){
                    n.setImagem("");
                }
                else{
                    String img = rs.getString("imagem").replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web\\", ".\\");
                    n.setImagem(img);
                }
                if(rs.getString("video")==null){
                    n.setVideo("");
                }
                else{
                    n.setVideo(rs.getString("video"));
                }
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                
                n.setTexto(rs.getString("texto"));
                n.setData(rs.getString("data"));
                n.setGeneros(lgeneros);
                lnoticia.add(n);
            }
            if(lnoticia == null){
                throw new Exception("vazio");
            }
            return n;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }

    public void delete(Noticia n) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("DELETE FROM noticia WHERE nnot = ?;"
                                      + "DELETE FROM genero WHERE nnot = ?;");
            stmt.setInt(1, n.getId());
            stmt.setInt(2, n.getId());
            stmt.executeUpdate();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public List<Noticia> pesNoticias(String pesq) throws Exception {
        try{
            List<Noticia> lnoticia = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT n.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + " FROM noticia n INNER JOIN genero g ON g.nnot = n.nnot WHERE lower(n.titulo) LIKE lower(?) ORDER BY nnot DESC;");
            stmt.setString(1, '%'+pesq+'%');
            rs = stmt.executeQuery();
            while(rs.next()){
                List <String> lgeneros = new ArrayList();
                Noticia n = new Noticia();
                n.setId(Integer.parseInt(rs.getString("nnot")));
                n.setTitulo(rs.getString("titulo"));
                if(rs.getString("imagem")==null){
                    n.setImagem("");
                }
                else{
                    String img = rs.getString("imagem").replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web\\", ".\\");
                    n.setImagem("\\dac\\"+img);
                }
                if(rs.getString("video")==null){
                    n.setVideo("");
                }
                else{
                    n.setVideo(rs.getString("video"));
                }
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                
                n.setTexto(rs.getString("texto"));
                n.setData(rs.getString("data"));
                n.setGeneros(lgeneros);
                
                lnoticia.add(n);
            }
            if(lnoticia == null){
                throw new Exception("vazio");
            }
            return lnoticia;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
}
