package Infra;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    
        public static Connection getConnection() throws Exception{
        
        Connection con = null;
        try{
            Class.forName("org.postgresql.Driver");
            String URL     = "jdbc:postgresql://localhost:5432/ntembuk";
            String usuario = "postgres";
            String senha   = "postgres";
            
            con = DriverManager.getConnection(URL, usuario, senha);
        }
        catch(ClassNotFoundException | SQLException erro){
            throw new Exception(erro);
        }
        if (con != null) {
            return con;
        }
        else{
            return con;
        }
    }
}
