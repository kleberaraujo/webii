<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<!DOCTYPE html>
<html>
    <head>
        <title>CineClub | Cadastro </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="./includes/scripts_inc.jsp" %>
        <script type="text/javascript" src="./js/validacoes.js"></script>
    </head>
    <body class="home">

        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>

        <!-- MAIN -->
        <div id="main">
            <div class="wrapper cf">
                <div id="respond">
                    <c:choose>
                        <c:when test="${empty p.id}">
                            <h2 id="reply-title">Faça parte do Club</h2>
                        </c:when>
                        <c:otherwise>
                            <h2 id="reply-title">Meus dados</h2>
                        </c:otherwise>
                    </c:choose>
                    <form method="post" id="commentform" 
                          action="usuarios?action=${p.id == null || p.id == 0 ? "inserir" : "atualizar" }" enctype="multipart/form-data">
                        
                        <input type="hidden" name="antid" value="${p.id}" />
                        <input type="hidden" name="antimg" value="${p.img}" />
                                                
                        <p class="comment-form-comment">
                            <label for="comment">Nome:</label>
                            <input name="nome" size="60px" value="${p.nome}" />
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">Sobrenome:</label>
                            <input name="sobrenome" size="60px" value="${p.sobrenome}" />
                        </p>

                        <p class="comment-form-comment">
                            <input type="hidden" id="sexo" value="${p.sexo}">
                            <label for="comment">Sexo:</label>
                            <select id="sex" name="sexo">
                                <option value="F">Feminino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">CPF:</label>
                            <input id="cpf" name="cpf" size="30px" value="${p.cpf}" />
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">RG:</label>
                            <input name="rg" size="30px" value="${p.rg}" />
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">Email:</label>
                            <input type="email" name="email" size="60px" value="${p.email}" />
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">Senha:</label>
                            <input size="40px" name="senha" type="password" value="${p.senha}" />
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">Foto de perfil:</label>
                            <input id="input_file" name="file" type="file"/>
                            <c:if test="${p.id != null || p.id != 0}">
                                <label id="comment_note">
                                    *Nota: Caso não seja selecionada nenhuma imagem, sua conta permanecerá com a imagem atual
                                </label>
                            </c:if>
                        </p>

                        <p class="comment-form-comment">
                            <label for="comment">Preferências:</label>
                        </p>

                        <p>
                            <input type="hidden" id="gen" value="${p.genero}">
                            <input type="checkbox" name="p_act" id="p_act" > Ação &nbsp
                            <input type="checkbox" name="p_anm" id="p_anm" > Animação &nbsp
                            <input type="checkbox" name="p_avt" id="p_avt" > Aventura &nbsp
                            <input type="checkbox" name="p_bio" id="p_bio" > Biografias &nbsp
                            <input type="checkbox" name="p_cmd" id="p_cmd" > Comédia &nbsp
                            <input type="checkbox" name="p_drm" id="p_drm" > Drama &nbsp
                            <input type="checkbox" name="p_fts" id="p_fts" > Fantasia &nbsp
                            <input type="checkbox" name="p_fcc" id="p_fcc" > Ficção &nbsp
                            <input type="checkbox" name="p_hrr" id="p_hrr" > Horror &nbsp
                            <input type="checkbox" name="p_rmc" id="p_rmc" > Romance &nbsp
                            <input type="checkbox" name="p_sps" id="p_sps" > Suspense
                        </p>
                        <c:choose>
                            <c:when test="${empty p.id}">
                                <input name="submit" type="submit" class="submitcadastro" value="Quero fazer parte do Club">
                            </c:when>
                            <c:otherwise>
                                <input name="submit" type="submit" class="submitcadastro" value="Atualizar"/>
                            </c:otherwise>
                        </c:choose>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="./includes/footer.jsp" %>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
