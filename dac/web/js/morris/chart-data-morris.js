// Chart
var x = document.getElementById("chart").value;
var data =  new Array();
var s1 = x.replace("[", "");
s1 = s1.replace("]", "");
var s = s1.split(",");
var count = 0;
for(a = 0 ; a < s.length ; a = a+2 ){
    var d = new Date(s[a]);
    var d1 = d.toISOString().substring(0, 10);
    data[count] = { d: d1 , Vendas: s[a+1] } ;
    count++;
}
//End chart
Morris.Area({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-area',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart. { d: '2014-03-01', Vendas: 4 }
  data: data,
  
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['Vendas'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Vendas'],
  // Disables line smoothing
  smooth: true,
});
//percent
var p = document.getElementById("percgen").value;
s1 = p.replace("[", "");
s1 = s1.replace("]", "");
s = s1.split(",");
var data2 =  new Array();
data2[0] = { label: "Ação" , value: ( 100 * s[1] / s[0]).toFixed(2) };
data2[1] = { label: "Animação" , value: ( 100 * s[2] / s[0]).toFixed(2) };
data2[2] = { label: "Aventura" , value: ( 100 * s[3] / s[0]).toFixed(2) };
data2[3] = { label: "Biografias" , value: ( 100 * s[4] / s[0]).toFixed(2) };
data2[4] = { label: "Comédia" , value: ( 100 * s[5] / s[0]).toFixed(2) };
data2[5] = { label: "Drama" , value: ( 100 * s[6] / s[0]).toFixed(2) };
data2[6] = { label: "Fantasia" , value: ( 100 * s[7] / s[0]).toFixed(2) };
data2[7] = { label: "Ficção" , value: ( 100 * s[8] / s[0]).toFixed(2) };
data2[8] = { label: "Horror" , value: ( 100 * s[9] / s[0]).toFixed(2) };
data2[9] = { label: "Romance" , value: ( 100 * s[10] / s[0]).toFixed(2) };
data2[10] = { label: "Suspense" , value: ( 100 * s[11] / s[0]).toFixed(2) };

//end percent
Morris.Donut({
  element: 'morris-chart-donut',
  //{label: "Referral", value: 42.7},
  data: data2,
  formatter: function (y) { return y + "%" ;}
});

Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-line',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
	
  ],
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['visits'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Visits'],
  // Disables line smoothing
  smooth: false,
});
/*
Morris.Bar ({
  element: 'morris-chart-bar',
  data: [
	{device: 'iPhone', geekbench: 136},
	{device: 'iPhone 3G', geekbench: 137},
	{device: 'iPhone 3GS', geekbench: 275},
	{device: 'iPhone 4', geekbench: 380},
	{device: 'iPhone 4S', geekbench: 655},
	{device: 'iPhone 5', geekbench: 1571}
  ],
  xkey: 'device',
  ykeys: ['geekbench'],
  labels: ['Geekbench'],
  barRatio: 0.4,
  xLabelAngle: 35,
  hideHover: 'auto'
});
*/