<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:setProperty name="filme" property="*" />
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CineClub | Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script>
            tinymce.init({selector: 'textarea'});
        </script>
    </head>

    <body>

        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>

            <div id="page-wrapper">

                <div class="row">
                    <div class="col-lg-12">
                        <h1>Cadastro de Filme</h1>
                        <form onsubmit="f_message()" action="filmes?action=${param.action == null ? "inserir" : param.action }" method="POST" enctype="multipart/form-data" >
                            <input type="hidden" name="id" value="${filme.id}" />
                            <input type="hidden" name="antimg" value="${filme.imagem}" />
                            <div class="form-group">
                                <label>Título</label>
                                <input class="form-control" name="titulo" placeholder="Título do filme..." value="${filme.titulo}" />
                            </div>

                            <div class="form-group">
                                <label>Diretor</label>
                                <input class="form-control"  name="diretor" placeholder="Diretor do filme..." value="${filme.diretor}" />
                            </div>

                            <div class="form-group">
                                <label>Duração</label>
                                <input class="form-control"  name="duracao" placeholder="Duração em minutos..." value="${filme.duracao}" />
                            </div>

                            <div class="form-group">
                                <label>Faixa etária</label>
                                <input class="form-control" name="idademin" placeholder="Idade mínima..." value="${filme.idademin}" />
                            </div>

                            <div class="form-group">
                                <label>Imagem</label>
                                <input type="file" name="imagem" />
                                <c:if test="${filme.id != 0}" >
                                    <span class="notas">
                                        *Nota: Caso não seja selecionado nenhuma imagem a notícia permanecerá com a imagem atual
                                    </span>
                                </c:if>
                            </div>

                            <div class="form-group">
                                <label>Vídeo</label>
                                <input class="form-control" placeholder="ID do video no YouTube..." name="video" value="${filme.video}"/>
                            </div>

                            <div class="form-group">
                                <label>Gênero</label>
                                <p>
                                    <input type="hidden" id="gen" value="${filme.generos}"/>
                                    <input type="checkbox" name="p_act" id="p_act" > Ação &nbsp
                                    <input type="checkbox" name="p_anm" id="p_anm" > Animação &nbsp
                                    <input type="checkbox" name="p_avt" id="p_avt" > Aventura &nbsp
                                    <input type="checkbox" name="p_bio" id="p_bio" > Biografias &nbsp
                                    <input type="checkbox" name="p_cmd" id="p_cmd" > Comédia &nbsp
                                    <input type="checkbox" name="p_drm" id="p_drm" > Drama &nbsp
                                    <input type="checkbox" name="p_fts" id="p_fts" > Fantasia &nbsp
                                    <input type="checkbox" name="p_fcc" id="p_fcc" > Ficção &nbsp
                                    <input type="checkbox" name="p_hrr" id="p_hrr" > Horror &nbsp
                                    <input type="checkbox" name="p_rmc" id="p_rmc" > Romance &nbsp
                                    <input type="checkbox" name="p_sps" id="p_sps" > Suspense
                                </p>

                                <div class="form-group">
                                    <label>Sinopse</label>
                                    <textarea class="form-control" rows="10" name="sinopse" >${filme.sinopse}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Curiosidades</label>
                                    <textarea name="curiosidades" class="form-control" rows="10">${filme.curiosidades}</textarea>
                                </div>
                                <c:if test="${not empty filme.id && filme.id != 0 && filme.id != '' }">
                                    <button type="submit" class="btn btn-default">Atualizar</button>
                                </c:if>
                                <c:if test="${empty filme.id || filme.id == 0 || filme.id == null}" >
                                    <button type="submit" class="btn btn-default">Cadastrar</button>
                                </c:if>
                            </div>
                        </form>   
                    </div><!-- /.row -->

                </div><!-- /#page-wrapper -->
            </div>
        </div><!-- /#wrapper -->

        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/tablesorter/tables.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/popup/jquery.blockUI.js"></script>
    </body>
</html>
