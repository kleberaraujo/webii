package Model;

import Controller.Filme;
import Controller.Promocao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PromoDAO {
    
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public void inserePromo(Promocao p) throws Exception{
        
        try{
            con = FactoryConnection.getConnection();
            if (con != null) {
                int i = 0;
                stmt = con.prepareStatement("INSERT INTO promocao (descricao,desconto,ativo) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, p.getDescricao());
                stmt.setDouble(2, p.getDesconto());
                if(p.isAtivo()){
                    stmt.setString(3, "X");
                }else{
                    stmt.setString(3, "");
                }
                stmt.executeUpdate();
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    i = rs.getInt(1);
                }
                p.setNpromo(i);
            }
            else{
                throw new Exception("Erro na Conexão com o Banco de Dados");
            }
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public int getTotPromo() throws Exception {
        try {
            int i = 0;
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM promocao;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                i = rs.getInt("total");
            }
            return i;
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            stmt.close();
            con.close();
        }
    }
    
    public List<Promocao> getLista() throws Exception {
        try{
            List<Promocao> lpromo = new ArrayList();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT p.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + " FROM promocao p JOIN genero g ON g.npromo = p.npromo ORDER BY npromo DESC;");
            rs = stmt.executeQuery();
            while(rs.next()){
                List <String> lgeneros = new ArrayList();
                Promocao p = new Promocao();
                p.setNpromo(Integer.parseInt(rs.getString("npromo")));
                p.setDescricao(rs.getString("descricao"));
                if(rs.getString("ativo") == null ){
                    p.setAtivo(false);
                }
                else if (rs.getString("ativo").equals("X")){
                    p.setAtivo(true);
                }
                p.setDesconto(Double.parseDouble(rs.getString("desconto")));
                
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                
                p.setGeneros(lgeneros);
                lpromo.add(p);
            }
            return lpromo;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public int totPromoAtiva(int id, Promocao p) throws Exception{
        int i = 0;
        boolean ctr = false;
        try {
            con = FactoryConnection.getConnection();
            String qry = "SELECT COUNT(*) AS total FROM promocao JOIN genero USING (npromo) WHERE ativo = 'X' AND npromo != ? AND (";
            for(String opt : p.getGeneros()){
                switch (opt) {
                    case "1":
                        qry = qry + "acao = 'X' OR ";
                        break;
                    case "2":
                        qry = qry + "animacao = 'X' OR ";
                        break;
                    case "3":
                        qry = qry + "aventura = 'X' OR ";
                        break;
                    case "4":
                        qry = qry + "bio = 'X' OR ";
                        break;
                    case "5":
                        qry = qry + "comedia = 'X' OR ";
                        break;
                    case "6":
                        qry = qry + "drama = 'X' OR ";
                        break;
                    case "7":
                        qry = qry + "fantasia = 'X' OR ";
                        break;
                    case "8":
                        qry = qry + "ficcao = 'X' OR ";
                        break;
                    case "9":
                        qry = qry + "horror = 'X' OR ";
                        break;
                    case "10":
                        qry = qry + "romance = 'X' OR ";
                        break;
                    case "11":
                        ctr = true;
                        qry = qry + "suspense = 'X')";
                        break;
                }
            }
            if(!ctr){
                qry = qry.substring(0, qry.length() - 4) + ");";
            }
            System.out.println(qry);
            stmt = con.prepareStatement(qry);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                i = rs.getInt("total");
            }
            System.out.println(i);
            return i;
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public void removePromo(String id) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("DELETE FROM promocao WHERE npromo = ?;"
                                      + "DELETE FROM genero   WHERE npromo = ?");
            stmt.setInt(1, Integer.parseInt(id));
            stmt.setInt(2, Integer.parseInt(id));
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public Promocao getPromo(String id) throws Exception {
        try{
            Promocao p = new Promocao();
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("SELECT p.*, g.acao AS ac, g.animacao AS an, g.aventura AS av,"
                    + "g.bio AS b, g.comedia AS co, g.drama AS d, g.fantasia AS fa, g.ficcao AS fi,"
                    + "g.horror AS h, g.romance AS r, g.suspense AS s "
                    + " FROM promocao p JOIN genero g USING (npromo) WHERE npromo = ? ;");
            stmt.setInt(1, Integer.parseInt(id));
            rs = stmt.executeQuery();
            while (rs.next()) {
                List <String> lgeneros = new ArrayList();
                p.setNpromo(Integer.parseInt(rs.getString("npromo")));
                p.setDescricao(rs.getString("descricao"));
                if(rs.getString("ativo") == null ){
                    p.setAtivo(false);
                }
                else if (rs.getString("ativo").equals("X")){
                    p.setAtivo(true);
                }
                p.setDesconto(Double.parseDouble(rs.getString("desconto")));
                
                if("X".equals(rs.getString("ac")))
                    lgeneros.add("Acao");  
                if("X".equals(rs.getString("an")))
                    lgeneros.add("Animacao");
                if("X".equals(rs.getString("av")))
                    lgeneros.add("Aventura");
                if("X".equals(rs.getString("b")))
                    lgeneros.add("Biografia");
                if("X".equals(rs.getString("co")))
                    lgeneros.add("Comedia");
                if("X".equals(rs.getString("d")))
                    lgeneros.add("Drama");
                if("X".equals(rs.getString("fa")))
                    lgeneros.add("Fantasia");
                if("X".equals(rs.getString("fi")))
                    lgeneros.add("Ficcao");
                if("X".equals(rs.getString("h")))
                    lgeneros.add("Horror");
                if("X".equals(rs.getString("r")))
                    lgeneros.add("Romance");
                if("X".equals(rs.getString("s")))
                    lgeneros.add("Suspense");
                
                p.setGeneros(lgeneros);
            }
            
            return p;
            
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            stmt.close();
            con.close();
        }
    }

    public void updatePromo(Promocao p) throws Exception {
        try{
            con = FactoryConnection.getConnection();
            stmt = con.prepareStatement("UPDATE promocao SET descricao = ?, desconto = ?, ativo = ? WHERE npromo = ? ;");
            stmt.setString(1, p.getDescricao());
            stmt.setDouble(2, p.getDesconto());
            if(p.isAtivo()){
                stmt.setString(3, "X");
            }else{
                stmt.setString(3, "");
            }
            stmt.setInt(4, p.getNpromo());
            stmt.executeUpdate();
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
    }
    
    public double getDescBySessao(int id) throws Exception{
        try{
            double desc = 0;
            FilmeDAO fd = new FilmeDAO();
            List<Promocao> lp = this.getLista();
            Filme f = fd.getFilmeBySessao(id);
            for(Promocao p : lp){
                for(String g : p.getGeneros() ){
                    for(String fg : f.getGeneros()){
                        if(g.equals(fg)){
                            desc = p.getDesconto();
                            return desc;
                        }
                    }
                }
            }
            return desc;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }

    public int getPromoBySessao(int id) throws Exception{
        try{
            int promo = 0;
            FilmeDAO fd = new FilmeDAO();
            List<Promocao> lp = this.getLista();
            Filme f = fd.getFilmeBySessao(id);
            for(Promocao p : lp){
                for(String g : p.getGeneros() ){
                    for(String fg : f.getGeneros()){
                        if(g.equals(fg)){
                            promo = p.getNpromo();
                            return promo;
                        }
                    }
                }
            }
            return promo;
        }
        catch(Exception erro){
            throw new Exception(erro);
        }
        finally{
            stmt.close();
            con.close();
        }
    }
}
