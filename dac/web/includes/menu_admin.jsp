<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./admin.jsp">CineClub Management</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active"><a href="admin.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="dropdown">
                <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart-o"></i> Not�cias <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="nova-noticia.jsp">Nova Not�cia</a></li>
                    <li><a href="./noticias?action=todas">Todas as Not�cias</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="tables.html" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-film"></i> Filmes <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="novo-filme.jsp">Novo Filme</a></li>
                    <li><a href="./filmes?action=todos">Todos os Filmes</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="forms.html" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-edit"></i> Promo��es <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="nova-promocao.jsp">Nova Promo��o</a></li>
                    <li><a href="promocao-todas.jsp">Todas as Promo��es</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="forms.html" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Usu�rios <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="novo-user.jsp">Novo Usu�rio</a></li>
                    <li><a href="usuarios?action=usuarios">Usu�rios</a></li>
                    <li><a href="usuarios?action=clientes">Clientes</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="forms.html" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-video-camera"></i> Sess�es <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="processaSessao?action=novaSessao">Nova Sess�o</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${usuario.nome} ${usuario.sobrenome} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="usuarios?action=logout"><i class="fa fa-power-off"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>