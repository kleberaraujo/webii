
package DAOs;

import Infra.ConnectionFactory;
import Beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO{
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public UsuarioDAO() {}
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public void Salvar(Usuario pUsuario) throws Exception{
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("INSERT INTO Usuario (login,senha,email,tipoAcesso) "
                    + "VALUES (?, md5(?), ?, ?)");
            stmt.setString(1, pUsuario.getLogin());
            stmt.setString(2, pUsuario.getSenha());
            stmt.setString(3, pUsuario.getEmail());
            stmt.setInt(4, pUsuario.getAcesso());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    
    public String Verifica(String usuario, String senha) throws Exception{
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT tipoAcesso FROM Usuario WHERE login = ? AND senha = md5(?)");
            stmt.setString(1, usuario);
            stmt.setString(2, senha);
            rs = stmt.executeQuery();
            if(rs.next()==false)
                return null;
            int tipo = rs.getInt("tipoAcesso");
            return String.valueOf(tipo);
        } catch (Exception e) {
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();
        }
    }
    // </editor-fold>
    
}
