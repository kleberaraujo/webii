
package DAOs;

import Beans.Relatorio;
import Infra.ConnectionFactory;
import Infra.Data;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class RelatorioDAO {
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sqlTemp;
    private Date sqlDate;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public RelatorioDAO() {
        sqlTemp = "SELECT co.idconserto, cli.nome, co.idtipo AS tipo, co.fabricante, co.modelo,"
                + "co.dataentrada, co.dataentregaprevisao, co.valor, co.problema, co.descricao, "
                + "co.status, tc.login AS tecnico, co.datainicial, co.datafinal, co.observacao, co.dataentregareal, "
                + "cli.idCliente, cli.cpf, cli.datanasc, cli.sexo, cli.cep, cid.nome AS cidade, "
                + "cli.endereco, cli.email, (co.dataEntregaPrevisao - ?) AS diasAtrasados "
                + "FROM conserto co "
                + "INNER JOIN cliente cli ON (co.idcliente = cli.idcliente) "
                + "INNER JOIN status s ON (co.status = s.idstatus) "
                + "INNER JOIN tipoconserto tipo ON (co.idtipo = tipo.idtipo) "
                + "INNER JOIN cidade cid ON (cid.idcidade = cli.cidade) "
                + "LEFT JOIN tecnicoconserto tc ON (tc.idconserto = co.idconserto) ";
        Data dTemp = new Data();
        java.util.Date d = dTemp.DataAtualDate();
        sqlDate  = new Date(d.getTime());
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public List<Integer> totConsertos() throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            List <Integer> lista = new ArrayList<Integer>();
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt0 = new java.sql.Date(ft.parse(s).getTime());
            //Pedidos abertos na data atual
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM conserto WHERE dataentrada = ? ");
            stmt.setDate(1, dt0);
            rs = stmt.executeQuery();
            if(rs.next()==false)
                lista.add(0);
            else
                lista.add(rs.getInt("total"));
            //Status Andamento
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM conserto WHERE status = 1 OR status = 2");
            rs = stmt.executeQuery();
            if(rs.next()==false)
                lista.add(0);
            else
                lista.add(rs.getInt("total"));
            //Status Realizado
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM conserto WHERE datafinal = ? AND status = 3 ");
            stmt.setDate(1, dt0);
            rs = stmt.executeQuery();
            if(rs.next()==false)
                lista.add(0);
            else
                lista.add(rs.getInt("total"));
            //Status Entregue
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM conserto WHERE status = 4");
            rs = stmt.executeQuery();
            if(rs.next()==false)
                lista.add(0);
            else
                lista.add(rs.getInt("total"));
            //Status Atrasado
            stmt = con.prepareStatement("SELECT COUNT(*) AS total FROM conserto "
                    + "WHERE dataEntregaPrevisao < ?\n"
                    + "AND status = 1\n"
                    + "OR dataEntregaPrevisao < ?\n"
                    + "AND status = 2");
            stmt.setDate(1, sqlDate);
            stmt.setDate(2,sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==false)
                lista.add(0);
            else
                lista.add(rs.getInt("total"));
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pedidosAbertos() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt0 = new java.sql.Date(ft.parse(s).getTime());
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE dataentrada = ? "
                        + "ORDER BY co.dataEntregaPrevisao");
            stmt.setDate(1,sqlDate);
            stmt.setDate(2, dt0);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pedidosEmAberto() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE status = 1 "
                        + "ORDER BY co.dataEntregaPrevisao");
            stmt.setDate(1,sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> consertosRealizados() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE status = 3"
                        + "ORDER BY co.idConserto");
            stmt.setDate(1,sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> consertosEmAndamento() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE status = 1 OR status = 2"
                        + "ORDER BY co.idConserto");
            stmt.setDate(1, sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> consertosEntregues() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE status = 4"
                        + "ORDER BY co.idConserto");
            stmt.setDate(1,sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> consertosAtrasados() throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE dataEntregaPrevisao < ? "
                        + "AND ( status = 1 OR status = 2 ) "
                        + "ORDER BY diasAtrasados");
            stmt.setDate(1,sqlDate);
            stmt.setDate(2,sqlDate);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            System.out.print(lista);
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> meusConsertos(String login) throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE tc.login = ?");
            stmt.setDate(1,sqlDate);
            stmt.setString(2,login);
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pesquisaConsertos(int idConserto) throws Exception{
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList();
            stmt = con.prepareStatement(sqlTemp + "WHERE co.idConserto = ?");
            stmt.setDate(1,sqlDate);
            stmt.setInt(2,idConserto);
            rs = stmt.executeQuery();
            while(rs.next()){
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }
            return lista;
        }    
        catch(Exception e){
            throw new Exception("Erro ao Pesquisar");
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pesquisaConsertos(String dataInicial, String dataFinal) throws Exception{        
        try{
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt0 = new java.sql.Date(ft.parse(s).getTime());
            java.sql.Date dt1 = new java.sql.Date(ft.parse(dataInicial).getTime());
            java.sql.Date dt2 = new java.sql.Date(ft.parse(dataFinal).getTime());
            List <Relatorio> lista = new ArrayList();
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(sqlTemp + "WHERE co.dataEntrada BETWEEN ? AND ? "
                            + "OR co.dataEntregaPrevisao BETWEEN ? AND ? "
                            + "OR co.dataInicial BETWEEN ? AND ? "
                            + "OR co.dataFinal BETWEEN ? AND ? ;");
            stmt.setDate(1,dt0);
            stmt.setDate(2,dt1);
            stmt.setDate(3,dt2);
            stmt.setDate(4,dt1);
            stmt.setDate(5,dt2);
            stmt.setDate(6,dt1);
            stmt.setDate(7,dt2);
            stmt.setDate(8,dt1);
            stmt.setDate(9,dt2);
            rs = stmt.executeQuery();
            while(rs.next()){
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                        rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                        rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                        rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                        rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                        rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                        rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }
            return lista;
         }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pesquisaConsertosRealz(String dataInicial, String dataFinal) throws Exception{        
        try{
            DateFormat df = DateFormat.getDateInstance();
            java.util.Date data1 = new java.util.Date();
            String s = df.format(data1);
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date dt0 = new java.sql.Date(ft.parse(s).getTime());
            java.sql.Date dt1 = new java.sql.Date(ft.parse(dataInicial).getTime());
            java.sql.Date dt2 = new java.sql.Date(ft.parse(dataFinal).getTime());
            List <Relatorio> lista = new ArrayList();
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(sqlTemp + "WHERE co.dataFinal BETWEEN ? AND ? ;");
            stmt.setDate(1,dt0);
            stmt.setDate(2,dt1);
            stmt.setDate(3,dt2);
            rs = stmt.executeQuery();
            while(rs.next()){
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                        rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                        rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                        rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                        rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                        rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                        rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }
            return lista;
         }
        catch(Exception e){
            throw new Exception(e);
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    
    public List<Relatorio> pesquisaConsertos(String pesquisa) throws Exception{
        
        try{
            con = ConnectionFactory.getConnection();
            List <Relatorio> lista = new ArrayList<Relatorio>();
            stmt = con.prepareStatement(sqlTemp + "WHERE cli.nome LIKE ?"
                            + "OR cli.cpf LIKE ?"
                            + "OR co.modelo LIKE ?"
                            + "OR co.descricao LIKE ?"
                            + "OR cli.endereco LIKE ?");
            stmt.setDate(1,sqlDate);
            stmt.setString(2,"%"+pesquisa+"%");
            stmt.setString(3,"%"+pesquisa+"%");
            stmt.setString(4,"%"+pesquisa+"%");
            stmt.setString(5,"%"+pesquisa+"%");
            stmt.setString(6,"%"+pesquisa+"%");
            rs = stmt.executeQuery();
            if(rs.next()==true){
                do{
                    Relatorio r = new Relatorio(rs.getInt("idconserto"), rs.getString("nome"), rs.getInt("tipo"), 
                           rs.getString("fabricante"), rs.getString("modelo"), rs.getDate("dataentrada"), rs.getDate("dataentregaprevisao"), 
                            rs.getDouble("valor"), rs.getString("problema"), rs.getString("descricao"), rs.getInt("status"),
                            rs.getString("tecnico"),rs.getDate("datainicial"), rs.getDate("datafinal"), rs.getString("observacao"), 
                            rs.getDate("dataentregareal"), rs.getInt("idcliente"), rs.getString("cpf"), rs.getDate("datanasc"),
                            rs.getString("sexo"), rs.getString("cep"), rs.getString("cidade"), rs.getString("endereco"),
                            rs.getString("email"), rs.getInt("diasAtrasados")*(-1));
                    lista.add(r);
                }while(rs.next());
            }
            return lista;
        }    
        catch(Exception e){
            throw new Exception("Erro ao Pesquisar");
        }
        finally{
            con.close();
            stmt.close();        
        }
    }
    // </editor-fold>
    
}
