<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="noti" class="Model.NoticiaDAO"/>
<jsp:useBean id="fd" class="Model.FilmeDAO"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CineClub</title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="home">		
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>

                <!-- SLIDER -->				
                <div id="home-slider" class="lof-slidecontent">

                    <div class="preload"><div></div></div>
                    <c:set var="lf" value="${fd.FSessoes}"></c:set>
                    <!-- slider content --> 
                    <div class="main-slider-content" >
                        <ul class="sliders-wrap-inner">
                            <c:forEach var="f" items="${lf}">
                                <li>
                                    <a href="filmes?action=seefilm&nfilme=${f.id}"><img id="teste" src="./${f.imagem}" title="" alt="alt" /></a>
                                    <div class="slider-description">
                                        <h4>${f.titulo}</h4>
                                        <p> 
                                            <a class="link" href="filmes?action=sessoes&filme=${f.id}"> Comprar Ingressos </a>
                                        </p>
                                    </div> 
                                </li>
                            </c:forEach>
                        </ul>  	
                    </div>
                    <!-- ENDS slider content --> 

                    <!-- slider nav -->
                    <div class="navigator-content">
                        <div class="navigator-wrapper">
                            <ul class="navigator-wrap-inner">
                                <c:forEach var="f" items="${lf}">
                                    <li><img src="./${f.imagem}" width="200px" alt="alt" /></li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="button-next">Next</div>
                        <div  class="button-previous">Previous</div>
                        <!-- REMOVED TILL FIXED <div class="button-control"><span>STOP</span></div> -->
                    </div> 
                    <!-- slider nav -->

                </div> 
                <!-- ENDS SLIDER -->

            </div>
        </header>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            
            <div class="wrapper cf">

                <!-- featured -->
                <div class="home-featured">

                    <ul id="filter-buttons">
                        <li><a href="#" class="selected" data-filter="*" >Últimas Notícias</a></li>
                        <c:if test="${not empty usuario}">
                            <li><a href="#" data-filter=".photo">Do meu interesse</a></li>
                        </c:if>
                    </ul>

                    <!-- Filter container -->
                    <div id="filter-container" class="cf">
                        <c:set var="count" value="0"></c:set>
                        <c:forEach var="n" items="${noti.lista}">
                            <c:if test="${count < 9}">
                                <figure 
                                    <c:if test="${not empty usuario}">
                                        <c:forEach var="gn" items="${n.generos}">
                                            <c:set var="ct" value="${false}"></c:set>
                                            <c:forEach var="gu" items="${usuario.generos}">
                                                <c:if test="${gn eq gu}">
                                                    <c:set var="ct" value="${true}"></c:set>
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${ct}">
                                                class="photo"
                                            </c:if>
                                            <c:if test="${ct eq false}">
                                                class="web"
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${empty usuario}">
                                        class="web"
                                    </c:if>
                                    >    
                                    <a href="noticias?action=single&noticia=${n.id}" class="thumb">
                                        <img src="${n.imagem}" alt="alt" />
                                    </a>
                                    <figcaption>
                                        <h5><a class="link-index" href="noticias?action=single&noticia=${n.id}">${n.titulo}</a></h5>
                                    </figcaption>
                                </figure>
                            </c:if>
                            <c:set var="count" value="${count+1}"></c:set>
                        </c:forEach>
                        <!-- 
                        <figure class="photo">
                                <a href="project.html" class="thumb"><img src="img/dummies/featured/04.jpg" alt="alt" /></a>
                                <figcaption>
                                        <a href="project.html" ><h3 class="heading">Pellentesque habitant morbi</h3></a>
                                        Tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. </figcaption>
                        </figure>
                        
                        
                        <figure class="web photo">
                                <a href="project.html" class="thumb"><img src="img/dummies/featured/05.jpg" alt="alt" /></a>
                                <figcaption>
                                        <a href="project.html" ><h3 class="heading">Pellentesque habitant morbi</h3></a>
                                        Tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. </figcaption>
                        </figure>
                        
                        
                        <figure class="web print">
                                <a href="project.html" class="thumb"><img src="img/dummies/featured/01.jpg" alt="alt" /></a>
                                <figcaption>
                                        <a href="project.html" ><h3 class="heading">Pellentesque habitant morbi</h3></a>
                                        Tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. </figcaption>
                        </figure> -->

                    </div><!-- ENDS Filter container -->

                </div>
                <!-- ENDS featured -->

            </div><!-- ENDS WRAPPER -->

        </div>
        <!-- ENDS MAIN -->

        <%@include file="./includes/footer.jsp" %>

    </body>
</html>