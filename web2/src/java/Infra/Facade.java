package Infra;

import Beans.Cliente;
import Beans.Conserto;
import Beans.Relatorio;
import Beans.Usuario;
import DAOs.ClienteDAO;
import DAOs.ConsertoDAO;
import DAOs.RelatorioDAO;
import DAOs.UsuarioDAO;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Facade {
    
    public Facade(){}
    
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public static void inserirCliente(Cliente tCliente) throws Exception{
        try{
            ClienteDAO tClienteDAO = new ClienteDAO();
            tClienteDAO.Salvar(tCliente);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }   
    
    public static String buscarEmail(int idConserto) throws Exception{
        try{
            ClienteDAO tClienteDAO = new ClienteDAO();
            String cliente = tClienteDAO.BuscarEmail(idConserto);
            return cliente;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<String> listarClientes() throws Exception{
        try {
            List <String> lista = new ArrayList();
            ClienteDAO tClienteDAO = new ClienteDAO();
            lista = tClienteDAO.listarClientes();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<String> listarCidades() throws Exception{
        try {
            List <String> lista = new ArrayList();
            ClienteDAO tClienteDAO = new ClienteDAO();
            lista = tClienteDAO.listarCidades();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void inserirConserto(Conserto pConserto) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            tConsertoDAO.Salvar(pConserto);
        }
        catch(Exception e){
            throw new Exception(e);
        } 
    }
    
    public static Relatorio consultarConserto(int id) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            Relatorio consertoTec = tConsertoDAO.consultarConserto(id);
            return consertoTec;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static void alterarPedidoAberto(int id) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            tConsertoDAO.AlterarPedidoAberto(id);
            tConsertoDAO.RemoverConseroTecnico(id);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static void alterarConsertoAndamento(int id, Date dataInicial, String tecnico, int status) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            tConsertoDAO.AlterarConsertoAndamento(id, dataInicial, status);
            tConsertoDAO.SalvarConsertoTecnico(id, tecnico);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static void alterarConsertoRealizado(int id, Date dataFinal, String observacao, int status) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            tConsertoDAO.AlterarConsertoRealizado(id, dataFinal, observacao, status);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static void alterarConsertoEntregue(int id, Date dataEntrega, int status) throws Exception{
        try{
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            tConsertoDAO.AlterarConsertoEntregue(id, dataEntrega, status);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> pesquisarConserto(int id) throws Exception{
        try{
            List <Relatorio> lista = new ArrayList<Relatorio>();
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            lista = tRelatorioDAO.pesquisaConsertos(id);
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> pesquisarConserto(String pesquisa) throws Exception{
        try{
            List <Relatorio> lista = new ArrayList<Relatorio>();
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            lista = tRelatorioDAO.pesquisaConsertos(pesquisa);
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> pesquisarConserto(String dataInicial, String dataFinal) throws Exception{
        try{
            List <Relatorio> lista = new ArrayList<Relatorio>();
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            lista = tRelatorioDAO.pesquisaConsertos(dataInicial, dataFinal);
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> pesquisarConsertosRealz(String dataInicial, String dataFinal) throws Exception{
        try{
            List <Relatorio> lista = new ArrayList<Relatorio>();
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            lista = tRelatorioDAO.pesquisaConsertosRealz(dataInicial, dataFinal);
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<String> listarStatus() throws Exception{
        try {
            List <String> lista = new ArrayList<String>();
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            lista = tConsertoDAO.listarStatus();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<String> listarTipo() throws Exception{
        try {
            List <String> lista = new ArrayList<String>();
            ConsertoDAO tConsertoDAO = new ConsertoDAO();
            lista = tConsertoDAO.listarTipo();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Integer> listarTotalConsertos() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Integer> lista = new ArrayList<Integer>();
            lista = tRelatorioDAO.totConsertos();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarPedidosAbertos() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.pedidosAbertos();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarPedidosEmAberto() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.pedidosEmAberto();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarConsertosRealizados() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.consertosRealizados();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarConsertosEntregues() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.consertosEntregues();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarConsertosEmAndamento() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.consertosEmAndamento();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarConsertosAtrasados() throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.consertosAtrasados();
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static List<Relatorio> listarMeusConsertos(String login) throws Exception{
        try{
            RelatorioDAO tRelatorioDAO = new RelatorioDAO();
            List<Relatorio> lista = new ArrayList<Relatorio>();
            lista = tRelatorioDAO.meusConsertos(login);
            return lista;
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static void inserirUsuario(Usuario tUsuario) throws Exception{
        try{
            UsuarioDAO tUsuarioDAO = new UsuarioDAO();
            tUsuarioDAO.Salvar(tUsuario);
        }
        catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static String verificaUsusario(String usuario, String senha) throws Exception{
        try{
            UsuarioDAO tUsuarioDAO = new UsuarioDAO();
            String i = tUsuarioDAO.Verifica(usuario, senha);
            return i;
        }
        catch(Exception e){
            throw new Exception(e);
        }
        // </editor-fold>
        
    }
}
