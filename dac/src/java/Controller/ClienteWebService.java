package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ClienteWebService {

    public static List<Filme> getFilmes() throws Exception{
        JSONObject json = readJsonFromUrl("http://culturabeta.com.br/labs/movies-at/Curitiba"); 
        int valido = (int) json.get("code");
        List<Filme> lista = new ArrayList();
        if(valido == 0){
            String titulo = null;
            JSONArray local = (JSONArray) json.get("movies");
            int tamanho = (int) json.get("count");
            for(int i = 0; i < tamanho; i++){
                JSONObject temp = (JSONObject) local.get(i);
                titulo = temp.getString("name");
                Filme filme = new Filme(titulo);
                lista.add(filme);
            }
            for( int i = 0 ; i < lista.size() -1 ; i++ ){
                Filme m1 = lista.get(i);
                Filme m2 = lista.get(i+1);
                if(m1.getTitulo().equals(m2.getTitulo()))
                    lista.remove(i);
            }
        }
        return lista;
    }
    
    private static String readAll(Reader rd) throws IOException {
      StringBuilder sb = new StringBuilder();
      int cp;
      while ((cp = rd.read()) != -1) {
        sb.append((char) cp);
      }
      return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
    
  }