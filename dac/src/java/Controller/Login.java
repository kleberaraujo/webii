package Controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html;charset=UTF-8");
        
        //Dados
        String nome      = request.getParameter("nome");
        String sobrenome = request.getParameter("sobrenome");
        String idade     = request.getParameter("idade");
        String cep       = request.getParameter("cep");
        String cidade    = request.getParameter("cidade");
        String rua       = request.getParameter("rua");
        String num       = request.getParameter("num");
        String email     = request.getParameter("email");
        
        //Upload da imagem
        String imgname;
        String imgsize;
        if(isMultipart){
            try{
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);
                List<FileItem> items = (List<FileItem>) upload.parseRequest(request);
                for(FileItem item : items) {
                    if(item.isFormField()){
                        //se for um campo normal de form
                        
                    }
                    else{
                        
                    }
                }
            }
            catch(Exception erro){
                throw new Exception("Erro ao fazer upload da imagem");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }
        catch(Exception erro){
            
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }
        catch(Exception erro){
            
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
