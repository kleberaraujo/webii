<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Nãotebuk - Concerto de Equipamentos - Login</title>
		<link rel="stylesheet" type="text/css" href="./css/login.css">
		<link rel="stylesheet" type="text/css" href="./css/style.css">
                <script type="text/javascript" src="./javascript/funcoes.js"></script>
                <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
	</head>
	<body>
		<div id="login_box">
			<h1>Nãotebuk - ${Titulo}</h1>
                        <h2>${Mensagem}</h2>
                        <c:choose>
                            <c:when test="${Titulo == 'Mensagem'}">
                                <button class="b_canc" onclick="window.close();">Fechar</button>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${acesso == 0}">
                                    <button class="b_canc" onclick="voltarMainGerente();">Voltar</button>
                                </c:if>
                                <c:if test="${acesso == 1}">
                                    <button class="b_canc" onclick="voltarMainTecnico();">Voltar</button>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
		</div>
	</body>
</html>