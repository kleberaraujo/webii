<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>CineClub - ${filme.titulo} </title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="single">
        <!-- HEADER -->
    <header>
        <div class="wrapper cf">
            <%@include file="./includes/header.jsp" %>
        </div>
    </header>
    <!-- ENDS HEADER -->

    <!-- MAIN -->
    <div id="main">
        <div class="wrapper cf">

            <!-- posts list -->
            <div id="posts-list" class="cf">   	

                <!-- Standard -->
                <article class="format-standard">
                    <div class="feature-image">
                        <img src="./${filme.imagem}" alt="Alt text" />
                    </div>
                    <div class="box cf">

                        <!-- <div class="entry-date"><div class="number">22</div><div class="month">FEV</div></div> -->

                        <div class="excerpt">
                            <div class="post-heading" > ${filme.titulo} </div>
                            <div class="entry-content">
                                <p>
                                    ${filme.sinopse}
                                </p>
                            </div>
                            
                            <c:if test="${not empty filme.video}">
                                <p>Confira o trailer</p>
                                <div class="video-container">
                                    <iframe  src="http://www.youtube.com/embed/${filme.video}" ></iframe>
                                </div>
                            </c:if>
                            <p><p><a href="filmes?action=sessoes&filme=${filme.id}" class="learnmore">Comprar Ingressos</a></p>
                        </div>

                        <div class="meta">
                            
                        </div>

                    </div>
                </article>
                <!-- ENDS  Standard -->

                <!-- comments list -->
                <div id="comments-wrap">
                    
                    <c:if test="${empty comentarios}">
                        <h4 class="heading">Nenhum comentário</h4>
                    </c:if>
                    
                    <c:if test="${not empty comentarios}">
                        <c:set var="tot" value="${0}"></c:set>
                        <c:forEach var="c" items="${comentarios}">
                            <c:set var="tot" value="${tot + 1}"></c:set>
                        </c:forEach>
                        <c:if test="${tot > 1}">
                            <h4 class="heading">${tot} Comentários</h4>
                        </c:if>
                        <c:if test="${tot == 1}">
                            <h4 class="heading">${tot} Comentário</h4>
                        </c:if>
                    </c:if>
                        
                    <ol class="commentlist">
                        <li class="comment even thread-even depth-1">

                            <c:forEach var="c" items="${comentarios}">
                                <div id="comment-1" class="comment-body cf">
                                    <img alt='' src='./${c.user.img}' class='avatar avatar-35 photo' height='45' width='45' />
                                    <div class="comment-author vcard">${c.user.nome}</div>
                                    <div class="comment-inner">
                                        <p>	
                                            ${c.comentario}
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </li>
                    </ol>
                </div>
                <!-- ENDS comments list -->

                <!-- Respond -->				
                <div id="respond">
                    <h3 id="reply-title">Deixe um Comentário
                        <small>

                        </small>
                    </h3>
                    
                    <c:if test="${empty usuario.id}">
                        Faça Login para deixar um Comentário!
                    </c:if>
                    
                    <form action="filmes?action=coment" method="post" id="commentform">
                        
                        <input type="hidden" name="coduser" value="${usuario.id}" />
                        <input type="hidden" name="nfilme" value="${filme.id}" />
                        <p class="comment-form-comment">
                            <textarea id="comment" name="coment" cols="45" rows="8"
                            <c:if test="${empty usuario.id}">
                                disabled="disabled"
                            </c:if>
                            ></textarea>
                        </p>

                        <p class="form-submit">
                            <input name="submit" type="submit" 
                            <c:if test="${empty usuario.id}">
                                disabled="disabled"
                            </c:if> class="submitcadastro" id="submit" value="Comentar">
                        </p>

                    </form>
                </div>
                <!-- ENDS Respond -->									

            </div>
            <!-- ENDS posts list -->

            <!-- sidebar -->
            <aside id="sidebar">

                <ul>
                    <li class="block">
                        <h4>CURIOSIDADES</h4>
                        <p>
                            <c:if test="${empty filme.curiosidades}">
                                - Nenhuma Curiosidade 
                            </c:if>
                            ${filme.curiosidades}
                        </p>
                    </li>

                </ul>

            </aside>
            <!-- ENDS sidebar -->

        </div><!-- ENDS WRAPPER -->
    </div>
    <!-- ENDS MAIN -->
    <%@include file="./includes/footer.jsp" %>
</body>
</html>