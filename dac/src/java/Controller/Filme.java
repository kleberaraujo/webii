package Controller;

import java.util.List;

public class Filme {
    
    private int    id;
    private String titulo;
    private String diretor;
    private int    duracao;
    private int    idademin;
    private String sinopse;
    private String curiosidades;
    private String video;
    private String imagem;
    private List<String> generos;

    public Filme(){  
    }

    public Filme(String titulo) {
        this.titulo = titulo;
    }
    
    //ID
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //Nome
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    //Diretor
    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }
    
    //Duração
    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    //Idade mínima
    public int getIdademin() {
        return idademin;
    }

    public void setIdademin(int idademin) {
        this.idademin = idademin;
    }

    //Sinopse
    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    //Curiosidades
    public String getCuriosidades() {
        return curiosidades;
    }

    public void setCuriosidades(String curiosidades) {
        this.curiosidades = curiosidades;
    }

    //Id do Vídeo
    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    //Imagem
    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public List<String> getGeneros() {
        return generos;
    }

    public void setGeneros(List<String> generos) {
        this.generos = generos;
    }
}
