
package Beans;

import java.io.Serializable;
import java.util.Date;

public class Conserto implements Serializable { 
    
    // <editor-fold defaultstate="collapsed" desc="Instâncias">
    private int idConserto;
    private int tipo;
    private int idCliente;
    private String fabricante;
    private String modelo;
    private Date dataEntrada;
    private Date dataEntregaPrevisao;
    private Date dataEntregaReal;
    private double valor;
    private String problema;
    private String descricao;
    private int status;
    private Date dataInicio;
    private Date dataFim;
    private String observacao;
    private int diasAtrasados;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Contrutores">
    public Conserto() {}

    public Conserto(int tipo, int idCliente,String fabricante, String modelo, Date dataEntrada, Date dataEntregaPrevisao, double valor, String problema, String descricao, int status) {
        this.tipo = tipo;
        this.idCliente = idCliente;
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.dataEntrada = dataEntrada;
        this.dataEntregaPrevisao = dataEntregaPrevisao;
        this.valor = valor;
        this.problema = problema;
        this.descricao = descricao;
        this.status = status;
    }
    
    public Conserto(int idConserto, int tipo, int idCliente, String fabricante, String modelo,
            Date dataEntrega, Date dataEntregaPrevisao, Date dataEntregaReal,
            double valor, String problema, String descricao, int status,
            Date dataInicio, Date dataFim, String observacao, int diasAtrasados) {
        this.idConserto = idConserto;
        this.tipo = tipo;
        this.idCliente = idCliente;
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.dataEntrada = dataEntrega;
        this.dataEntregaPrevisao = dataEntregaPrevisao;
        this.dataEntregaReal = dataEntregaReal;
        this.valor = valor;
        this.problema = problema;
        this.descricao = descricao;
        this.status = status;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.observacao = observacao;
        this.diasAtrasados = diasAtrasados;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters e Setters">
    public int getIdConserto() {
        return idConserto;
    }

    public void setIdConserto(int idConserto) {
        this.idConserto = idConserto;
    }
    
    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setidCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrada = dataEntrega;
    }

    public Date getDataEntregaPrevisao() {
        return dataEntregaPrevisao;
    }

    public void setDataEntregaPrevisao(Date dataEntregaPrevisao) {
        this.dataEntregaPrevisao = dataEntregaPrevisao;
    }

    public Date getDataEntregaReal() {
        return dataEntregaReal;
    }

    public void setDataEntregaReal(Date dataEntregaReal) {
        this.dataEntregaReal = dataEntregaReal;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public int getDiasAtrasados() {
        return diasAtrasados;
    }

    public void setDiasAtrasados(int diasAtrasados) {
        this.diasAtrasados = diasAtrasados;
    }
    // </editor-fold>
        
}