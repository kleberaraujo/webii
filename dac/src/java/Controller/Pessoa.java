package Controller;

import java.util.List;

public class Pessoa {
    
    private int     id;
    private String  cpf;
    private String  nome;
    private String  sobrenome;
    private String  idade;
    private String  rg;
    private String  datanasc;
    private String  sexo;
    private String  img;
    private String  email;
    private String  senha;
    private List<String> generos;
    private boolean admin;
    
    public Pessoa(String cpf, String nome, String sobrenome, String rg, String sexo,
                  String email, String senha, String img, List<String> gen){
        
        this.cpf = cpf;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.rg = rg;
        this.generos = gen;
        this.img = img;
        this.email = email;
        this.senha = senha;
        this.sexo = sexo;
        this.generos = gen;
    }
    public Pessoa() {}
    
    public Pessoa(String cpf, String nome, String sobrenome, String rg, String sexo,
                  String email, String senha, String img, List<String> gen, boolean admin){
        this.cpf = cpf;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.rg = rg;
        this.generos = gen;
        this.img = img;
        this.email = email;
        this.senha = senha;
        this.sexo = sexo;
        this.generos = gen;
        this.admin = admin;
    }
    
    public Pessoa(String cpf, String nome,String sobrenome,String email,String senha,boolean admin){
        this.cpf = cpf;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.admin = admin;
        this.email = email;
        this.senha = senha;
    }
    
    //ID
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    //Nome
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    //Sobrenome
    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
        
    //RG
    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
    
    //Sexo
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    //Generos
    public List<String> getGenero() {
        return getGeneros();
    }

    public void setGeneros(List<String> gen) {
        this.generos = gen;
    }
    
    //img
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    //email
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    //senha
    public String getSenha() {
        return senha;
    }
    
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the generos
     */
    public List<String> getGeneros() {
        return generos;
    }
}
