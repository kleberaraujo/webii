<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<jsp:setProperty name="promo" property="*" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> CineClub | Admin </title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Add custom CSS here -->
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <link href="css/sb-admin.css" rel="stylesheet">
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <form action="promocoes?action=${param.action == null ? "inserir" : param.action }" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1>Cadastro de Promoção</h1>
                            <input type="hidden" name="npromo" value="${promo.npromo}" />
                            <div class="form-group">
                                <label>Descrição</label>
                                <input class="form-control" value="${promo.descricao}" placeholder="Descrição da promoção" name="descricao" >
                            </div>

                            <div class="form-group">
                                <label>Desconto em %</label>
                                <input class="form-control" value="${promo.desconto}" placeholder="Desconto em %" name="desconto" >
                            </div>

                            <div class="form-group">
                                <label>Ativo</label>
                                <p>
                                    <input type="hidden" id="isAtivo" value="${promo.ativo}" />
                                    <input type="checkbox" name="p_ativo" id="p_ativo" > Sim
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Gênero</label>
                                <p>
                                    <input type="hidden" id="gen" value="${promo.generos}"/>
                                    <input type="checkbox" name="p_act" id="p_act" > Ação &nbsp
                                    <input type="checkbox" name="p_anm" id="p_anm" > Animação &nbsp
                                    <input type="checkbox" name="p_avt" id="p_avt" > Aventura &nbsp
                                    <input type="checkbox" name="p_bio" id="p_bio" > Biografias &nbsp
                                    <input type="checkbox" name="p_cmd" id="p_cmd" > Comédia &nbsp
                                    <input type="checkbox" name="p_drm" id="p_drm" > Drama &nbsp
                                    <input type="checkbox" name="p_fts" id="p_fts" > Fantasia &nbsp
                                    <input type="checkbox" name="p_fcc" id="p_fcc" > Ficção &nbsp
                                    <input type="checkbox" name="p_hrr" id="p_hrr" > Horror &nbsp
                                    <input type="checkbox" name="p_rmc" id="p_rmc" > Romance &nbsp
                                    <input type="checkbox" name="p_sps" id="p_sps" > Suspense
                                </p>
                            </div>

                            <button type="submit" class="btn btn-default">Salvar</button>
                        </div>
                    </div><!-- /.row -->
                </form>
            </div><!-- /#page-wrapper -->
        </div><!-- /#wrapper -->
        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script type="text/javascript" src="./js/jquery.mask.js"></script>
        <script type="text/javascript">
            tinymce.init({selector: 'textarea'});
            $(document).ready(function() {
                $('#percent').mask('##0,00%', {reverse: true});
            });
        </script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/popup/jquery.blockUI.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
