<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="noti" class="Model.NoticiaDAO"/>

<html class="no-js">
    <head>
        <meta charset="utf-8"/>
        <title> CineClub - Notícias </title>
        <%@include file="./includes/scripts_inc.jsp" %>	
    </head>
    <body class="blog">
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">
            <div class="wrapper cf">
                <!-- posts list -->
                <div id="posts-list" class="cf">
                    <c:forEach var="n" items="${noti.lista}">
                        <c:if test="${empty n.video }">
                            <!-- Quote -->
                            <article class="format-standard">
                                <div class="feature-image">
                                    <a>
                                        <a href="./noticias?action=single&noticia=${n.id}">
                                            <img src="${n.imagem}"/>
                                        </a>
                                    </a>
                                </div>
                            </c:if>

                            <div class="box cf">
                                <div class="entry-date"><div class="number">
                                        ${fn:substring(n.data, 0, 2)}
                                    </div><div class="month">
                                        <c:choose>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='01'}">${"JAN"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='02'}">${"FEV"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='03'}">${"MAR"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='04'}">${"ABR"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='05'}">${"MAI"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='06'}">${"JUN"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='07'}">${"JUL"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='08'}">${"AGO"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='09'}">${"SET"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='10'}">${"OUT"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='11'}">${"NOV"}</c:when>
                                            <c:when test="${(fn:substring(n.data, 3, 5))=='12'}">${"DEZ"}</c:when>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="excerpt">
                                    <a class="post-heading"  href="./noticias?action=single&noticia=${n.id}">${n.titulo}</a>
                                    <p>
                                        <a href="./noticias?action=single&noticia=${n.id}" class="submitcadastro" >Continuar lendo</a>
                                    </p>
                                </div>

                                <div class="meta">
                                    <span class="format">Noticias</span>
                                    <!-- <span class="user"><a href="#">Kleber Araujo</a></span> -->
                                    <span class="tags">
                                        <c:forEach var="g" items="${n.generos}">
                                            ${g}<br />
                                        </c:forEach>
                                    </span>
                                </div>

                            </div>
                        </article>
                        <!-- ENDS Video -->
                    </c:forEach>
                    <!-- page-navigation -->
                    <div class="page-navigation cf">
                        <div class="nav-next"><a href="#">&#8592; Notícias mais antigas </a></div>
                        <!-- <div class="nav-previous"><a href="#">Newer Entries &#8594;</a></div>-->
                    </div>
                    <!--ENDS page-navigation -->
                </div>
                <!-- ENDS posts list -->
                <!-- sidebar -->
                <aside id="sidebar">
                    <!-- <ul>
                            <li class="block">
                                    <h4>Text Widget</h4>
                            </li>
                    </ul> -->
                </aside>
                <!-- ENDS sidebar -->
            </div><!-- ENDS WRAPPER -->
        </div>
        <!-- ENDS MAIN -->
        <%@include file="./includes/footer.jsp" %>		
    </body>
</html>