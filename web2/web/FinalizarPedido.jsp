<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${empty login}">
    <c:redirect url="./"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nãotebuk - Finalizar Pedido</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <link rel="stylesheet" type="text/css" href="./css/jquery-ui-1.10.4.custom.min.css">
        <link rel="shortcut icon" href="./images/ntembuk-16x16.png" />
        <script type="text/javascript" src="./javascript/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="./javascript/jquery-ui-1.10.4.custom.js"></script>
        <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
        <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="./javascript/jquery.maskMoney.js"></script>
        <script type="text/javascript" src="./javascript/incrementaData.js"></script>
        <script type="text/javascript" src="./javascript/funcoes.js"></script>
    </head>
    
    <body onload="definecampos(); Data();">
        <h1>Pedido n° ${Conserto.conserto.idConserto}</h1>
        
        <div id="main_box2">
            <form name="f_finpedido" action="./Processar?action=AlterarConserto" method="post">
                
                <input type="hidden" name="id" value="${Conserto.conserto.idConserto}"/>
                <div class="f_row">
                    <label class="l_cadastro">Cliente:</label>
                    <input type="text" name="cliente" id="i_nome" class="i_text" readonly="readonly" value="${Conserto.cliente.nome}"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Tipo:</label>
                    <input type="text" name="tipo" class="i_text" readonly="readonly" value="<c:if test="${Conserto.conserto.tipo == 1}">Desktop</c:if><c:if test="${Conserto.conserto.tipo == 2}">Notebook</c:if>"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Fabricante:</label>
                    <input type="text" name="fabricante" class="i_text" readonly="readonly" value="${Conserto.conserto.fabricante}"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Modelo:</label>
                    <input type="text" name="modelo" class="i_text" readonly="readonly" value="${Conserto.conserto.modelo}"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Data de Entrada:</label>
                    <input type="date" name="dataEntrada" class="i_text" readonly="readonly" id="dateEntrada" value="${Conserto.conserto.dataEntrada}"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Data de Entrega:</label>
                    <input type="date" name="dataEntrega" class="i_text" id="dateEntraga" readonly="readonly" value="${Conserto.conserto.dataEntregaPrevisao}"/>
                </div>

                <div class="f_row">
                    <label for="i_valor" class="l_cadastro">Valor:</label>
                    <input type="text" id="valor" name="valor" class="i_text" readonly="readonly" value="<fmt:formatNumber value="${Conserto.conserto.valor}" type="currency"/>"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Problema:</label>
                    <input type="text" id="i_sdesc" name="problema" class="i_text" readonly="readonly" value="${Conserto.conserto.problema}"/>
                </div>

                <div class="f_row">
                    <label class="l_cadastro">Descrição completa:</label>
                    <textarea name="descricaoProblema" id="i_fdesc" class="i_text" cols=50 rows=7 readonly="readonly" >${Conserto.conserto.descricao}</textarea>
                </div>
                
                <div class="f_row">
                    <label class="l_cadastro">Status:</label>
                    <select name="s_status" id="s_status" size="1" class="combo_user" onchange="definecampos();">
                        <c:if test="${Conserto.conserto.status == 1 }">
                            <option value="1">Pedido Aberto
                            <option value="2">Concerto em Andamento
                        </c:if>
                        <c:if test="${Conserto.conserto.status == 2 }">
                            <option value="2">Concerto em Andamento
                            <option value="3">Concerto Realizado
                        </c:if>
                        <c:if test="${Conserto.conserto.status == 3 }">
                            <option value="3">Concerto Realizado
                            <option value="4">Entregue e Pago
                        </c:if>
                        <c:if test="${Conserto.conserto.status == 4 }">
                            <option value="4">Entregue e Pago
                        </c:if>
                    </select>
                </div>
                
                <!-- Opções para Concerto em Andamento -->
                <div class="f_row" id="opt2">
                    <label class="l_cadastro" id="opt2" id="tresp">Técnico Respons.:</label>
                    <input type="text" id="i_tresp" name="tecnico" class="i_text" readonly="readonly" 
                           <c:if test="${not empty Conserto.usuario.login}" >
                               value="${Conserto.usuario.login}"/>
                           </c:if>
                           <c:if test="${empty Conserto.usuario.login}" >
                               value="${Usuario}"/>
                           </c:if>
                </div>
                
                <div class="f_row" id="opt2-2">
                    <label class="l_cadastro">Data de Início:</label>
                    <input type="text" name="dataInicial" id="i_dinit" class="i_text" readonly="readonly"/>
                </div>
                
                <!-- Opções para Concerto Realizado -->
                <div class="f_row" id="opt3">
                    <label class="l_cadastro">Data de Finalização:</label>
                    <input type="text" name="dataFinal" id="i_dfin" class="i_text" readonly="readonly"/>
                </div>
                
                <div class="f_row" id="opt3-3">
                    <label class="l_cadastro">Observação:</label>
                    <textarea name="observacao" id="i_descomp" class="i_text" cols=50 rows=7>${Conserto.conserto.observacao}</textarea>
                </div>
                
                <!-- Opções para Concerto Entregue e pago -->
                <div class="f_row" id="opt4">
                    <label class="l_cadastro">Data de Entrega:</label>
                    <input type="text" name="dataEntregaReal" id="i_dent" class="i_text" readonly="readonly" value="1"/>
                </div>
                
                <div class="f_row">
                    <input type="button" onclick="cancelar();" id="b_salvar" value="Cancelar"/>
                    <input type="submit" id="b_salvar" name="b_salvar" value="Salvar"/>
                </div>
            </form>
        </div>
    </body>
</html>
