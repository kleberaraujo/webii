<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="promo" class="Model.PromoDAO" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>CineClub | Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css' />
        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-8">
                        <h1>Promoções</h1>
                    </div>
                    <div class="col-lg-4">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <br />
                        <c:forEach var="p" items="${promo.lista}">
                            <div class="bs-example">
                                <div class="jumbotron">
                                    <div class="row">
                                        <c:choose>
                                            <c:when test="${p.ativo}">
                                                <div id="pativo"></div>
                                            </c:when>
                                            <c:otherwise>
                                                <div id="pnativo"></div>
                                            </c:otherwise>
                                        </c:choose>
                                        <div id="pdesc"> &nbsp ${p.descricao} </div>
                                        <a id="bt_remover" onclick="return f_delete();" href="promocoes?action=remover&id=${p.npromo}">Remover</a>
                                        <a id="bt_alterar" href="promocoes?action=setAlterar&id=${p.npromo}">Alterar</a>
                                        <b>&nbsp Desconto:</b> ${p.desconto}% <br/>
                                        <b>&nbsp Aplicavel a:</b>
                                        <c:forEach var="g" items="${p.generos}">
                                            <c:choose>
                                                <c:when test="${g == 'Acao' }">&nbsp Ação</c:when>
                                                <c:when test="${g == 'Animacao' }">&nbsp Animação</c:when>
                                                <c:when test="${g == 'Comedia' }">&nbsp Comédia</c:when>
                                                <c:when test="${g == 'Ficcao' }">&nbsp Ficção</c:when>
                                                <c:otherwise>${g}</c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </div>     
                                </div>
                            </div>
                        </c:forEach>
                        <!--<div class="col-lg-12">
                            <ul class="pagination">
                                <li class="disabled"><a href="#">&laquo;</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>-->
                    </div>
                </div><!-- /.row -->
            </div><!-- /#page-wrapper -->
        </div><!-- /#wrapper -->
        <!-- JavaScript -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>
