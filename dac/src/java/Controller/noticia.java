package Controller;

import java.io.Serializable;
import java.util.List;

public class Noticia implements Serializable{
    
    private int id;
    private String titulo;
    private String texto;
    private String video;
    private String data;
    private String imagem;
    private List<String> generos;
    
    public Noticia(){
        
    }
    
    public Noticia(String titulo, String video, String data, String texto, List<String> generos, String img) {
        
        this.generos = generos;
        this.titulo = titulo;
        this.video = video;
        this.texto = texto;
        this.generos = generos;
    }

    //ID
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //Titulo
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
        
    //Texto
    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    //Video
    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    
    //Data de publicação
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    //Imagem
    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public List<String> getGeneros() {
        return generos;
    }

    public void setGeneros(List<String> generos) {
        this.generos = generos;
    }
}
