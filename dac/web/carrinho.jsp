<jsp:useBean id="fd" class="Model.FilmeDAO"/>
<jsp:useBean id="pd" class="Model.PromoDAO"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="pt-BR" ></fmt:setLocale>
<html class="no-js">
    <head>
        <meta charset="utf-8"/>
        <title> CineClub | Carrinho </title>
        <%@include file="./includes/scripts_inc.jsp" %>	
        <link rel="stylesheet" href="css/compra.css" />
        <script src="js/popup/modal.js"></script>
    </head>
    <body class="blog">
        <!-- HEADER -->
        <header>
            <div class="wrapper cf">
                <%@include file="./includes/header.jsp" %>
            </div>
        </header>
        <!-- ENDS HEADER -->
        <!-- MAIN -->
        <div id="main">
            <div id="cbox">
                <div id="cbox-hader">
                    <div class="dhselec">Número</div>
                    <div id="dhfilme">Filme</div>
                    <div id="dhorario">Data</div>
                    <div class="cbox-hora">Horário</div>
                    <div class="cbox-hora">Valor</div>
                    <div class="cbox-desc">Desconto</div>
                    <div class="cbox-hora">Eliminar</div>
                </div>
                <div id="cbox-item-box">
                    <c:set var="s" value="${0}"></c:set>
                    <c:set var="total" value="${0}"></c:set>
                    <c:if test="${empty carrinho}">
                        <div id="car-erro"> Seu carrinho está vazio! </div>
                    </c:if>
                    <c:forEach var="i" items="${carrinho}">
                        <c:set var="count" value="${count + 1}"></c:set>
                        <c:if test="${i.nsessao ne s}">
                            <c:set var="f" value="${fd.getFilmeBySessao(i.nsessao)}"></c:set>
                            <c:set var="ss" value="${fd.getSessao(i.nsessao)}"></c:set>
                            <c:set var="desc" value="${pd.getDescBySessao(i.nsessao)}"></c:set>
                        </c:if>
                        <div class="item-line">
                            <div id="citem-id"> ${count} </div>
                            <div id="citem-filme"> ${f.titulo} </div>
                            <div id="citem-data"> ${ss.data} </div>
                            <div id="citem-hora"> ${ss.hinit}h </div>
                            <c:set var="valor" value="${i.valor}"></c:set>
                            <div id="citem-valor"><fmt:formatNumber value="${valor}" minFractionDigits="2" type="currency"/></div>
                            <div id="citem-desc"> <fmt:formatNumber value="${i.valor * desc / 100}" minFractionDigits="2" type="currency"/> </div>
                            <a id="ldel" href="processaSessao?action=carrinho&option=del&id=${count}"><div id="citem-exc">X</div></a>
                        </div>
                        <c:set var="s" value="${i.nsessao}"></c:set>
                        <c:set var="total" value="${total + i.valor - ( i.valor * desc / 100 ) }"></c:set>
                    </c:forEach>
                </div>
            </div>
            <div id="cbox-footer">
                <c:if test="${not empty carrinho}">
                    <h4>
                        Total: <fmt:formatNumber value="${total}" minFractionDigits="2" type="currency"/>
                    </h4>
                    
                    <c:if  test="${not empty usuario.id}">
                        <button id="link-comprar" type="button" data-toggle="modal" data-target="#myModal">Finalizar Compra</button>
                    </c:if>

                    <c:if test="${empty usuario.id}">
                        <a id="nlogin"> Faça Login para continuar a compra </a>
                    </c:if>
                </c:if>
            </div> 
        </div>
        <!-- ENDS MAIN -->
                <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Deseja Finalizar sua Compra?</h4>
                    </div>
                    <div class="modal-body">
                        Atualmente seu carrinho possui ${count} ingresso(s) com um total de 
                        <fmt:formatNumber value="${total}" minFractionDigits="2" type="currency"/>
                    </div>
                    <div class="modal-footer">
                        <form action="processaSessao?action=finalizar" method="POST" >
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Continuar Comprando</button>
                            <button type="submit" class="btn btn-primary" >Finalizar Compra</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="./includes/footer.jsp" %>
        <script>
            
            $('#myModal').on('shown.bs.modal', function (e) {
                
             });
            
            function formatReal(num)
            {
                alert(num);
                x = 0;
                if (num < 0) {
                    num = Math.abs(num);
                    x = 1;
                }
                if (isNaN(num))
                    num = "0";
                cents = Math.floor((num * 100 + 0.5) % 100);
                num = Math.floor((num * 100 + 0.5) / 100).toString();
                if (cents < 10)
                    cents = "0" + cents;
                for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                    num = num.substring(0, num.length - (4 * i + 3)) + '.'
                            + num.substring(num.length - (4 * i + 3));
                ret = num + ',' + cents;
                if (x == 1)
                    ret = ' - ' + ret;
                document.getElementById("ivalor").value = ret;
            }
        </script>
    </body>
</html>