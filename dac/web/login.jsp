<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CineClub | Login</title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="blog">
        <div id="login_box">
            <h1>CINECLUB</h1>
            <input type="hidden" id="login_err" value="${lerro}" />
            <form name="f_login" method="post" action="usuarios?action=login">
                <div class="f_row">
                    <label class="l_login">Email:</label>
                    <input type="text" name="i_login" class="login_input" maxlength="80"/>
                </div>
                <div class="f_row">
                    <label class="l_login">Senha:</label>
                    <input type="password" name="i_senha" class="login_input" maxlength="80"/>
                </div>
                <div class="f_row">
                    <input type="submit" id="b_logar" name="b_logar" value="Logar"/>
                </div>
            </form>
            <div id="login_bottom">
                <a href="./cadastro.jsp">Quero me cadastrar</a>
            </div>
        </div>
    </body>
    <script>
        var x = document.getElementById("login_err").value;
        if(x == 'X'){
            alert("Usuário ou senha inválida");
        }
    </script>
</html>