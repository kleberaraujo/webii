package Servlets;

import Controller.ClienteWebService;
import Controller.Compra;
import Controller.Filme;
import Controller.Mediator;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/admin.jsp")
public class Filtro implements Filter {
    
    private Mediator mediator;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        mediator = new Mediator();
        try {
            List<Filme> lf = ClienteWebService.getFilmes();
            List<String> totdados = mediator.getTotDados();
            List<String> totcompras = mediator.getCompras();
            List<Integer> percgen = mediator.getGenPerc();
            request.setAttribute("totdados", totdados);
            request.setAttribute("totcompras", totcompras);
            request.setAttribute("percgen", percgen);
            request.setAttribute("othersfilm", lf);
            chain.doFilter(request, response);
        } catch (Exception ex) {
            response.getWriter().println(ex);
        }
    }

    @Override
    public void destroy() {
        
    }
}