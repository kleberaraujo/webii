package Servlets;

import Controller.Comentario;
import Controller.Mediator;
import Controller.Noticia;
import Model.GenericDAO;
import Model.NoticiaDAO;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "ProcessaNoticia", urlPatterns = {"/noticias"})
public class ProcessaNoticia extends HttpServlet {

    private String filePath;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
                
        String action = request.getParameter("action");
        switch (action) {
            case "inserir":
            case "alterar":
                {
                    Noticia n = new Noticia();
                    File file;
                    int maxFileSize = 5000 * 1024;
                    int maxMemSize = 5000 * 1024;
                    Mediator mediator = new Mediator();
                    String titulo = "", img = "", video = "", texto = "", antimg = "", antdata = "";
                    int id = 0;
                    List<String> generos = new ArrayList();
                    filePath = this.getServletContext().getRealPath("") + "\\img\\noticias\\";
                    String contentType = request.getContentType();
                    if ((contentType.indexOf("multipart/form-data") >= 0)) {
                        
                        DiskFileItemFactory factory = new DiskFileItemFactory();
                        
                        factory.setSizeThreshold(maxMemSize);
                        
                        factory.setRepository(new File("c:\\temp"));
                        
                        ServletFileUpload upload = new ServletFileUpload(factory);
                        
                        upload.setSizeMax(maxFileSize);
                        try {
                            List fileItems = upload.parseRequest(request);
                            Iterator i = fileItems.iterator();
                            FileItem fi = null;
                            while (i.hasNext()) {
                                fi = (FileItem) i.next();
                                if (fi.isFormField()) {
                                    switch (fi.getFieldName()) {
                                        case "id":
                                            if(!fi.getString().equals("")){
                                                id = Integer.parseInt(fi.getString());
                                                n.setId(id);
                                            }
                                            break;
                                        case "titulo":
                                            titulo = fi.getString();
                                            n.setTitulo(titulo);
                                            break;
                                        case "antimg":
                                            antimg = fi.getString();
                                            n.setImagem(antimg);
                                            break;
                                        case "antdata":
                                            antdata = fi.getString();
                                            n.setData(antdata);
                                            break;
                                        case "video":
                                            video = fi.getString();
                                            n.setVideo(video);
                                            break;
                                        case "texto":
                                            texto = fi.getString();
                                            n.setTexto(texto);
                                            break;
                                        case "p_act":
                                            generos.add("1");
                                            break;
                                        case "p_anm":
                                            generos.add("2");
                                            break;
                                        case "p_avt":
                                            generos.add("3");
                                            break;
                                        case "p_bio":
                                            generos.add("4");
                                            break;
                                        case "p_cmd":
                                            generos.add("5");
                                            break;
                                        case "p_drm":
                                            generos.add("6");
                                            break;
                                        case "p_fts":
                                            generos.add("7");
                                            break;
                                        case "p_fcc":
                                            generos.add("8");
                                            break;
                                        case "p_hrr":
                                            generos.add("9");
                                            break;
                                        case "p_rmc":
                                            generos.add("10");
                                            break;
                                        case "p_sps":
                                            generos.add("11");
                                            break;
                                    }
                                } else {
                                    if ( n.getId() == 0) {
                                        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                                        Calendar cal = Calendar.getInstance();
                                        String hora = dateFormat.format(cal.getTime());
                                        MessageDigest m = MessageDigest.getInstance("MD5");
                                        m.update(hora.getBytes(), 0, hora.length());
                                        img = new BigInteger(1, m.digest()).toString(16) + ".jpg";
                                        filePath = filePath + img;
                                        img = "\\img\\noticias\\" + img;
                                        file = new File(filePath);
                                        fi.write(file);
                                    }
                                    else if(fi.getString().equals("") || fi.getString() == null ){
                                        img = antimg;
                                    }
                                    else {
                                        img = antimg;
                                        filePath = filePath + img.substring(14);
                                        file = new File(filePath);
                                        fi.write(file);
                                        img = filePath.replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web\\", ".\\");
                                    }
                                    n.setImagem(img);
                                }
                            }
                            if ( n.getData().equals("") || n.getData() == null ) {
                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                Calendar cal = Calendar.getInstance();
                                String data = dateFormat.format(cal.getTime());
                                n.setData(data);
                            }
                            n.setGeneros(generos);
                            GenericDAO gen = new GenericDAO();
                            if (n.getId() == 0) {
                                mediator.InsereNoticia(n);
                                String sid = "" + n.getId();
                                gen.InsereGenero(sid, "nnot", n.getGeneros());
                            } else {
                                mediator.AtualizaNoticia(n);
                                String sid = "" + n.getId();
                                gen.AtualizaGenero(sid, "nnot", n.getGeneros());
                            }
                            synchronized(response){
                                response.wait(2000);
                            }
                            response.sendRedirect("./noticias-todas.jsp");
                        } catch (FileUploadException ex) {
                            response.getWriter().println(ex);
                        }
                    }       break;
                }
            case "setAlterar":
                {
                    String id = request.getParameter("id");
                    NoticiaDAO noticiaDAO = new NoticiaDAO();
                    Noticia n = noticiaDAO.getNoticia(id);
                    request.setAttribute("noti", n);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/nova-noticia.jsp?action=alterar");
                    rd.forward(request, response);
                    break;
                }
            case "remover":
                {
                    String id = request.getParameter("id");
                    NoticiaDAO noticiaDAO = new NoticiaDAO();
                    Noticia n = noticiaDAO.getNoticia(id);
                    filePath = this.getServletContext().getRealPath("") + n.getImagem();
                    File f = new File(filePath);
                    f.delete();
                    noticiaDAO.delete(n);
                    response.sendRedirect("./noticias-todas.jsp");
                    break;
                }
            case "single":
                {
                    String id = request.getParameter("noticia");
                    NoticiaDAO noticiaDAO = new NoticiaDAO();
                    Noticia n = noticiaDAO.getNoticia(id);
                    GenericDAO generoDAO = new GenericDAO();
                    String dado = "nnot";
                    List<Comentario> lc = generoDAO.getComenarios(id, dado);
                    request.setAttribute("n", n);
                    request.setAttribute("comentarios", lc);
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/single-noticia.jsp");
                    rd.forward(request, response);
                    break;
                }
            case "coment":
                {
                    int nnot = Integer.parseInt(request.getParameter("nnot"));
                    int coduser = Integer.parseInt(request.getParameter("coduser"));
                    String texto = request.getParameter("coment");
                    Comentario c = new Comentario();
                    c.setCoduser(coduser);
                    c.setComentario(texto);
                    c.setNnot(nnot);
                    c.setNfilme(0);
                    GenericDAO generoDAO = new GenericDAO();
                    generoDAO.setComentario(c);
                    response.sendRedirect("noticias?action=single&noticia="+nnot);
                    break;
                }
            case "pesq":
                String pesq = request.getParameter("pesq");
                NoticiaDAO nd = new NoticiaDAO();
                List<Noticia> ln = nd.pesNoticias(pesq);
                request.setAttribute("noticias", ln);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/noticias-todas.jsp");
                rd.forward(request, response);
                //response.getWriter().println(pesq);
                break;
            case "todas":
                nd = new NoticiaDAO();
                ln = nd.getLista();
                request.setAttribute("noticias", ln);
                rd = getServletContext().getRequestDispatcher("/noticias-todas.jsp");
                rd.forward(request, response);
                break;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
