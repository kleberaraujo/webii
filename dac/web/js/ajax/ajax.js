var xmlhttp;
if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.overrideMimeType('text/xml');
}
else if (window.ActiveXObject) {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
else {
    alert("Browser sem suporte para XML HTTP Request! Troque de navegador!");
}

function f_getHorarios(f){
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
            document.getElementById("ditens").innerHTML = xmlhttp.responseText;
        }
        else{
            return false;
        }
    };
    var d = document.getElementById("data");
    document.getElementById("subtotal").innerHTML = 'Subtotal: R$ 0,00';
    if(d.value){
        xmlhttp.open("GET","filmes?action=dataSessoes&id="+f+"&data="+d.value, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send(null);
    }
};