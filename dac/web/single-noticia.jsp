<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>CineClub - Sobre o filme</title>
        <%@include file="./includes/scripts_inc.jsp" %>
    </head>
    <body class="single">
        <!-- HEADER -->
    <header>
        <div class="wrapper cf">
            <%@include file="./includes/header.jsp" %>
        </div>
    </header>
    <!-- ENDS HEADER -->

    <!-- MAIN -->
    <div id="main">
        <div class="wrapper cf">

            <!-- posts list -->
            <div id="posts-list" class="cf">   	

                <!-- Standard -->
                <article class="format-standard">
                    <div class="feature-image">
                        <img src="./${n.imagem}" alt="Alt text" />
                    </div>
                    <div class="box cf">

                        <div class="entry-date"><div class="number">
                                ${fn:substring(n.data, 0, 2)}
                            </div><div class="month">
                                <c:choose>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='01'}">${"JAN"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='02'}">${"FEV"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='03'}">${"MAR"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='04'}">${"ABR"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='05'}">${"MAI"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='06'}">${"JUN"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='07'}">${"JUL"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='08'}">${"AGO"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='09'}">${"SET"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='10'}">${"OUT"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='11'}">${"NOV"}</c:when>
                                    <c:when test="${(fn:substring(n.data, 3, 5))=='12'}">${"DEZ"}</c:when>
                                </c:choose>
                            </div></div>

                        <div class="excerpt">
                            <div class="post-heading" >${n.titulo}</div>
                            <div class="entry-content">
                                <p>
                                    ${n.texto}
                                </p>
                            </div>

                            <c:if test="${not empty n.video}">
                                <div class="video-container">
                                    <iframe  src="http://www.youtube.com/embed/${n.video}" ></iframe>
                                </div>
                            </c:if>
                        </div>

                        <div class="meta">

                        </div>

                    </div>
                </article>
                <!-- ENDS  Standard -->

                <!-- comments list -->
                <div id="comments-wrap">
                    
                    <c:if test="${empty comentarios}">
                        <h4 class="heading">Nenhum comentário</h4>
                    </c:if>
                    
                    <c:if test="${not empty comentarios}">
                        <c:set var="tot" value="${0}"></c:set>
                        <c:forEach var="c" items="${comentarios}">
                            <c:set var="tot" value="${tot + 1}"></c:set>
                        </c:forEach>
                        <c:if test="${tot > 1}">
                            <h4 class="heading">${tot} Comentários</h4>
                        </c:if>
                        <c:if test="${tot == 1}">
                            <h4 class="heading">${tot} Comentário</h4>
                        </c:if>
                    </c:if>
                        
                    <ol class="commentlist">
                        <li class="comment even thread-even depth-1">

                            <c:forEach var="c" items="${comentarios}">
                                <div id="comment-1" class="comment-body cf">
                                    <img alt='' src='./${c.user.img}' class='avatar avatar-35 photo' height='45' width='45' />
                                    <div class="comment-author vcard">${c.user.nome}</div>
                                    <div class="comment-inner">
                                        <p>	
                                            ${c.comentario}
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </li>
                    </ol>
                </div>
                <!-- ENDS comments list -->

                <!-- Respond -->				
                <div id="respond">
                    <h3 id="reply-title">Deixe um Comentário
                        <small>

                        </small>
                    </h3>
                    
                    <c:if test="${empty usuario.id}">
                        Faça Login para deixar um Comentário!
                    </c:if>
                    
                    <form action="noticias?action=coment" method="post" id="commentform">
                        
                        <input type="hidden" name="coduser" value="${usuario.id}" />
                        <input type="hidden" name="nnot" value="${n.id}" />
                        <p class="comment-form-comment">
                            <textarea id="comment" name="coment" cols="45" rows="8"
                                      <c:if test="${empty usuario.id}">
                                          disabled="disabled"
                                      </c:if>
                                      ></textarea>
                        </p>

                        <p class="form-submit">
                            <input name="submit" type="submit" 
                                   <c:if test="${empty usuario.id}">
                                       disabled="disabled"
                                   </c:if> class="submitcadastro" id="submit" value="Comentar">
                        </p>

                    </form>
                </div>
                <!-- ENDS Respond -->										

            </div>
            <!-- ENDS posts list -->

            <!-- sidebar -->
            <aside id="sidebar">

            </aside>
            <!-- ENDS sidebar -->

        </div><!-- ENDS WRAPPER -->
    </div>
    <!-- ENDS MAIN -->

    <%@include file="./includes/footer.jsp" %>

</body>
</html>