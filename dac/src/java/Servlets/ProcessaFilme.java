package Servlets;

import Controller.Comentario;
import Controller.Filme;
import Controller.Mediator;
import Controller.Sessao;
import Model.FilmeDAO;
import Model.GenericDAO;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "ProcessaFilme", urlPatterns = {"/filmes"})
public class ProcessaFilme extends HttpServlet {

    private String filePath;
    private Mediator mt;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        
        mt = new Mediator();
        
        String action = request.getParameter("action");
        switch (action) {
            case "inserir":
            case "alterar": {
                Filme f = new Filme();
                File file;
                int maxFileSize = 5000 * 1024;
                int maxMemSize = 5000 * 1024;
                Mediator mediator = new Mediator();
                String titulo = "", diretor = "", sinopse = "", antimg = "", curiosidades = "", img = "", video = "";
                int duracao = 0, idademin = 0, ano = 0, id = 0;
                List<String> generos = new ArrayList();
                filePath = this.getServletContext().getRealPath("");
                String contentType = request.getContentType();
                if ((contentType.indexOf("multipart/form-data") >= 0)) {

                    DiskFileItemFactory factory = new DiskFileItemFactory();

                    factory.setSizeThreshold(maxMemSize);

                    factory.setRepository(new File("c:\\temp"));

                    ServletFileUpload upload = new ServletFileUpload(factory);

                    upload.setSizeMax(maxFileSize);
                    try {
                        List fileItems = upload.parseRequest(request);

                        Iterator i = fileItems.iterator();

                        FileItem fi = null;

                        while (i.hasNext()) {

                            fi = (FileItem) i.next();

                            if (fi.isFormField()) {
                                switch (fi.getFieldName()) {
                                    case "id":
                                        if (!fi.getString().equals("")) {
                                            id = Integer.parseInt(fi.getString());
                                            f.setId(id);
                                        }
                                        break;
                                    case "antimg":
                                        antimg = fi.getString();
                                        f.setImagem(antimg);
                                        break;
                                    case "titulo":
                                        titulo = fi.getString();
                                        f.setTitulo(titulo);
                                        break;
                                    case "video":
                                        video = fi.getString();
                                        f.setVideo(video);
                                        break;
                                    case "sinopse":
                                        sinopse = fi.getString();
                                        f.setSinopse(sinopse);
                                        break;
                                    case "diretor":
                                        diretor = fi.getString();
                                        f.setDiretor(diretor);
                                        break;
                                    case "duracao":
                                        duracao = Integer.parseInt(fi.getString());
                                        f.setDuracao(duracao);
                                        break;
                                    case "idademin":
                                        idademin = Integer.parseInt(fi.getString());
                                        f.setIdademin(idademin);
                                        break;
                                    case "curiosidades":
                                        curiosidades = fi.getString();
                                        f.setCuriosidades(curiosidades);
                                        break;
                                    case "p_act":
                                        generos.add("1");
                                        break;
                                    case "p_anm":
                                        generos.add("2");
                                        break;
                                    case "p_avt":
                                        generos.add("3");
                                        break;
                                    case "p_bio":
                                        generos.add("4");
                                        break;
                                    case "p_cmd":
                                        generos.add("5");
                                        break;
                                    case "p_drm":
                                        generos.add("6");
                                        break;
                                    case "p_fts":
                                        generos.add("7");
                                        break;
                                    case "p_fcc":
                                        generos.add("8");
                                        break;
                                    case "p_hrr":
                                        generos.add("9");
                                        break;
                                    case "p_rmc":
                                        generos.add("10");
                                        break;
                                    case "p_sps":
                                        generos.add("11");
                                        break;
                                }
                            } else {
                                if (f.getId() == 0) {
                                    DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                                    Calendar cal = Calendar.getInstance();
                                    String hora = dateFormat.format(cal.getTime());
                                    MessageDigest m = MessageDigest.getInstance("MD5");
                                    m.update(hora.getBytes(), 0, hora.length());
                                    img = new BigInteger(1, m.digest()).toString(16) + ".jpg";
                                    img = "\\img\\filmes\\" + img;
                                    filePath = filePath + img;
                                    file = new File(filePath);
                                    fi.write(file);
                                } else if (fi.getString().equals("") || fi.getString() == null) {
                                    img = antimg;
                                } else {
                                    img = antimg;
                                    filePath = filePath + img.substring(14);
                                    file = new File(filePath);
                                    fi.write(file);
                                    img = filePath.replace("C:\\Users\\Kleber Araujo\\Documents\\NetBeansProjects\\dac\\build\\web\\", ".\\");
                                }
                                f.setImagem(img);
                            }
                        }
                        f.setGeneros(generos);
                        GenericDAO gen = new GenericDAO();
                        if (f.getId() == 0) {
                            mediator.InsereFilme(f);
                            String sid = "" + f.getId();
                            gen.InsereGenero(sid, "nfilme", f.getGeneros());
                        } else {
                            mediator.AtualizaFilme(f);
                            String sid = "" + f.getId();
                            gen.AtualizaGenero(sid, "nfilme", f.getGeneros());
                        }
                        synchronized (response) {
                            response.wait(2000);
                        }
                        response.sendRedirect("./filmes-todos.jsp");
                    } catch (FileUploadException ex) {
                        Logger.getLogger(ProcessaFilme.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            }
            case "setAlterar": {
                String id = request.getParameter("id");
                FilmeDAO filmeDAO = new FilmeDAO();
                Filme f = filmeDAO.getFilme(id);
                request.setAttribute("filme", f);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/novo-filme.jsp?action=alterar");
                rd.forward(request, response);
                break;
            }
            case "em-cartaz": {
                List<Filme> lfilmes = mt.getFilmesemcartaz();
                request.setAttribute("filmes", lfilmes);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/em-cartaz.jsp");
                rd.forward(request, response);
                break;
            }
            case "sessoes":{
                HttpSession session = request.getSession();
                String id = request.getParameter("filme");
                request.setAttribute("idf", id);
                FilmeDAO filmeDAO = new FilmeDAO();
                Filme f = filmeDAO.getFilme(id);
                List<String> ldataSessoes = mt.getDataSessoes(id);
                session.setAttribute("dataSessoes", ldataSessoes);
                request.setAttribute("filme", f);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/comprar_ingressos.jsp");
                rd.forward(request, response);
                break;
            }
            case "dataSessoes":{
                PrintWriter out = response.getWriter();
                String id = request.getParameter("id");
                String data = request.getParameter("data");
                NumberFormat nf = NumberFormat.getCurrencyInstance();
                List<Sessao> lSessoes = mt.getHSessoes(id, data);
                for(Sessao s: lSessoes){
                    String valor = nf.format(s.getValor());
                    out.println("<div id='dline'>");
                    out.println("<div id='dselec'><input id='fselec"+s.getNsessao()+"' name='fselec"+s.getNsessao()+"' value='"+s.getNsessao()+"' type='checkbox' onchange='f_bloq("+s.getNsessao()+");' /></div>");
                    out.println("<div id='dsala'>Sala "+s.getNsala()+"</div>");
                    out.println("<div id='dhora'>"+s.getHinit().substring(0, 5)+"h</div>");
                    out.println("<div id='dvalor'>"+valor+"</div>");
                    out.println("<input type='hidden' id='rvalue' name='rvalue"+s.getNsessao()+"' value='"+s.getValor()+"'/>");
                    out.println("<div id='dint' onclick='verifBloq("+s.getNsessao()+");'><input type='number' id='fint"+s.getNsessao()+"' name='fint"+s.getNsessao()+"' size='1' min='0' disabled='disabled' onchange='f_sum();' /></div>");
                    out.println("<div id='dmeia' onclick='verifBloq("+s.getNsessao()+");'><input type='number' id='fmeia"+s.getNsessao()+"' name='fmeia"+s.getNsessao()+"' size='1' min='0' disabled='disabled' onchange='f_sum();' /></div>");
                    out.println("<br/></div>");
                }
                break;
            }
            case "seefilm":{
                String fid = request.getParameter("nfilme");
                FilmeDAO fd = new FilmeDAO();
                Filme f = fd.getFilme(fid);
                request.setAttribute("filme", f);
                String dado = "nfilme";
                GenericDAO genericDAO = new GenericDAO();
                List<Comentario> lc = genericDAO.getComenarios(fid, dado);
                request.setAttribute("comentarios", lc);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/single-filme.jsp");
                rd.forward(request, response);
                //PrintWriter out = response.getWriter();
                //out.print(request.getAttribute("filme"));
                break;
            }
            case "coment":{
                int nfilme = Integer.parseInt(request.getParameter("nfilme"));
                int coduser = Integer.parseInt(request.getParameter("coduser"));
                String texto = request.getParameter("coment");
                Comentario c = new Comentario();
                c.setCoduser(coduser);
                c.setComentario(texto);
                c.setNnot(0);
                c.setNfilme(nfilme);
                GenericDAO genericDAO = new GenericDAO();
                genericDAO.setComentario(c);
                response.sendRedirect("filmes?action=seefilm&nfilme="+nfilme);
                break;
            }
            case "todos":{
                FilmeDAO fd = new FilmeDAO();
                List<Filme> lf = fd.getLista();
                request.setAttribute("filmes", lf);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/filmes-todos.jsp");
                rd.forward(request, response);
                break;
            }
            case "pesq":{
                String pesq = request.getParameter("pesq");
                FilmeDAO fd = new FilmeDAO();
                List<Filme> lf = fd.pesqFilme(pesq);
                request.setAttribute("filmes", lf);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/filmes-todos.jsp");
                rd.forward(request, response);
                break;
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaFilme.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessaFilme.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
