package Controller;

public class Comentario {
    
    private int ncoment;
    private int nfilme;
    private int nnot;
    private int coduser;
    private String comentario;
    private Pessoa user;
    
    public Comentario(){
        
    }

    public int getNcoment() {
        return ncoment;
    }

    public void setNcoment(int ncoment) {
        this.ncoment = ncoment;
    }

    public int getNfilme() {
        return nfilme;
    }

    public void setNfilme(int nfilme) {
        this.nfilme = nfilme;
    }

    public int getNnot() {
        return nnot;
    }

    public void setNnot(int nnot) {
        this.nnot = nnot;
    }

    public int getCoduser() {
        return coduser;
    }

    public void setCoduser(int coduser) {
        this.coduser = coduser;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Pessoa getUser() {
        return user;
    }

    public void setUser(Pessoa user) {
        this.user = user;
    }    
}
