<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8" errorPage="" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not usuario.admin}">
    <c:redirect url="./index.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CineClub | Admin</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.mask.js"></script>
        <script src="js/jquery.mask.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/morris/chart-data-morris.js"></script>
        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
        <script>
            $(document).ready(function(){
               $('#valor').mask('000.000.000.000.000,00', {reverse: true});
            });
            function f_message(){
                $.blockUI({
                    message: '<h2>Salvando...</h2>',
                    timeout: 2000
                });
            }
            function f_sethfin(){
                var h = document.getElementById('hinit').value;
                var m = h[3] + h[4];
                h = h[0] + h[1];
                h = parseInt(h) + parseInt(3) > 23 ? '0' + (parseInt(h) + parseInt(3) - 24) : parseInt(h) + parseInt(3) ;
                document.getElementById('hfin').setAttribute("value", h+":"+m );
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <%@include file="./includes/menu_admin.jsp" %>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Cadastro de Sessões</h1>
                        <br />
                        <form onsubmit="f_message()" action="processaSessao?action=${param.action == 'novaSessao' ? "inserir" : param.action }" method="POST">
                            <div class="form-group">
                                <label>Filme</label>
                                <select class="form-control" name="filme">
                                    <c:forEach var="f" items="${filmes}">
                                        <option value="${f.id}" >${f.titulo}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Sala</label>
                                <select class="form-control" name="sala">
                                    <c:forEach var="s" items="${salas}">
                                        <option value="${fn:substring(s, 0, 1)}">${fn:substring(s, 0, 1)} - Capacidade de ${fn:substring(s, 3, 7)} pessoas </option>
                                    </c:forEach>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Dia</label><br/>
                                <input required="required" type="date" class="form-control-date" name="data" />
                            </div>
                            
                            <div class="form-group">
                                <label>Horário</label><br />
                                <input onchange="f_sethfin()" class="form-control-date" id="hinit" required="required" type="time" name="hinit" />
                                <label>até </label> <input disabled="disabled" class="form-control-date" required="required" id="hfin" type="time" name="hfin" />
                            </div>
                            
                            <div class="form-group">
                                <label>Valor do Ingresso</label>
                                <input type="text" class="form-control" required="required" id="valor" name="valor" />
                            </div>
                            <button type="submit" class="btn btn-default">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/popup/jquery.blockUI.js"></script>
    </body>
</html>